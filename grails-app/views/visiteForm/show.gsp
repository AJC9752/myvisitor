<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitStatus" %>
<%@ page import="org.afdb.rs.VisitType" %>


<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label', default:'Error')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<div class="row">		
		<div class="col-md-12">
			<h3 class="center">${message(code:'visiteForm.show.title', default:'Request Management')}</h3>
			<br/>
		</div>
	</div>
	

<g:render template="/_menu/menuVisitForm" model="[visiteFormInstance:visiteFormInstance]"/>
<br/>
<section id="show-visiteForm" class="first">
	
	<div class="row">
	
		<div class="col-md-6">
			<table class="table">
				<tbody>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></td>
						<td valign="top" class="value">
							<strong>${visiteFormInstance?.receiverVisit?.lastName} ${visiteFormInstance?.receiverVisit?.firstName}</strong>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visiteForm.askBy.label" default="Requested By" /></td>
						<td valign="top" class="value">
							<strong>${visiteFormInstance?.askBy?.lastName} ${visiteFormInstance?.askBy?.firstName}</strong>
						</td>
					</tr>
										
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitType.label" default="Type of Visit" /></td>
						<td valign="top" class="value">
							<g:visitType type="${visiteFormInstance?.visitType}"/>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="location.label" default="Location" /></td>
						<td valign="top" class="value">
							${fieldValue(bean: visiteFormInstance, field: "location")}
						</td>
					</tr>
					
					<g:if test="${visiteFormInstance?.visitType?.code == VisitType.BUSINESS}">
						<tr class="prop">
							<td valign="top" class="name"><g:message code="visiteForm.company.label" default="Company" /></td>
							<td valign="top" class="value">
								<strong>${fieldValue(bean: visiteFormInstance, field: "company")}</strong>
							</td>
						</tr>
					</g:if>
					
					<g:if test="${visiteFormInstance?.visitType?.code == VisitType.EVENT}">
						<tr class="prop">
							<td valign="top" class="name"><g:message code="visiteForm.event.label" default="Event" /></td>
							<td valign="top" class="value">
								<strong>${fieldValue(bean: visiteFormInstance, field: "event")}</strong>
							</td>
						</tr>
					</g:if>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="status.label" default="Status" /></td>
						<td valign="top" class="value">
							<g:visitStatus status="${visiteFormInstance?.status}"/>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" colspan="2" class="name">
							<g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" />
							<div class="alert" role="alert">
								<strong>
									${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}
								</strong>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="2">
							<table class="table table-bordered margin-top-medium">
								<thead>
									<tr>							
										<th><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /></th>
										<g:if test="${!(visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY])}">
											<th><g:message code="visiteForm.expectedDepartureDateTime.label" default="Expected Departure Date Time" /></th>
										</g:if>
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]}">
											<th><g:message code="visiteForm.visitDuration.label" default="Visit Duration" /></th>
										</g:if>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="dd MMM yyyy"/></strong>
											<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="HH:mm"/></strong>
										</td>
										
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.BUSINESS, VisitType.EVENT]}">
											<td>
												<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="dd MMM yyyy"/></strong>
												<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="HH:mm"/></strong>
											</td>
										</g:if>
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]}">
											<td>
												<strong>${fieldValue(bean: visiteFormInstance, field: "visitDuration")}</strong>
											</td>				
										</g:if>				
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
			
				</tbody>
			</table>
		</div>
		
		<div class="col-md-4">
			<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.BUSINESS, VisitType.EVENT]}">
				<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS]}">
					<g:form method="post" class="form-horizontal" >
						
						<div class="control-group fieldcontain ${hasErrors(bean: visitorInstance, field: 'gender', 'error')} ">
							<label for="category" class="control-label"><g:message code="visitor.gender.label" default="Gender" /></label>
							<div class="controls">
								<g:select class="form-control" name="gender" from="${visitorInstance.constraints.gender.inList}" value="${visitorInstance?.gender}" valueMessagePrefix="visitor.gender" noSelection="['': '']" style="width:20%;"/>
								<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'gender', 'error')}</span>
							</div>
						</div>
		
						<div class="control-group fieldcontain ${hasErrors(bean: visitorInstance, field: 'firstName', 'error')} ">
							<label for="firstName" class="control-label"><g:message code="visitor.firstName.label" default="First Name" /></label>
							<div class="controls">
								<g:textField class="form-control" name="firstName" value="${visitorInstance?.firstName}" style="width:100%;"/>
								<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'firstName', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: visitorInstance, field: 'lastName', 'error')} ">
							<label for="lastName" class="control-label"><g:message code="visitor.lastName.label" default="Last Name" /></label>
							<div class="controls">
								<g:textField class="form-control" name="lastName" value="${visitorInstance?.lastName}" style="width:100%;"/>
								<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'lastName', 'error')}</span>
							</div>
						</div>
						
						<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
		
						<div class="form-actions">
							<%-- 
							<g:actionSubmit class="btn icon-btn btn-sm btn-success" action="addVisitor" value="${message(code: 'button.addVisitor.label', default: 'Add Visitor')}" />
							--%>
							<button name="_action_addVisitor" type="submit" class="btn btn-sm btn-default" >
								<i class="glyphicon glyphicon-plus"></i>
								<strong>${message(code: 'button.addVisitor.label', default: 'Add Visitor')}</strong>
							</button>
						</div>
					</g:form>
				</g:if>
			</g:if>
			
			<table class="table">
				<tbody>
					<g:each in="${visiteFormInstance.visitors.sort{it.id}}" status="i" var="v">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								<strong>
									${v?.encodeAsHTML()}
								</strong>
							</td>
							<td>
								<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS]}">
								<div class="pull-right">
									<g:link	class="btn btn-sm" controller="visiteForm" action="removVisitor" 
										params="[visitId:visiteFormInstance?.id, visitorId:v?.id]">
										<strong>
											<i class="glyphicon glyphicon-ban-circle danger"></i>
										</strong> 
									</g:link>
								</div>
								</g:if>
							</td>
						</tr>	
					</g:each>		
				</tbody>
			</table>
			
												
											
		</div>
		
		<div class="col-md-2">
			<div class="col-md-12">
				<g:form method="post" class="form-horizontal" action="submit4Approval" name="submitForm">
					<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
											
					<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS]}">			
						<button type="submit" class="btn btn-sm btn-success" id="validateId" onClick="pleaseWait(this);">
							<i class="glyphicon glyphicon-export"></i>
							<span id="idOK">
								<strong><g:message code="button.submission.label" default="Submission"/></strong>
							</span>	
						</button>
					</g:if>
					<g:else>
						<button type="submit" disabled="disabled" class="btn btn-sm btn-success">
							<i class="glyphicon glyphicon-ok"></i>
							<strong><g:message code="button.submission.ok.message" default="Form Submitted"/></strong>
						</button>
					</g:else>
				</g:form>
			</div>
			<br/>
			
			<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS]}">	
				<div class="col-md-12">
					<g:form method="post" class="form-horizontal" action="delete">
						<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
						<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('${message(code: 'default.button.reset.submission.confirm.message', default: 'Are you sure?')}');">
							<i class="glyphicon glyphicon-remove"></i>
							<strong><g:message code="default.button.delete.label" default="delete"/></strong>
						</button>
					</g:form>
				</div>
			<br/>
			</g:if>
			
			<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.SUBMITTED, VisitStatus.APPROVED]}">
				<div class="col-md-12">
					<g:form method="post" class="form-horizontal" action="canceled">
						<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
						<button type="submit" class="btn btn-sm btn-warning" onclick="return confirm('${message(code: 'default.button.reset.submission.confirm.message', default: 'Are you sure?')}');">
							<i class="glyphicon glyphicon-ban-circle"></i>
							<strong><g:message code="default.button.cancel.label" default="Canceled"/></strong>
						</button>
					</g:form>
				</div>
			</g:if>
		</div>
	</div>

</section>

</body>

<r:script>
	function pleaseWait(btn){
		btn.disabled=true;
		btn.value='Please wait ...';
		document.getElementById('idOK').innerText='Please wait ...';		
		//alert(msg);
		if (msieversion()==false){ //CHROME OU Firefox
			document.getElementById('submitForm').submit();
		}
	};
	
	function msieversion() {
 		var ms_ie = false;
	    var ua = window.navigator.userAgent;
	    var old_ie = ua.indexOf('MSIE ');
	    var new_ie = ua.indexOf('Trident/');
	
	    if ((old_ie > -1) || (new_ie > -1)) {
	        ms_ie = true;
	    }
		return ms_ie;
	};
</r:script>

</html>