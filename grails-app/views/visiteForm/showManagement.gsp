<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitStatus" %>
<%@ page import="org.afdb.rs.VisitType" %>


<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>
	
	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label', default:'Error')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<div class="row">		
		<div class="col-md-12">
			<h3 class="center">${message(code:'visiteForm.showManagement.title', default:'Request Management')}</h3>
			<br/>
		</div>
	</div>
	
<g:render template="/_menu/menuVisitForm"/>
<br/>
<section id="show-visiteForm" class="first">
	
	<div class="row">
	
		<div class="col-md-6">
			<table class="table">
				<tbody>
				
					<tr class="prop">
						<td valign="top" class="name" width="200"><g:message code="visiteForm.askBy.label" default="Ask By" /></td>
						<td valign="top" class="value">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								<strong>${visiteFormInstance?.askBy?.lastName} ${visiteFormInstance?.askBy?.firstName}</strong>
							</a>
							<div id="collapseOne" class="panel-collapse collapse">
								<g:render template="/staff/staffDetails" model="[staffInstance:visiteFormInstance?.askBy]"/>
		                    </div>
						</td>
					</tr>
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></td>
						<td valign="top" class="value">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								<strong>${visiteFormInstance?.receiverVisit?.lastName} ${visiteFormInstance?.receiverVisit?.firstName}</strong>
							</a>
							<div id="collapseTwo" class="panel-collapse collapse">
								<g:render template="/staff/staffDetails" model="[staffInstance:visiteFormInstance?.receiverVisit]"/>
		                    </div>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitType.label" default="Type of Visit" /></td>
						<td valign="top" class="value">
							<g:visitType type="${visiteFormInstance?.visitType}"/>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="location.label" default="Location" /></td>
						<td valign="top" class="value">
							${fieldValue(bean: visiteFormInstance, field: "location")}
						</td>
					</tr>
					
					<g:if test="${visiteFormInstance?.visitType?.code == VisitType.BUSINESS}">
						<tr class="prop">
							<td valign="top" class="name"><g:message code="visiteForm.company.label" default="Company" /></td>
							<td valign="top" class="value">
								<strong>${fieldValue(bean: visiteFormInstance, field: "company")}</strong>
							</td>
						</tr>
					</g:if>
					
					<g:if test="${visiteFormInstance?.visitType?.code == VisitType.EVENT}">
						<tr class="prop">
							<td valign="top" class="name"><g:message code="visiteForm.event.label" default="Event" /></td>
							<td valign="top" class="value">
								<strong>${fieldValue(bean: visiteFormInstance, field: "event")}</strong>
							</td>
						</tr>
					</g:if>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="status.label" default="Status" /></td>
						<td valign="top" class="value">
							<g:visitStatus status="${visiteFormInstance?.status}"/>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" colspan="2" class="name">
							<g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" />
							<div class="alert" role="alert">
								<strong>
									${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}
								</strong>
							</div>
						</td>
					</tr>
				
					<tr>
						<td colspan="2">
							<table class="table table-bordered margin-top-medium">
								<thead>
									<tr>							
										<th><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /></th>
										<g:if test="${!(visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY])}">
											<th><g:message code="visiteForm.expectedDepartureDateTime.label" default="Expected Departure Date Time" /></th>
										</g:if>
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]}">
											<th><g:message code="visiteForm.visitDuration.label" default="Visit Duration" /></th>
										</g:if>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="dd MMM yyyy"/></strong>
											<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="HH:mm"/></strong>
										</td>
										
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.BUSINESS, VisitType.EVENT]}">
											<td>
												<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="dd MMM yyyy"/></strong>
												<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="HH:mm"/></strong>
											</td>
										</g:if>
										<g:if test="${visiteFormInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]}">
											<td>
												<strong>${fieldValue(bean: visiteFormInstance, field: "visitDuration")}</strong>
											</td>				
										</g:if>				
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				
				</tbody>
			</table>
		</div>
		
		<div class="col-md-4">
						
			<table class="table">
				<tbody>
					<g:each in="${visiteFormInstance.visitors.sort{it.id}}" status="i" var="v">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<td>
								<strong>
									${v?.encodeAsHTML()}
								</strong>
							</td>
						</tr>	
					</g:each>		
				</tbody>
			</table>
		</div>
		
		<div class="col-md-2">
			<div class="col-md-12">
				<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.SUBMITTED]}">			
					<g:form method="post" class="form-horizontal" action="approved" name="submitForm">
						<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
						<button type="submit" class="btn btn-sm btn-success" id="validateId" onClick="pleaseWait(this);">
							<i class="glyphicon glyphicon-ok"></i>
							<span id="idOK">
								<strong><g:message code="button.approve.label" default="Approved"/></strong>
							</span>	
						</button>
					</g:form>
				</g:if>
				<g:elseif test="${visiteFormInstance?.status?.code in [VisitStatus.APPROVED]}">
					<button type="submit" disabled="disabled" class="btn btn-sm btn-success">
						<i class="glyphicon glyphicon-ok"></i>
						<strong><g:message code="button.approved.ok.message" default="Approved"/></strong>
					</button>
				</g:elseif>
			</div>
			<br/>
			<div class="col-md-12">
				<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.SUBMITTED]}">		
					<g:form method="post" class="form-horizontal" action="declined">
						<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />	
						<button type="submit" class="btn btn-sm btn-danger" id="declineId" onClick="pleaseWait(this);">
							<i class="glyphicon glyphicon-remove"></i>
							<span id="idOK">
								<strong><g:message code="button.decline.label" default="Declined"/></strong>
							</span>	
						</button>
					</g:form>
				</g:if>
				<g:elseif test="${visiteFormInstance?.status?.code in [VisitStatus.DECLINED]}">
					<button type="submit" disabled="disabled" class="btn btn-sm btn-danger">
						<i class="glyphicon glyphicon-remove"></i>
						<strong><g:message code="button.declined.ok.message" default="Declined"/></strong>
					</button>
				</g:elseif>				
			</div>
			
		</div>
	</div>

</section>

</body>

<r:script>
	function pleaseWait(btn){
		btn.disabled=true;
		btn.value='Please wait ...';
		document.getElementById('idOK').innerText='Please wait ...';		
		//alert(msg);
		if (msieversion()==false){ //CHROME OU Firefox
			document.getElementById('submitForm').submit();
		}
	};
	
	function msieversion() {
 		var ms_ie = false;
	    var ua = window.navigator.userAgent;
	    var old_ie = ua.indexOf('MSIE ');
	    var new_ie = ua.indexOf('Trident/');
	
	    if ((old_ie > -1) || (new_ie > -1)) {
	        ms_ie = true;
	    }
		return ms_ie;
	};
</r:script>

</html>