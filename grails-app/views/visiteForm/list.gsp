<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitType" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>
	
	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<g:render template="/_menu/menuVisitForm"/>
	<br/>
	
	<h3 class="center">${message(code:'visiteForm.list.title', default:'List of Visit Forms')}</h3>
	<br/>
	
	<div class="row">
	
		<div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation" class="${(codeVisitType == "INDIVIDUAL" )? 'active' : '' }">
					<g:link controller="visiteForm" action="list" params="[visitType:VisitType.INDIVIDUAL]"><strong>${message(code:'visitor.list.personal.label', default:'Personal')}</strong></g:link>
				</li>
								
				<li role="presentation" class="${(codeVisitType == "FAMILY" )? 'active' : '' }">
					<g:link controller="visiteForm" action="list" params="[visitType:VisitType.FAMILY]"><strong>${message(code:'visitor.list.family.label', default:'Family')}</strong></g:link>
				</li>	
				
				<li role="presentation" class="${(codeVisitType == "BUSINESS" )? 'active' : '' }">
					<g:link controller="visiteForm" action="list" params="[visitType:VisitType.BUSINESS]"><strong>${message(code:'visitor.list.business.label', default:'Business')}</strong></g:link>
				</li>
				
				<li role="presentation" class="${(codeVisitType == "EVENT" )? 'active' : '' }">
					<g:link controller="visiteForm" action="list" params="[visitType:VisitType.EVENT]"><strong>${message(code:'visitor.list.event.label', default:'Event')}</strong></g:link>
				</li>
			</ul>
		</div>
		
		<div class="col-md-10">
			<g:render template="list"/>
		</div>
	
	</div>

</body>

</html>
