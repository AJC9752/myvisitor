<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.Staff" %>
    
<g:set var="lang" value="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE' ?: org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().substring(0,2)}"/>
	
	<div class="row">
	
		<div class="col-md-6">
			
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body form-horizontal payment-form">
							
							<div class="form-group">
								<div class="col-sm-12">
									<strong><g:message code="visiteForm.visitType.info.message" default="Veuillez selectionner le type de visite." /></strong>
								</div>
								<br/>
								<label for=visitType class="col-sm-4 control-label"><g:message code="visitType.label" default="Type of Visit" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:if test="${visiteFormInstance?.visitType}">
										<g:select disabled class="form-control" 
											id="visitType" name="visitType.id" 
											from="${org.afdb.rs.VisitType.list()}"
											optionKey="id" required=""
											value="${visiteFormInstance?.visitType?.id}"
											optionValue="${{it?.getName(lang.toString())}}"
											style="width:70%;" noSelection="${[null:'Select One...']}" />
									</g:if>
									<g:else>
										<g:select class="form-control" 
											id="visitType" name="visitType.id" 
											from="${org.afdb.rs.VisitType.list()}"
											optionKey="id" required=""
											value="${visiteFormInstance?.visitType?.id}"
											optionValue="${{it?.getName(lang.toString())}}"
											onChange="toggle('#testDiv', this)"
											style="width:70%;" noSelection="${[null:'Select One...']}" />
										<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'visitType', 'error')}</span>
									</g:else>									
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body form-horizontal payment-form">
					
							<div class="form-group">
								<label for="askBy" class="col-sm-4 control-label"><g:message code="visiteForm.askBy.label" default="Ask By" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<span class="label label-default">${visiteFormInstance?.askBy?.lastName} ${visiteFormInstance?.askBy?.firstName}</span>
								</div>							
							</div>
					
							<g:hiddenField  value="${visiteFormInstance?.askBy?.id}" id="askBy" name="askBy.id" />
							<g:hiddenField  value="${visiteFormInstance?.status?.id}" id="status" name="status.id" />
												
							<div class="form-group">
								<label for="purposeOfVisite" class="col-sm-4 control-label"><g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:textArea class="form-control" name="purposeOfVisite" value="${visiteFormInstance?.purposeOfVisite}" maxlength="1024" cols="50" rows="5"/>
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'purposeOfVisite', 'error')}</span>
								</div>
							</div>
							
							<div class="form-group">
								<label for="expectedArrivingDateTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<div class="row">
										<div class="col-sm-7">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1">
													<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
												</span>
												<bs:datePicker class="form-control" name="expectedArrivingDateTime_date" id="expectedArrivingDateTime-date" value="${expectedArrivingDateTime_date}"/>
											</div>
										</div>
										<div class="col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
											    <input class="form-control" name="expectedArrivingDateTime_time" id="expectedArrivingDateTime-time" value="${expectedArrivingDateTime_time}" type="text">
											    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
											</div>
										</div>
									</div>
								</div>
							</div>
																	
							<div id="idExpectedDeparture">
								<div class="form-group registration-date">
									<label for="expectedDepartureDateTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedDepartureDateTime.label" default="Expected Departure Date Time" /><span class="required-indicator">*</span></label>
									<div class="col-sm-8">
										<div class="row">
											<div class="col-sm-7">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
													</span>
													<bs:datePicker class="form-control" name="expectedDepartureDateTime_date" id="expectedDepartureDateTime-date-date" value="${expectedDepartureDateTime_date}"/>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="input-group bootstrap-timepicker timepicker">
												    <input class="form-control" name="expectedDepartureDateTime_time" id="expectedDepartureDateTime-time" value="${expectedDepartureDateTime_time}" type="text">
												    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							
							<div id="idVisitDuration">
								<div class="form-group">
									<div class="col-sm-12">
										<strong><g:message code="visiteForm.visitDuration.info.message" default="Veuillez entrer une estimation de la durée de la visite" /></strong><br/>
									</div>
									<label for="visitDuration" class="col-sm-4 control-label"><g:message code="visiteForm.visitDuration.label" default="Visit Duration" /><span class="required-indicator">*</span></label>
									<div class="col-sm-7">
										<g:field class="form-control" type="number" name="visitDuration" min="1" max="10" value="${visiteFormInstance.visitDuration}" style="width:70%;"/>
										${message(code:'inhour.label', default:'in hour(s)')}
										<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"visitDuration"', 'error')}</span>
									</div>
									<div class="col-sm-1"></div>
								</div>
							</div>
							
							<%-- 
							<div class="form-group">
								<label for="expectedArrivingTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedArrivingTime.label" default="Expected Arriving Time" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:field class="form-control" type="number" name="expectedArrivingTime" required="" value="${visiteFormInstance.expectedArrivingTime}" style="width:70%;"/>
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedArrivingTime"', 'error')}</span>
								</div>
							</div>
							
							<div class="form-group">
								<label for="expectedDepartureTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedDepartureTime.label" default="Expected Departure Time" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:field class="form-control" type="number" name="expectedDepartureTime" required="" value="${visiteFormInstance.expectedDepartureTime}" style="width:70%;"/>
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedDepartureTime"', 'error')}</span>
								</div>
							</div>
							--%>
						</div>
					</div>
				</div>
			</div>
		
		</div>
		
		<div class="col-md-6">
			
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body form-horizontal payment-form">
							
							<div class="form-group">
								<div class="col-sm-12">
									<strong><g:message code="visiteForm.receiverVisit.info.message" default="Saisissez le nom du récipiendaire de la visite, si différent du demandeur." /></strong>
								</div>
								<br/>
								<label for="receiverVisit" class="col-sm-4 control-label"><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></label>
								<div class="col-sm-8">
									<input type='text' name='receiver' class='staffs form-control' autocomplete="off" value="${visiteFormInstance?.receiverVisit}"  style="width:100%"/>
									<span style="font-size:12px" class="txt-grey"><g:message code="visiteForm.receiverVisit.message" default="Si aucun nom ne s'affiche, contactez un admin" /></span>
								</div>
							</div>
						</div>
					</div>
				</div>			

				<div class="col-md-12">
				
					<div class="panel panel-default">
						<div class="panel-body form-horizontal payment-form">
						
							<div class="form-group">
								<label for=location class="col-sm-4 control-label"><g:message code="location.label" default="Location" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:select class="form-control" 
										id="location" name="location.id" 
										from="${org.afdb.rs.Location.list()}"
										optionKey="id" required=""
										value="${visiteFormInstance?.location?.id}"
										style="width:70%;" noSelection="${[null:'Select One...']}" />
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'location', 'error')}</span>
								</div>
							</div>
							
							<div id="idIndividual">
								<h3 class="">${message(code:'visitor.title', default:'Visitor')}</h3>
		
								<div class="form-group">
									<label for="gender" class="col-sm-4 control-label"><g:message code="visitor.gender.label" default="Gender" /><span class="required-indicator">*</span></label>
									<div class="col-sm-8">
										<g:select class="form-control" name="gender" from="${visitorInstance.constraints.gender.inList}" value="${visitorInstance?.gender}" valueMessagePrefix="visitor.gender" noSelection="['': '']" style="width:20%;"/>
										<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'gender', 'error')}</span>
									</div>
								</div>
				
								<div class="form-group">
									<label for="firstName" class="col-sm-4 control-label"><g:message code="visitor.firstName.label" default="First Name" /><span class="required-indicator">*</span></label>
									<div class="col-sm-8">
										<g:textField class="form-control" name="firstName" value="${visitorInstance?.firstName}" style="width:100%;"/>
										<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'firstName', 'error')}</span>
									</div>
								</div>
								
								<div class="form-group">
									<label for="lastName" class="col-sm-4 control-label"><g:message code="visitor.lastName.label" default="Last Name" /><span class="required-indicator">*</span></label>
									<div class="col-sm-8">
										<g:textField class="form-control" name="lastName" value="${visitorInstance?.lastName}" style="width:100%;"/>
										<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'lastName', 'error')}</span>
									</div>
								</div>
							</div>
							
							<div id="idBusiness" class="form-group">
								<label for="company" class="col-sm-4 control-label"><g:message code="visiteForm.company.label" default="Company" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:textField class="form-control" name="company" value="${visiteFormInstance?.company}"/>
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'company', 'error')}</span>
								</div>
							</div>
							
							<div id="idEvent" class="form-group">
								<label for="event" class="col-sm-4 control-label"><g:message code="visiteForm.event.label" default="Event" /><span class="required-indicator">*</span></label>
								<div class="col-sm-8">
									<g:textField class="form-control" name="event" value="${visiteFormInstance?.event}"/>
									<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'event', 'error')}</span>
								</div>
							</div>
						
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
		
	</div>

	<script type="text/javascript">
		
		var staffs = [
			<g:each in="${Staff.list()}" status="i" var="staff">
				"${staff?.perno} - ${staff?.lastName} ${staff?.firstName}",
			</g:each>
		];
		$(".staffs").autocomplete({
			source: staffs
		});

		$('#expectedArrivingDateTime-time').timepicker({
			maxHours: 24,
			showMeridian: false 
		});

		$('#expectedDepartureDateTime-time').timepicker({
			maxHours: 24,
			showMeridian: false 
		});
	</script>

	