<%@ page import="org.afdb.rs.VisiteForm" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	
	<%-- 	
	<link type="text/css" rel="stylesheet" href="${(resource(dir: 'css', file: 'bootstrap.min.css')}" />
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	--%>
	<link type="text/css" rel="stylesheet" href="${(resource(dir: 'css', file: 'bootstrap-timepicker.min.css'))}" />
	<g:javascript src="bootstrap-timepicker.min.js"/>
	
	<script type="text/javascript">
	
		function toggle(className, obj) {	
			//alert('Hello');		
		    var $select = $(obj);
		    var user_cat = $('#visitType').find(":selected").val();
		  	//alert('user44:'+user_cat);
		    if (user_cat=='1')  {//INDIVIDUAL
		    	//alert('INDIVIDUAL');
		    	$("#idIndividual").fadeOut().fadeIn();
		    	$("#idBusiness").hide();
		    	$('#idEvent').hide();
		    	$("#idVisitDuration").fadeOut().fadeIn();
		    	$('#idExpectedDeparture').hide();
			}else if (user_cat=='2')  {
				//alert('BUSINESS');
				$("#idIndividual").hide();
				$("#idBusiness").fadeOut().fadeIn();
				$('#idEvent').hide();
				$('#idVisitDuration').hide();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}else if (user_cat=='3')  {
				//alert('EVENT');
				$("#idIndividual").hide();
				$("#idBusiness").hide();
				$('#idEvent').fadeOut().fadeIn();
				$('#idVisitDuration').hide();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}else if (user_cat=='4')  {//FAMILY
		    	//alert('FAMILY');
		    	$("#idIndividual").fadeOut().fadeIn();
		    	$("#idBusiness").hide();
		    	$('#idEvent').hide();
		    	$("#idVisitDuration").fadeOut().fadeIn();
		    	$('#idExpectedDeparture').hide();
			}else{
				$("#idIndividual").hide();
				$("#idBusiness").hide();
				$('#idEvent').hide();
				$('#idVisitDuration').fadeOut().fadeIn();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}
		}
	</script>

	<r:script>
	 $('#myTab a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		});
		
		function toggle(obj) {	
		    var $select = $(obj);
		    var user_cat = $('#visitType').find(":selected").val();
		    //alert('user44:'+user_cat);	   
		   if (user_cat=='1')  {//INDIVIDUAL
		    	//alert('INDIVIDUAL');
		    	$("#idIndividual").fadeOut().fadeIn();
		    	$("#idBusiness").hide();
		    	$('#idEvent').hide();
		    	$("#idVisitDuration").fadeOut().fadeIn();
		    	$('#idExpectedDeparture').hide();
			}else if (user_cat=='2')  {
				//alert('BUSINESS');
				$("#idIndividual").hide();
				$("#idBusiness").fadeOut().fadeIn();
				$('#idEvent').hide();
				$('#idVisitDuration').hide();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}else if (user_cat=='3')  {
				//alert('EVENT');
				$("#idIndividual").hide();
				$("#idBusiness").hide();
				$('#idEvent').fadeOut().fadeIn();
				$('#idVisitDuration').hide();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}else if (user_cat=='4')  {//FAMILY
		    	//alert('FAMILY');
		    	$("#idIndividual").fadeOut().fadeIn();
		    	$("#idBusiness").hide();
		    	$('#idEvent').hide();
		    	$("#idVisitDuration").fadeOut().fadeIn();
		    	$('#idExpectedDeparture').hide();
			}else{
				$("#idIndividual").hide();
				$("#idBusiness").hide();
				$('#idEvent').hide();
				$('#idVisitDuration').fadeOut().fadeIn();
				$('#idExpectedDeparture').fadeOut().fadeIn();
			}
		  return true;
		};
		
		$(document).ready(function() {		
			toggle('#visitType');
		});
	</r:script>
	
	<r:require module="jquery-ui"/>
	
</head>

<body>

	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<g:render template="/_menu/menuVisitForm"/>
	<br/>

	<div class="row">		
		<div class="col-md-12">
			<h3 class="center">${message(code:'visiteForm.form.title', default:'List of Visit Forms')}</h3>
			<br/>
		</div>
	</div>
	
	<section id="edit-visiteForm" class="first">
	
		<g:hasErrors bean="${visiteFormInstance}">
		<div class="alert alert-error">
			<g:renderErrors bean="${visiteFormInstance}" as="list" />
		</div>
		</g:hasErrors>
	
		<g:form method="post" class="form-horizontal" >
			<g:hiddenField name="id" value="${visiteFormInstance?.id}" />
			<g:hiddenField name="version" value="${visiteFormInstance?.version}" />
			<fieldset class="form">
				<g:render template="form"/>
			</fieldset>
			<div class="form-actions">
				<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.save.label', default: 'Update')}" />
				<%-- 
				<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
	            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	            --%>
			</div>
		</g:form>
	
	</section>
			
</body>

</html>
