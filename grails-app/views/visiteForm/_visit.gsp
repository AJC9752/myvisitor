	
	<div class="panel panel-default">
		<div class="panel-body form-horizontal payment-form">
			
			<div class="form-group">
				<label for="askBy" class="col-sm-4 control-label"><g:message code="visiteForm.askBy.label" default="Ask By" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<g:select class="form-control" id="askBy" name="askBy.id" from="${org.afdb.Staff.list()}" optionKey="id" required="" value="${visiteFormInstance?.askBy?.id}" style="width:70%;"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'askBy', 'error')}</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="receiverVisit" class="col-sm-4 control-label"><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<g:select class="form-control" id="receiverVisit" name="receiverVisit.id" from="${org.afdb.Staff.list()}" optionKey="id" required="" value="${visiteFormInstance?.receiverVisit?.id}" style="width:70%;"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'receiverVisit', 'error')}</span>
				</div>
			</div>

			<g:hiddenField  value="${visiteFormInstance?.status?.id}" id="status" name="status.id" />
			
			<g:if test="${visitType == 'BUSINESS' }">
				<div class="form-group">
					<label for="company" class="col-sm-4 control-label"><g:message code="visiteForm.company.label" default="Company" /><span class="required-indicator">*</span></label>
					<div class="col-sm-8">
						<g:textField class="form-control" name="company" value="${visiteFormInstance?.company}" style="width:75%;"/>
						<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'company', 'error')}</span>
					</div>
				</div>
			</g:if>
			
			<g:if test="${visitType == 'EVENT' }">
				<div class="form-group">
					<label for="event" class="col-sm-4 control-label"><g:message code="visiteForm.event.label" default="Event" /><span class="required-indicator">*</span></label>
					<div class="col-sm-8">
						<g:textField class="form-control" name="event" value="${visiteFormInstance?.event}" style="width:75%;"/>
						<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'event', 'error')}</span>
					</div>
				</div>
			</g:if>
			
			<div class="form-group">
				<label for="purposeOfVisite" class="col-sm-4 control-label"><g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<g:textArea class="form-control" name="purposeOfVisite" value="${visiteFormInstance?.purposeOfVisite}" maxlength="1024" cols="50" rows="5"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'purposeOfVisite', 'error')}</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="expectedArrivingDateTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<bs:datePicker name="expectedArrivingDateTime" precision="day"  value="${visiteFormInstance?.expectedArrivingDateTime}"  />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedArrivingDateTime"', 'error')}</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="expectedArrivingTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedArrivingTime.label" default="Expected Arriving Time" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<g:field class="form-control" type="number" name="expectedArrivingTime" required="" value="${visiteFormInstance.expectedArrivingTime}" style="width:70%;"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedArrivingTime"', 'error')}</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="expectedDepartureDateTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedDepartureDateTime.label" default="Expected Departure Date Time" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<bs:datePicker name="expectedDepartureDateTime" precision="day"  value="${visiteFormInstance?.expectedDepartureDateTime}"  />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedDepartureDateTime"', 'error')}</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="expectedDepartureTime" class="col-sm-4 control-label"><g:message code="visiteForm.expectedDepartureTime.label" default="Expected Departure Time" /><span class="required-indicator">*</span></label>
				<div class="col-sm-8">
					<g:field class="form-control" type="number" name="expectedDepartureTime" required="" value="${visiteFormInstance.expectedDepartureTime}" style="width:70%;"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: '"expectedDepartureTime"', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'actualArrivingDateTime', 'error')} ">
				<label for="actualArrivingDateTime" class="control-label"><g:message code="visiteForm.actualArrivingDateTime.label" default="Actual Arriving Date Time" /></label>
				<div class="controls">
					<bs:datePicker name="actualArrivingDateTime" precision="day"  value="${visiteFormInstance?.actualArrivingDateTime}" default="none" noSelection="['': '']" />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'actualArrivingDateTime', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'actualDepartureDateTime', 'error')} ">
				<label for="actualDepartureDateTime" class="control-label"><g:message code="visiteForm.actualDepartureDateTime.label" default="Actual Departure Date Time" /></label>
				<div class="controls">
					<bs:datePicker name="actualDepartureDateTime" precision="day"  value="${visiteFormInstance?.actualDepartureDateTime}" default="none" noSelection="['': '']" />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'actualDepartureDateTime', 'error')}</span>
				</div>
			</div>
			

			<%-- 
			<g:hiddenField name="caseId" value="${caseId}" />
			--%>
		</div>
	</div>