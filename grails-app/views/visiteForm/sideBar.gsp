<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>

	<div class="w3-sidebar w3-bar-block w3-black w3-xxlarge" style="width:70px">
	  <a href="#" class="w3-bar-item w3-button"><i class="fa fa-home"></i></a> 
	  <a href="#" class="w3-bar-item w3-button"><i class="fa fa-search"></i></a> 
	  <a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a> 
	  <a href="#" class="w3-bar-item w3-button"><i class="fa fa-globe"></i></a>
	  <a href="#" class="w3-bar-item w3-button"><i class="fa fa-trash"></i></a> 
	</div>
	
	<div style="margin-left:70px">
	
		<div class="w3-container">
			<h2>Sidebar Icons</h2>
			<p>In this example we have used icons in the sidebar.</p>
		</div>
	
	</div>
      
</body>
</html>

<%-- 

<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitType" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><g:message code="sideBar.list.label" default="SideBar" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'sidebar.css')}" type="text/css">
	
</head>

<body>

	<g:render template="/_menu/sidebar"/>

</body>

</html>
--%>