
<g:form action="saveIndividual" class="form-horizontal" >
	<div class="row">
	
		<div class="col-md-6">
			<g:render template="visit"/>
		</div>
		
		<div class="col-md-6">
			
			<div class="control-group fieldcontain ${hasErrors(bean: visitorInstance, field: 'fullName', 'error')} ">
				<label for="fullName" class="control-label"><g:message code="visitor.fullName.label" default="Full Name" /></label>
				<div class="controls">
					<g:textField class="form-control" name="fullName" value="${visiteFormInstance?.visitors?.fullName}" style="width:70%;"/>
					<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'fullName', 'error')}</span>
				</div>
			</div>
									
		</div>
		
	</div>
	
	<div class="form-actions">
		<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
        <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
		
</g:form>