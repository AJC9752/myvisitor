<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitType" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<title><g:message code="visiteForm.dashboard.title" default="Dashboard" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	<g:javascript src="bootstrap-datetimepicker.js"/>
</head>

<body>
	
	<g:render template="/_menu/menuVisitForm"/>
	<br/>
	
	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<h3 class="center">${message(code:'visiteForm.dashboard.title', default:'Dashboard')}</h3>
	<br/>
	
	<div class="row">
	
		<div class="col-md-12">
			<ul class="nav nav-pills nav-justified">
				<li role="presentation" class="${(codeVisitorNumber == "THISDAY" )? 'active' : '' }"> 
					<g:link controller="visiteForm" action="dashboard" params="[visitorNumber:'THISDAY']">
						<strong>${message(code:'visiteForm.dashboard.current.label', default:'Current Visitors')}</strong>
					</g:link>
				</li>
								
				<li role="presentation" class="${(codeVisitorNumber == "BYDAY" )? 'active' : '' }"> 
					<g:link controller="visiteForm" action="dashboard" params="[visitorNumber:'BYDAY']">
						<strong>${message(code:'visiteForm.dashboard.day.label', default:'Visitors of the Period - Day')}</strong>
					</g:link>
				</li>	
				
				<li role="presentation" class="${(codeVisitorNumber == "MONTH" )? 'active' : '' }">
					<g:link controller="visiteForm" action="dashboard" params="[visitorNumber:'MONTH']">
						<strong>${message(code:'visiteForm.dashboard.month.label', default:'Visitors of the Period - Month')}</strong>
					</g:link>
				</li>
			</ul>
			<br/>
			<div class="tab-content">
				<g:if test="${codeVisitorNumber == 'THISDAY'}">
					<h3 class="center">${message(code:'visitorsByBuilding.list.label', default:'Nombre de Visiteur par Batiment')}</h3>
					<br/>
					<div class="row">
						<div class="col-md-6">
							<div class="center">
								<legend>${message(code:'dashboard.visitor.current.label', default:'Visiteur(s) actuel(s)')}</legend>
							</div>
							<div class="row">
								<div class="col-md-12">
									<g:render template="/visitorsByBuilding/tableVisitorsByBuilding"/>
								</div>
								<div class="col-md-12">
									<g:render template="/visitorsByBuilding/chartVisitorsByBuilding"/>	
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="center">
								<legend>${message(code:'dashboard.visitor.thisday.label', default:'Visiteur(s) de la journée')}</legend>
							</div>
							<div class="row">
								<div class="col-md-12">
									<g:render template="/visitorsInDay/tableVisitorsInDay"/>
								</div>
								<div class="col-md-12">
									<g:render template="/visitorsInDay/chartVisitorsInDay"/>
								</div>
							</div>
						</div>
					</div>
				</g:if>
				
				<g:elseif test="${codeVisitorNumber == 'BYDAY'}">
					<h3 class="center">${message(code:'visitorsInDay.list.label', default:'Nombre de Visiteur pour la période Jour')}</h3>
					<br/>
					<div class="row">
						<div class="col-md-12">
							<g:render template="/visitorsPerPeriod/selectionDay"/>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<g:render template="/visitorsPerPeriod/tableVisitorsPerPeriod"/>
								</div>
								<div class="col-md-12">
									<g:render template="/visitorsPerPeriod/chartVisitorsPerPeriod"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							
						</div>
					</div>
				</g:elseif>
				
				<g:elseif test="${codeVisitorNumber == 'MONTH'}">
					<h3 class="center">${message(code:'visitorsPerPeriod.list.label', default:'Nombre de Visiteur dans la Période')}</h3>
					<br/>
					<div class="row">
						<div class="col-md-12">
							<g:render template="/visitorsPerPeriod/selectionMonthly"/>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<g:render template="/visitorsPerPeriod/tableVisitorsPerPeriod"/>
								</div>
								<div class="col-md-12">
									<g:render template="/visitorsPerPeriod/chartVisitorsPerPeriod"/>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							
						</div>
					</div>			
							
				</g:elseif>
				
			</div>
			
		</div>

	</div>

</body>

</html>
