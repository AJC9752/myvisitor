
<%@ page import="org.afdb.rs.VisiteForm" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-visiteForm" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<th><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></th>
			
				<th><g:message code="visiteForm.askBy.label" default="Ask By" /></th>
			
				<th><g:message code="visiteForm.status.label" default="Status" /></th>
				<th><g:message code="visiteForm.visitType.label" default="Type of Visit" /></th>
			
				<g:sortableColumn property="purposeOfVisite" title="${message(code: 'visiteForm.purposeOfVisite.label', default: 'Purpose Of Visite')}" />
			
				<g:sortableColumn property="expectedArrivingDateTime" title="${message(code: 'visiteForm.expectedArrivingDateTime.label', default: 'Expected Arriving Date Time')}" />
			
				<g:sortableColumn property="visitDuration" title="${message(code: 'visiteForm.visitDuration.label', default: 'Visit Duration')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${visiteFormInstanceList}" status="i" var="visiteFormInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${visiteFormInstance.id}">${fieldValue(bean: visiteFormInstance, field: "receiverVisit")}</g:link></td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "askBy")}</td>
			
				<td><g:visitStatus status="${visiteFormInstance?.status}"/></td>
				
				<td><g:visitType type="${visiteFormInstance?.visitType}"/></td>
				
				<td>${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}</td>
			
				<td><g:formatDate date="${visiteFormInstance.expectedArrivingDateTime}" /></td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "visitDuration")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${visiteFormInstanceTotal}" />
	</div>
</section>

</body>

</html>
