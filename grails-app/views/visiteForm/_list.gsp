<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitStatus" %>
<%@ page import="org.afdb.rs.VisitType" %>

<section id="list-visiteForm" class="first">
	
	<%-- 
	<table class="table table-bordered margin-top-medium">
	--%>
	<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
			
				<th><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></th>
			
				<th><g:message code="visiteForm.askBy.label" default="Requested By" /></th>
			
				<th><g:message code="visiteForm.status.label" default="Status" /></th>
				<%--
				<th><g:message code="visitType.label" default="Type of Visit" /></th>
				--%>		
				<g:sortableColumn property="expectedArrivingDateTime" title="${message(code: 'visiteForm.expectedArrivingDateTime.label', default: 'Expected Arriving Date Time')}" params="${[visitType:codeVisitType]}"/>
				<g:if test="${codeVisitType in ["BUSINESS", "EVENT"]}">
					<g:sortableColumn property="expectedDepartureDateTime" title="${message(code: 'visiteForm.expectedDepartureDateTime.label', default: 'Expected Departure Date Time')}" params="${[visitType:codeVisitType]}"/>
				</g:if>
				<g:if test="${codeVisitType in ["INDIVIDUAL", "FAMILY"]}">
					<g:sortableColumn property="visitDuration" title="${message(code: 'visiteForm.visitDuration.label', default: 'Visit Duration')}" params="${[visitType:codeVisitType]}"/>
				</g:if>
			</tr>
		</thead>
		<tbody>
		<g:each in="${visiteFormInstanceList}" status="i" var="visiteFormInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					<strong>${visiteFormInstance?.receiverVisit?.lastName} ${visiteFormInstance?.receiverVisit?.firstName}</strong>
				</td>
				<td>
					<strong>${visiteFormInstance?.askBy?.lastName} ${visiteFormInstance?.askBy?.firstName}</strong>
				</td>
				<td><g:visitStatus status="${visiteFormInstance?.status}"/></td>
				<%-- 
				<td><g:visitType type="${visiteFormInstance?.visitType}"/></td>
				--%>
				<td>
					<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="dd MMM yyyy"/></strong>
					<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="HH:mm"/></strong>
				</td>
				<g:if test="${codeVisitType in ["BUSINESS", "EVENT"]}">
					<td>
						<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="dd MMM yyyy"/></strong>
						<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="HH:mm"/></strong>
					</td>
				</g:if>
				<g:if test="${codeVisitType in ["INDIVIDUAL", "FAMILY"]}">
					<td>${fieldValue(bean: visiteFormInstance, field: "visitDuration")}</td>
				</g:if>
				<td width=50>
					<g:link class="btn btn-sm btn-default" controller="visiteForm" action="show" id="${visiteFormInstance.id}">
						<i class="glyphicon glyphicon-share"></i>
					</g:link>
				</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${visiteFormInstanceTotal}" params="${[visitType:codeVisitType]}"/>
	</div>
</section>