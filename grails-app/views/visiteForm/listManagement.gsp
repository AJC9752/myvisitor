<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitStatus" %>
<%@ page import="org.afdb.rs.VisitType" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
	<g:render template="/_menu/menuVisitForm"/>
	<br/>
	
	<div class="row">		
		<div class="col-md-12">
			<h3 class="center">${message(code:'visiteForm.listManagement.title', default:'Request Management')}</h3>
			<br/>
		</div>
	</div>
	
	<div class="pull-right">	
		<g:render template="buttonListApproval"/>
		<br/><br/>
	</div>
	
	<section id="list-visiteForm" class="first">
		<%-- 
		<ul class="nav nav-pills nav-justified">
			<li role="presentation" class="active"><a href="#"><strong>Personal</strong></a></li>
			<li role="presentation"><a href="#"><strong>Business</strong></a></li>
			<li role="presentation"><a href="#"><strong>Event</strong></a></li>
		</ul>
		--%>
		<table class="table table-bordered margin-top-medium">
			<tbody>
			<g:each in="${visiteFormInstanceList}" status="i" var="visiteFormInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td>
						<div class="row">
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-6">
										Receiver Visit: <strong>${visiteFormInstance?.receiverVisit?.lastName} ${visiteFormInstance?.receiverVisit?.firstName}</strong>
										<br/>
										Status: <g:visitStatus status="${visiteFormInstance?.status}"/>
										<br/>
										Expected Arriving Date Time:	<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="dd MMM yyyy"/></strong>
																		<strong><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" format="HH:mm"/></strong>
										<br/>
										Visit Duration: ${fieldValue(bean: visiteFormInstance, field: "visitDuration")}
									</div>
									<div class="col-md-6">
										Request By: <strong>${visiteFormInstance?.askBy?.lastName} ${visiteFormInstance?.askBy?.firstName}</strong>
										<br/>
										Type of Visit: <g:visitType type="${visiteFormInstance?.visitType}"/>
										<br/>
										Expected Departure Date Time: 	<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="dd MMM yyyy"/></strong>
																		<strong><g:formatDate date="${visiteFormInstance?.expectedDepartureDateTime}" format="HH:mm"/></strong>
									</div>
								</div>
								<br/>
								<g:if test="${visiteFormInstance?.visitType?.code == VisitType.BUSINESS}">
									<g:message code="visiteForm.company.label" default="Company" />: <strong>${fieldValue(bean: visiteFormInstance, field: "company")}</strong>
								</g:if>
								<g:if test="${visiteFormInstance?.visitType?.code == VisitType.EVENT}">
									<g:message code="visiteForm.event.label" default="Event" />: <strong>${fieldValue(bean: visiteFormInstance, field: "event")}</strong>
								</g:if>
								<br/>
								Purpose of visit: ${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}
							</div>
							<div class="col-md-2">
								<div class="col-md-3">
									<g:form method="post" class="form-horizontal" action="approved" name="submitForm">
										<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
																
										<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.SUBMITTED]}">			
											<button type="submit" class="btn btn-sm btn-success" id="validateId" onClick="pleaseWait(this);">
												<i class="glyphicon glyphicon-ok"></i>
												<span id="idOK">
												</span>	
											</button>
										</g:if>
										<g:else>
											<button type="submit" disabled="disabled" class="btn btn-sm btn-success">
												<i class="glyphicon glyphicon-ok"></i>
											</button>
										</g:else>
									</g:form>
								</div>
								<div class="col-md-3">
									<g:form method="post" class="form-horizontal" action="declined">
										<g:hiddenField  value="${visiteFormInstance?.id}" id="visit" name="visit.id" />
																
										<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.SUBMITTED]}">			
											<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('${message(code: 'default.button.reset.submission.confirm.message', default: 'Are you sure?')}');">
												<i class="glyphicon glyphicon-remove"></i>
											</button>
										</g:if>
										<g:else>
											<button type="submit" disabled="disabled" class="btn btn-sm btn-danger">
												<i class="glyphicon glyphicon-remove"></i>
											</button>
										</g:else>
									</g:form>
								</div>
								<div class="col-md-6">
									<g:link class="btn btn-sm btn-default" controller="${params.controller}" action="showManagement" id="${visiteFormInstance.id}">
										<i class="glyphicon glyphicon-plus"></i> 
									</g:link>
								</div>
							</div>
						</div>
						
					</td>			
				</tr>
			</g:each>
			</tbody>
		</table>
		<div class="container">
			<bs:paginate total="${visiteFormInstanceTotal}" 
				params="${['status':status.code]}"/>
		</div>
	</section>

</body>

</html>
