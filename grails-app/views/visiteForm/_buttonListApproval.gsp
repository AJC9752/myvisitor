<%@ page import="org.afdb.rs.VisitStatus" %>

<div class="btn-group" role="group">	
	<g:link class="btn btn-sm btn-default ${(params.controller == "visiteForm" && params.action == "listManagement" && status.code == VisitStatus.SUBMITTED)? 'active' : '' }" action="listManagement" controller="${params.controller}" params="[status:VisitStatus.SUBMITTED]">
		<i class="glyphicon glyphicon-share"></i> 
		<strong>${message(code:'button.list.submitted.label', default:'Demande À Approuver')}</strong>
	</g:link>
	<g:link class="btn btn-sm btn-success ${(params.controller == "visiteForm" && params.action == "listManagement" && status.code == VisitStatus.APPROVED)? 'active' : '' }" action="listManagement" controller="${params.controller}" params="[status:VisitStatus.APPROVED]">
		<i class="glyphicon glyphicon-check"></i> 
		<strong>${message(code:'button.list.approved.label', default:'Demande Approuvé')}</strong>
	</g:link>
	<g:link class="btn btn-sm btn-danger ${(params.controller == "visiteForm" && params.action == "listManagement" && status.code == VisitStatus.DECLINED)? 'active' : '' }" action="listManagement" controller="${params.controller}" params="[status:VisitStatus.DECLINED]">
		<i class="glyphicon glyphicon-remove"></i> 
		<strong>${message(code:'button.list.declined.label', default:'Demande Refusé')}</strong>
	</g:link>
</div>

<%-- 
<g:form controller="visiteForm" action="listManagement">
	<div class="btn-group" role="group" aria-label="Basic example">
		<button type="button" class="btn btn-sm btn-default">
			<i class="glyphicon glyphicon-share"></i> 
			<strong>${message(code:'button.list.submitted.label', default:'Demande À Approuver')}</strong>
		</button>
		<button type="button" class="btn btn-sm btn-success">
			<i class="glyphicon glyphicon-check"></i> 
			<strong>${message(code:'button.list.approved.label', default:'Demande Approuvé')}</strong>
		</button>
		<button type="button" class="btn btn-sm btn-danger">
			<i class="glyphicon glyphicon-remove"></i> 
			<strong>${message(code:'button.list.declined.label', default:'Demande Refusé')}</strong>
		</button>
	</div>
</g:form>
--%>
