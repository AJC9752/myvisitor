<%@ page import="org.afdb.rs.VisiteForm" %>



			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'surname', 'error')}">
				<label for="surname" class="control-label"><g:message code="visiteForm.surname.label" default="Surname" /></label>
				<div class="controls ">
					<g:textField class="form-control" name="surname" value="${visiteFormInstance?.surname}" />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'surname', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'firstname', 'error')} ">
				<label for="firstname" class="control-label"><g:message code="visiteForm.firstname.label" default="Firstname" /></label>
				<div class="controls">
					<g:textField class="form-control" name="firstname" value="${visiteFormInstance?.firstname}"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'firstname', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'address', 'error')} ">
				<label for="address" class="control-label"><g:message code="visiteForm.address.label" default="Address" /></label>
				<div class="controls">
					<g:textField class="form-control" name="address" value="${visiteFormInstance?.address}"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'address', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'profession', 'error')} ">
				<label for="profession" class="control-label"><g:message code="visiteForm.profession.label" default="Profession" /></label>
				<div class="controls">
					<g:textField class="form-control" name="profession" value="${visiteFormInstance?.profession}"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'profession', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'wantToSee', 'error')} required">
				<label for="wantToSee" class="control-label"><g:message code="visiteForm.wantToSee.label" default="Want To See" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select class="form-control" id="wantToSee" name="wantToSee.id" from="${org.afdb.Staff.list()}" optionKey="id" required="" value="${visiteFormInstance?.wantToSee?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'wantToSee', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'purposeOfVisite', 'error')} ">
				<label for="purposeOfVisite" class="control-label"><g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" /></label>
				<div class="controls">
					<g:textArea class="form-control" rows="3" name="purposeOfVisite" value="${visiteFormInstance?.purposeOfVisite}"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'purposeOfVisite', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'expectedArrivingDateTime', 'error')} required">
				<label for="expectedArrivingDateTime" class="control-label"><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker class="form-control" name="expectedArrivingDateTime" precision="day"  value="${visiteFormInstance?.expectedArrivingDateTime}"  />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'expectedArrivingDateTime', 'error')}</span>
				</div>
			</div>
	
			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'actualArrivingDateTime', 'error')} required">
				<label for="actualArrivingDateTime" class="control-label"><g:message code="visiteForm.actualArrivingDateTime.label" default="Actual Arriving Date Time" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="actualArrivingDateTime" precision="day"  value="${visiteFormInstance?.actualArrivingDateTime}"  />
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'actualArrivingDateTime', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: visiteFormInstance, field: 'departureTime', 'error')} required">
				<label for="departureTime" class="control-label"><g:message code="visiteForm.departureTime.label" default="Departure Time" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="departureTime" precision="day"  value="${visiteFormInstance?.departureTime}"/>
					<span class="help-inline">${hasErrors(bean: visiteFormInstance, field: 'departureTime', 'error')}</span>
				</div>
			</div>

