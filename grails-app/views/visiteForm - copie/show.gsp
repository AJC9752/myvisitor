
<%@ page import="org.afdb.rs.VisiteForm" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
			<g:set var="layout_nomainmenu"		value="${true}" scope="request"/>
	
</head>

<body>

<section id="show-visiteForm" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.surname.label" default="Surname" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: visiteFormInstance, field: "surname")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.firstname.label" default="Firstname" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: visiteFormInstance, field: "firstname")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.address.label" default="Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: visiteFormInstance, field: "address")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.profession.label" default="Profession" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: visiteFormInstance, field: "profession")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.wantToSee.label" default="Want To See" /></td>
				
				<td valign="top" class="value"><g:link controller="staff" action="show" id="${visiteFormInstance?.wantToSee?.id}">${visiteFormInstance?.wantToSee?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.purposeOfVisite.label" default="Purpose Of Visite" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.expectedArrivingDateTime.label" default="Expected Arriving Date Time" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${visiteFormInstance?.expectedArrivingDateTime}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.actualArrivingDateTime.label" default="Actual Arriving Date Time" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${visiteFormInstance?.actualArrivingDateTime}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="visiteForm.departureTime.label" default="Departure Time" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${visiteFormInstance?.departureTime}" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
