
<%@ page import="org.afdb.rs.VisiteForm" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visiteForm.label', default: 'VisiteForm')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nomainmenu"		value="${true}" scope="request"/>
	
</head>

<body>
	
<section id="list-visiteForm" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="surname" title="${message(code: 'visiteForm.surname.label', default: 'Surname')}" />
				
				<g:sortableColumn property="firstname" title="${message(code: 'visiteForm.firstname.label', default: 'Firstname')}" />
				<g:sortableColumn property="expectedArrivingDateTime" title="${message(code: 'visiteForm.expectedArrivingDateTime.label', default: 'Expected Arriving time')}" />
				<g:sortableColumn property="actualArrivingDateTime" title="${message(code: 'visiteForm.actualArrivingDateTime.label', default: 'Actual Arriving time')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'visiteForm.address.label', default: 'Address')}" />				
			
				<g:sortableColumn property="profession" title="${message(code: 'visiteForm.profession.label', default: 'Profession')}" />
			
				<th><g:message code="visiteForm.wantToSee.label" default="Want To See" /></th>
			
				<g:sortableColumn property="purposeOfVisite" title="${message(code: 'visiteForm.purposeOfVisite.label', default: 'Purpose Of Visite')}" />
				<th>Action</th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${visiteFormInstanceList}" status="i" var="visiteFormInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td>${fieldValue(bean: visiteFormInstance, field: "surname")}</td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "firstname")}</td>
				<td>${fieldValue(bean: visiteFormInstance, field: "expectedArrivingDateTime")}</td>
				
				<td>${fieldValue(bean: visiteFormInstance, field: "actualArrivingDateTime")}</td>
				
			
				<td>${fieldValue(bean: visiteFormInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "profession")}</td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "wantToSee")}</td>
			
				<td>${fieldValue(bean: visiteFormInstance, field: "purposeOfVisite")}</td>
				<td><g:link class="btn btn-primary" action="show" id="${visiteFormInstance.id}">Open</g:link></td>
				
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${visiteFormInstanceTotal}" />
	</div>
</section>

</body>

</html>
