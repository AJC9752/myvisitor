<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div class="panel panel-default">
			
			<div>
				<div class="center">
					<legend>${message(code:'dashboard.period.month.selectmonth.label', default:'Veuillez sélectionner le mois et l année')}</legend>
				</div>
						
	 			<g:form controller="visiteForm" method="post" class="form-horizontal" >
	 				<div class="center">
						<g:datePicker class="form-label" name="monthYearSelected" precision="month"  value="${monthYear}" years="${2017..currentYear}"/>
						<g:hiddenField  value="MONTH" name="visitorNumber" />
						<button name="_action_dashboard" type="submit" class="btn btn-sm btn-success" >
							<i class="glyphicon glyphicon-log-out"></i>
						</button>
					</div>
				</g:form>
						
			</div>
		</div>
	</div>
	<div class="col-md-3"></div>
</div>