<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		<div class="panel panel-default">
			
			<div>
				<div class="center">
					<legend>${message(code:'dashboard.period.day.selectdate.label', default:'Veuillez sélectionner une date')}</legend>
				</div>
						
	 			<g:form controller="visiteForm" method="post" class="form-horizontal" >
	 				<div class="center">
						<bs:datePicker name="daySelected" value="${day}"  />
						<g:hiddenField  value="BYDAY" name="visitorNumber" />
						<button name="_action_dashboard" type="submit" class="btn btn-sm btn-success" >
							<i class="glyphicon glyphicon-log-out"></i>
						</button>
					</div>
				</g:form>
						
			</div>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>