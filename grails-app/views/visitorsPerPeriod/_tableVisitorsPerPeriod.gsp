	<g:each in="${visitorsPerPeriodData}" status="i" var="visitorsPerPeriodInstance">
		<li class="${(i % 2) == 0 ? 'odd' : 'even'} list-group-item">
			<span class="badge">${visitorsPerPeriodInstance[2]}</span>
			<strong>${visitorsPerPeriodInstance[1]}</strong>
		</li>
	</g:each>
