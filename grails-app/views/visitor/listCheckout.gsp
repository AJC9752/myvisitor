<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<g:render template="/_menu/menuVisitForm"/>

<section id="list-visitor" class="first">
	
	<div class="row">
		<div class="col-md-12">
			<span class="pull-right">
				<g:render template="/_menu/search" model="[searchController='visitor', searchAction='searchCheckout']"/>
			</span>
		
			<g:render template="/visitor/listCheckout"/>
		</div>
	
	</div>
	
</section>

</body>

</html>
