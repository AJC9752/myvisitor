<%@ page import="org.afdb.rs.Visitor" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<g:render template="/_menu/menuVisitForm"/>

<section id="list-visitor" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<g:sortableColumn property="firstName" title="${message(code: 'visitor.firstName.label', default: 'First Name')}" />
				<g:sortableColumn property="lastName" title="${message(code: 'visitor.lastName.label', default: 'Last Name')}" />
				<g:sortableColumn property="gender" title="${message(code: 'visitor.gender.label', default: 'Gender')}" />
				<g:sortableColumn property="address" title="${message(code: 'visitor.address.label', default: 'Address')}" />
				<g:sortableColumn property="contact" title="${message(code: 'visitor.contact.label', default: 'Contact')}" />
				<%--
				<g:sortableColumn property="company" title="${message(code: 'visitor.company.label', default: 'Company')}" />
				--%>
			</tr>
		</thead>
		<tbody>
		<g:each in="${visitorInstanceList}" status="i" var="visitorInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${visitorInstance.id}">${fieldValue(bean: visitorInstance, field: "firstName")}</g:link></td>
			
				<td>${fieldValue(bean: visitorInstance, field: "lastName")}</td>
			
				<td>${fieldValue(bean: visitorInstance, field: "gender")}</td>
			
				<td>${fieldValue(bean: visitorInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: visitorInstance, field: "contact")}</td>
				<%-- 
				<td>${fieldValue(bean: visitorInstance, field: "company")}</td>
			--%>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${visitorInstanceTotal}" />
	</div>
</section>

</body>

</html>
