<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

	<table class="table table-bordered margin-top-medium">
		<tbody>
		<g:each in="${visitorInstanceList}" status="i" var="visitorInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					<div class="row">
						<div class="col-md-10">
							<strong>${visitorInstance?.lastName} ${visitorInstance?.firstName}</strong>
							<br/>
							Gender:	<strong>${visitorInstance?.gender}</strong>
							<%--
							<br/> 
							Status: <g:visitorStatus status="${visitorInstance?.status}"/>
							--%>
						</div>
						<div class="col-md-2">
							<div class="col-md-3">
								<g:form method="post" class="form-horizontal" action="entrance" name="submitForm">
									<g:hiddenField  value="${visitorInstance?.id}" id="visitor" name="visitor.id" />
															
									<button type="submit" class="btn btn-sm btn-success" id="validateId" onClick="pleaseWait(this);">
										<i class="glyphicon glyphicon-ok"></i>
										<span id="idOK">
										</span>	
									</button>
									
								</g:form>
							</div>
							<%--
							<div class="col-md-3">
								<g:form method="post" class="form-horizontal" action="repressedGate">
									<g:hiddenField  value="${visitorInstance?.id}" id="visitor" name="visitor.id" />
															
									<g:if test="${visitorInstance?.status?.code in [VisitorStatus.AWAITING]}">				
										<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('${message(code: 'default.button.reset.submission.confirm.message', default: 'Are you sure?')}');">
											<i class="glyphicon glyphicon-remove"></i>
										</button>
									</g:if>
									<g:else>
										<button type="submit" disabled="disabled" class="btn btn-sm btn-danger">
											<i class="glyphicon glyphicon-remove"></i>
										</button>
									</g:else>
								</g:form>
							</div>
							--%>
							<div class="col-md-3">
								<g:link class="btn btn-sm btn-danger" controller="${params.controller}" action="showGate" id="${visitorInstance.id}">
									<i class="glyphicon glyphicon-remove"></i>
								</g:link>
							</div>
							
						</div>
					</div>
					
				</td>			
			</tr>
		</g:each>
		</tbody>
	</table>
	
	<%-- 
	<div class="container">
		<bs:paginate total="${visitorInstanceTotal}" />
	</div>
	--%>
