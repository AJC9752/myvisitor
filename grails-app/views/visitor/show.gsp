<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<g:render template="/_menu/menuVisitForm" model="[visitorEntryInstance:visitorEntryInstance]"/>

<section id="show-visitor" class="first">
	
	<div class="row">
		
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<h1 class="center">${message(code:'visitor.title', default:'Visitor')}</h1>
			<br/>
		</div>
		<div class="col-md-4">
			<div class="pull-right">
				<g:if test="${visitorEntryInstance?.status?.code in [VisitorStatus.CHECKIN]}">			
					<g:form method="post" class="form-horizontal" action="checkout" name="submitForm">
						<g:hiddenField  value="${visitorEntryInstance?.id}" id="entry" name="entry.id" />
						<button type="submit" class="btn btn-sm btn-danger" id="validateId" onClick="pleaseWait(this);">
							<i class="glyphicon glyphicon-log-out"></i> 
							<span id="idOK">
								<strong><g:message code="button.checkout.label" default="Checkout"/></strong>
							</span>	
						</button>
					</g:form>
				</g:if>
				<g:elseif test="${visitorEntryInstance?.status?.code in [VisitorStatus.CHECKOUT]}">
					<button type="submit" disabled="disabled" class="btn btn-sm btn-danger">
						<i class="glyphicon glyphicon-ok"></i> 
						<strong><g:message code="button.checkout.ok.message" default="Checkout"/></strong>
					</button>
				</g:elseif>
			</div>
		</div>
		
	</div>
		
	<div class="row">
	
		<div class="col-md-6">
		
			<table class="table">
				<tbody>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.firstName.label" default="First Name" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "firstName")}</strong>
						</td>				
					</tr>
		
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.lastName.label" default="Last Name" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "lastName")}</strong>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.gender.label" default="Gender" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "gender")}</strong>
						</td>
					</tr>

					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.status.label" default="Status" /></td>
						<td valign="top" class="value">
							<g:visitorStatus status="${visitorEntryInstance?.status}"/>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.address.label" default="Address" /></td>				
						<td valign="top" class="value">${fieldValue(bean: visitorInstance, field: "address")}</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.contact.label" default="Contact" /></td>
						<td valign="top" class="value">${fieldValue(bean: visitorInstance, field: "contact")}</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.badgeNumber.label" default="Badge Number" /></td>				
						<td valign="top" class="value">
							<g:typeBadge badge="${visitorEntryInstance?.badge}"/>
							<strong>${fieldValue(bean: visitorEntryInstance, field: "badgeNumber")}</strong>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.typeOfCard.label" default="Type Of Card" /></td>
						<td valign="top" class="value">
							<span class="label label-primary">
								<g:if test="${visitorInstance?.typeOfCard == "PC"}">
									${message(code:'visitor.typeOfCard.PC')}
								</g:if>
								<g:elseif test="${visitorInstance?.typeOfCard == "CNI"}">
									${message(code:'visitor.typeOfCard.CNI')}
								</g:elseif>
								<g:elseif test="${visitorInstance?.typeOfCard == "PASSPORT"}">
									${message(code:'visitor.typeOfCard.PASSPORT')}
								</g:elseif>
								<g:elseif test="${visitorInstance?.typeOfCard == "LAISSEZPASSER"}">
									${message(code:'visitor.typeOfCard.LAISSEZPASSER')}
								</g:elseif>
								<g:elseif test="${visitorInstance?.typeOfCard == "CARTECONSULAIRE"}">
									${message(code:'visitor.typeOfCard.CARTECONSULAIRE')}
								</g:elseif>
								<g:elseif test="${visitorInstance?.typeOfCard == "OTHER"}">
									${message(code:'visitor.typeOfCard.OTHER')}
								</g:elseif>
								</span>	
						</td>
					</tr>
					
					<g:if test="${visitorInstance?.otherCard}">
						<tr class="prop">
							<td valign="top" class="name"><g:message code="visitor.otherCard.label" default="Other Card" /></td>
							<td valign="top" class="value">
								<strong>${fieldValue(bean: visitorInstance, field: "otherCard")}</strong>
							</td>
						</tr>
					</g:if>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.numberCard.label" default="Number Card" /></td>
						<td valign="top" class="value">${fieldValue(bean: visitorInstance, field: "numberCard")}</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.validityDate.label" default="Validity Date" /></td>
						<td valign="top" class="value">
							<strong><g:formatDate date="${visitorInstance?.validityDate}" format="dd MMM yyyy"/></strong>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.checkinDateTime.label" default="Checkin Date Time" /></td>				
						<td valign="top" class="value">
							<strong><g:formatDate date="${visitorEntryInstance?.checkinDateTime}" format="dd MMM yyyy"/></strong>
							<strong><g:formatDate date="${visitorEntryInstance?.checkinDateTime}" format="HH:mm"/></strong>
						</td>
					</tr>
					
					<g:if test="${visitorEntryInstance?.checkoutDateTime}">
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.checkoutDateTime.label" default="Checkout Date Time" /></td>
						<td valign="top" class="value">
							<strong><g:formatDate date="${visitorEntryInstance?.checkoutDateTime}" format="dd MMM yyyy"/></strong>
							<strong><g:formatDate date="${visitorEntryInstance?.checkoutDateTime}" format="HH:mm"/></strong>
						</td>
					</tr>
					</g:if>
					
				</tbody>
			</table>
			
		</div>
		
		<div class="col-md-6">
			
			<table class="table">
				<tbody>
				
					<tr class="prop">
						<td valign="top" class="name" width="200"><g:message code="visiteForm.receiverVisit.label" default="Receiver Visit" /></td>
						<td valign="top" class="value">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								<strong>${visitorInstance?.visit?.receiverVisit?.lastName} ${visitorInstance?.visit?.receiverVisit?.firstName}</strong>
							</a>
							<div id="collapseOne" class="panel-collapse collapse">
								<g:render template="/staff/staffDetails" model="[staffInstance:visitorInstance?.visit?.receiverVisit]"/>
		                    </div>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visiteForm.askBy.label" default="Requested By" /></td>
						<td valign="top" class="value">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								<strong>${visitorInstance?.visit?.askBy?.lastName} ${visitorInstance?.visit?.askBy?.firstName}</strong>
							</a>
							<div id="collapseTwo" class="panel-collapse collapse">
								<g:render template="/staff/staffDetails" model="[staffInstance:visitorInstance?.visit?.askBy]"/>
		                    </div>
						</td>
					</tr>
										
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitType.label" default="Type of Visit" /></td>
						<td valign="top" class="value">
							<g:visitType type="${visitorInstance?.visitType}"/>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="status.label" default="Status" /></td>
						<td valign="top" class="value">
							<g:visitStatus status="${visitorInstance?.visit?.status}"/>
						</td>
					</tr>
					
					<tr class="prop">
						<td valign="top" class="name"><g:message code="location.label" default="Location" /></td>
						<td valign="top" class="value">
							<strong>${visitorInstance?.visit?.location}</strong>
						</td>
					</tr>
				</tbody>
			</table>
						
		</div>
		
	</div>
	
</section>

</body>

</html>
