<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<div class="row">
	<div class="col-md-12">
		<g:if test="${flash.error}">
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<span class="label label-danger">${message(code:'default.error.label', default:'Error')}</span> 
				${flash.error}
			</div>
		</g:if>
	</div>
</div>

<g:render template="/_menu/menuVisitForm"/>
<br/>
<section id="list-visitor" class="first">
	
	<%-- 
	<g:if test="${!session.location}">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<span class="label label-warning">${message(code:'warning.label')}</span>
			<strong>${message(code:'gate.location.isempty.label')}</strong>
		</div>
	</g:if>
	--%>
	<g:render template="/_menu/gateMenu"/>
	</br>
	
	<div class="row">
			
		<div class="col-md-12">
			<div class="center">
				<h2>${message(code:'visitor.event.title', default:'Event')}: ${message(code:'visitor.gate.event.list', default:'Visitors List')}</h2>			
			</div>
		</div>
		
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked">
						<g:each in="${visiteFormEventList}" status="i" var="visiteFormEventInstance">
							<li class="${i==0 ? 'active' : ''}">
								<a href="#event${visiteFormEventInstance?.id}" data-toggle="tab">
									<strong>${visiteFormEventInstance?.event}</strong>
								</a>
							</li>
						</g:each>
					</ul>
				</div>
				<div class="col-md-9">
					<!-- Tab panes -->
					<div class="tab-content">
						<g:each in="${visiteFormEventList}" status="i" var="visiteFormEventInstance">
							<div class="tab-pane ${i==0 ? 'active' : ''}" id="event${visiteFormEventInstance?.id}">
								<g:render template="listEvent" model="['visitors':visiteFormEventInstance?.visitors.sort{it.id}]"/>
							</div>
						</g:each>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
</section>

</body>

</html>
