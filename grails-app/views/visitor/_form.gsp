<%@ page import="org.afdb.rs.Visitor" %>

<g:set var="lang" value="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE' ?: org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().substring(0,2)}"/>

	<div class="row">
		
		<div class="col-md-12">
			<h1 class="center">${message(code:'visitor.title', default:'Visitor')}</h1>
			<br/>
		</div>
		
	</div>
	
	<div class="row">
	
		<div class="col-md-6">
			
			<div class="panel panel-default">
				<div class="panel-body form-horizontal payment-form">
			
					<div class="form-group">
						<label for="firstName" class="col-sm-4 control-label"><g:message code="visitor.firstName.label" default="First Name" /></label>
						<div class="col-sm-8">
							<g:textField class="form-control" name="firstName" value="${visitorInstance?.firstName}"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'firstName', 'error')}</span>
						</div>
					</div>
					
					<div class="form-group">
						<label for="lastName" class="col-sm-4 control-label"><g:message code="visitor.lastName.label" default="Last Name" /></label>
						<div class="col-sm-8">
							<g:textField class="form-control" name="lastName" value="${visitorInstance?.lastName}"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'lastName', 'error')}</span>
						</div>
					</div>
					
					<div class="form-group">
						<label for="gender" class="col-sm-4 control-label"><g:message code="visitor.gender.label" default="Gender" /></label>
						<div class="col-sm-8">
							<g:select class="form-control" name="gender" 
								from="${visitorInstance.constraints.gender.inList}"
								value="${visitorInstance?.gender}"
								valueMessagePrefix="visitor.gender"
								style="width:30%;" noSelection="['': '']" />
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'gender', 'error')}</span>
						</div>
					</div>
		
					<div class="form-group">
						<label for="address" class="col-sm-4 control-label"><g:message code="visitor.address.label" default="Address" /></label>
						<div class="col-sm-8">
							<g:textField class="form-control" name="address" value="${visitorInstance?.address}"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'address', 'error')}</span>
						</div>
					</div>
		
					<div class="form-group">
						<label for="contact" class="col-sm-4 control-label"><g:message code="visitor.contact.label" default="Contact" /></label>
						<div class="col-sm-8">
							<g:textField class="form-control" name="contact" value="${visitorInstance?.contact}"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'contact', 'error')}</span>
						</div>
					</div>
				
				</div>
			</div>
		
		</div>
		
		<div class="col-md-6">
			
			<div class="panel panel-default">
				<div class="panel-body form-horizontal payment-form">
					
					<div class="form-group">
						<label for="contact" class="col-sm-4 control-label"><g:message code="visitType.label" default="Contact" /></label>
						<div class="col-sm-8">
							<g:visitType type="${visitorInstance?.visit?.visitType}"/>
						</div>
					</div>
					
					<div class="form-group">
						<label for="badgeNumber" class="col-sm-4 control-label"><g:message code="visitorEntry.badgeNumber.label" default="Badge Number" /></label>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-6">
									<g:select class="form-control" 
										id="badge" name="badge.id" 
										from="${org.afdb.rs.TypeBadge.list()}"
										optionKey="id" required=""
										value="${visitorEntryInstance?.badge?.id}"
										optionValue="${{it?.getName(lang.toString())}}"
										onChange="toggle('#testDiv', this)"
										style="width:100%;" noSelection="${[null:'Select Type of Badge']}" />
								</div>
								<div class="col-sm-6">
									<g:field class="form-control" type="number" name="badgeNumber" value="${visitorEntryInstance?.badgeNumber}" style="width:100%;"/>
									<span class="help-inline">${hasErrors(bean: visitorEntryInstance, field: 'badgeNumber', 'error')}</span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="typeOfCard" class="col-sm-4 control-label"><g:message code="visitor.typeOfCard.label" default="Type Of Card" /></label>
						<div class="col-sm-8">
							<g:select class="form-control"  name="typeOfCard" 
								from="${listOptionTypeOfCard}" 
								noSelection="${[null:'Select One...']}" 
								value="${fieldValue(bean: visitorInstance, field: 'typeOfCard')}" 
								valueMessagePrefix="visitor.typeOfCard" 
								onChange="toggle('#testDiv', this)"
								style="width:70%;"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'typeOfCard', 'error')}</span>
						</div>
					</div>
					
					<div id="idOtherCard">
						<div class="form-group">
							<label for="otherCard" class="col-sm-4 control-label"><g:message code="visitor.otherCard.label" default="Other Card (Specify)" /></label>
							<div class="col-sm-8">
								<g:textField class="form-control" name="otherCard" value="${visitorInstance?.otherCard}"/>
								<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'otherCard', 'error')}</span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label for="numberCard" class="col-sm-4 control-label"><g:message code="visitor.numberCard.label" default="Number of Card" /></label>
						<div class="col-sm-8">
							<g:textField class="form-control" name="numberCard" value="${visitorInstance?.numberCard}"/>
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'numberCard', 'error')}</span>
						</div>
					</div>
					
					<div class="form-group">
						<label for="validityDate" class="col-sm-4 control-label"><g:message code="visitor.validityDate.label" default="Validity Date" /></label>
						<div class="col-sm-8">
							<bs:datePicker name="validityDate" precision="day"  value="${visitorInstance?.validityDate}" default="none" noSelection="['': '']" />
							<span class="help-inline">${hasErrors(bean: visitorInstance, field: 'validityDate', 'error')}</span>
						</div>
					</div>
										
				</div>
			</div>
			
		</div>
		
		<g:hiddenField  value="${visitorInstance?.visit?.id}" id="visit" name="visit.id" />
		<g:hiddenField  value="${visitorEntryInstance?.id}" id="entry" name="entry.id" />
		
	</div>

			

