<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<div class="row">
	<div class="col-md-12">
		<g:if test="${flash.error}">
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<span class="label label-danger">${message(code:'default.error.label', default:'Error')}</span> 
				${flash.error}
			</div>
		</g:if>
	</div>
</div>

<g:render template="/_menu/menuVisitForm"/>
<br/>
<section id="list-visitor" class="first">
	
	<%-- 
	<g:if test="${!session.location}">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<span class="label label-warning">${message(code:'warning.label')}</span>
			<strong>${message(code:'gate.location.isempty.label')}</strong>
		</div>
	</g:if>
	--%>
	<g:render template="/_menu/gateMenu"/>

	<g:render template="listGate"/>
	
</section>

</body>

</html>
