<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<g:render template="/_menu/menuVisitForm"/>

<section id="list-visitor" class="first">
	
	<table class="table table-bordered margin-top-medium">
		<tbody>
		<g:each in="${visitorEntryInstanceList}" status="i" var="visitorEntryInstance">
			<g:set var="visitorInstance" value="${Visitor?.get(visitorEntryInstance?.visitor?.id)}"/>
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					<div class="row">
						<div class="col-md-10">
							<strong>${visitorInstance?.lastName} ${visitorInstance?.firstName}</strong>
							<br/>
							Gender:	<strong>${visitorInstance?.gender}</strong>
							<br/>
							Status: <g:visitorStatus status="${visitorEntryInstance?.status}"/>
						</div>
						<div class="col-md-2">
							<div class="col-md-3">
								<g:form method="post" class="form-horizontal" action="checkin" name="submitForm">
									<g:hiddenField  value="${visitorEntryInstance?.id}" id="entry" name="entry.id" />
															
									<g:if test="${visitorEntryInstance?.status?.code in [VisitorStatus.ENTRANCE]}">			
										<button type="submit" class="btn btn-sm btn-success" id="validateId" onClick="pleaseWait(this);">
											<i class="glyphicon glyphicon-ok"></i>
											<span id="idOK">
											</span>	
										</button>
									</g:if>
									<g:else>
										<button type="submit" disabled="disabled" class="btn btn-sm btn-success">
											<i class="glyphicon glyphicon-ok"></i>
										</button>
									</g:else>
								</g:form>
							</div>
							<%-- 
							<div class="col-md-3">
								<g:form method="post" class="form-horizontal" action="repressedReception">
									<g:hiddenField  value="${visitorInstance?.id}" id="visitor" name="visitor.id" />
															
									<g:if test="${visitorInstance?.status?.code in [VisitorStatus.ENTRANCE]}">				
										<button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('${message(code: 'default.button.reset.submission.confirm.message', default: 'Are you sure?')}');">
											<i class="glyphicon glyphicon-remove"></i>
										</button>
									</g:if>
									<g:else>
										<button type="submit" disabled="disabled" class="btn btn-sm btn-danger">
											<i class="glyphicon glyphicon-remove"></i>
										</button>
									</g:else>
								</g:form>
							</div>
							--%>
							<div class="col-md-3">
								<g:link class="btn btn-sm btn-danger" controller="${params.controller}" action="showDesk" id="${visitorInstance.id}" params="[entryId: visitorEntryInstance?.id]">
									<i class="glyphicon glyphicon-remove"></i>
								</g:link>
							</div>
						</div>
					</div>
					
				</td>			
			</tr>
		</g:each>
		</tbody>
	</table>
	 
	<div class="container">
		<bs:paginate total="${visitorInstanceTotal}" />
	</div>
	
</section>

</body>

</html>
