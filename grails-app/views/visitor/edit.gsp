<%@ page import="org.afdb.rs.Visitor" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	
		<script type="text/javascript">
	
		function toggle(className, obj) {	
			//alert('Hello');		
		    var $select = $(obj);
		    var user_cat = $('#typeOfCard').find(":selected").val();
		  	//alert('user44:'+user_cat);
		    if (user_cat=='OTHER')  {//INDIVIDUAL
		    	//alert('INDIVIDUAL');
		    	$("#idOtherCard").fadeOut().fadeIn();
			}else{
				$("#idOtherCard").hide();
			}
		}
	</script>

	<r:script>
	 $('#myTab a').click(function (e) {
		  e.preventDefault();
		  $(this).tab('show');
		});
		
		function toggle(obj) {	
		    var $select = $(obj);
		    var user_cat = $('#typeOfCard').find(":selected").val();
		    //alert('user44:'+user_cat);	   
		   if (user_cat=='OTHER')  {//OTHER CARD
		    	//alert('INDIVIDUAL');
		    	$("#idOtherCard").fadeOut().fadeIn();
			}else{
				$("#idOtherCard").hide();
			}
		  return true;
		};
		
		$(document).ready(function() {		
			toggle('#typeOfCard');
		});
	</r:script>
	
</head>

<body>

<g:render template="/_menu/menuVisitForm"/>

<section id="edit-visitor" class="first">

	<g:hasErrors bean="${visitorInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${visitorInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${visitorInstance?.id}" />
		<g:hiddenField name="version" value="${visitorInstance?.version}" />
		<fieldset class="form">
			<g:render template="form"/>
		</fieldset>
		<div class="form-actions">
			<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
			<%-- 
			<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
            --%>
		</div>
	</g:form>

</section>
			
</body>

</html>
