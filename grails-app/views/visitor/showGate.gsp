
<%@ page import="org.afdb.rs.Visitor" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'visitor.label', default: 'Visitor')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
</head>

<body>

<g:render template="/_menu/menuVisitForm"/>

<section id="show-visitor" class="first">
	
	<div class="row">
	
		<div class="col-md-6">
	
			<table class="table">
				<tbody>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.firstName.label" default="First Name" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "firstName")}</strong>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.lastName.label" default="Last Name" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "lastName")}</strong>
						</td>
					</tr>
				
					<tr class="prop">
						<td valign="top" class="name"><g:message code="visitor.gender.label" default="Gender" /></td>
						<td valign="top" class="value">
							<strong>${fieldValue(bean: visitorInstance, field: "gender")}</strong>
						</td>
					</tr>
							
				</tbody>
			</table>
		
		</div>
		
		<div class="col-md-6">
			
			<g:form method="post" class="form-horizontal" action="deniedGate">
			
				<div class="${hasErrors(bean: visitorEntryInstance, field: 'deniedGate', 'error')} ">
					<label for="deniedGate" class="pull-left"><g:message code="remark.deniedGate.label" default="Remark" /></label>
					<div>
						<g:textArea class="form-control" name="deniedGate" maxlength="1024" cols="50" rows="10" value="${visitorEntryInstance?.deniedGate}"/>
						<span class="help-inline">${hasErrors(bean: visitorEntryInstance, field: 'deniedGate', 'error')}</span>
					</div>
				</div>	
				
				<g:hiddenField  value="${visitorInstance?.id}" id="visitor" name="visitor.id" />
		
				<div class="form-actions">
					<g:actionSubmit class="btn btn-sm btn-danger" action="deniedGate" value="${message(code: 'button.denied.label', default: 'Denied')}" />
				</div>
						
			</g:form>
			
		</div>
	
	</div>
							
</section>

</body>

</html>
