<%@ page import="org.afdb.rs.Visitor" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>
	
	<table class="table table-bordered margin-top-medium">
		<tbody>
		<g:each in="${visitorEntryInstanceList}" status="i" var="visitorEntryInstance">
			<g:set var="visitorInstance" value="${Visitor?.get(visitorEntryInstance?.visitor?.id)}"/>
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>
					<div class="row">
						<div class="col-md-10">
							<strong>${visitorInstance?.lastName} ${visitorInstance?.firstName}</strong>
							<br/>
							Gender:	<strong>${visitorInstance?.gender}</strong>
							<br/>
							Status: <g:visitorStatus status="${visitorEntryInstance?.status}"/>
						</div>
						<div class="col-md-2">
							<div class="col-md-3">
								<g:form method="post" class="form-horizontal" action="checkout" name="submitForm">
									<g:hiddenField  value="${visitorEntryInstance?.id}" id="entry" name="entry.id" />
									<g:if test="${visitorEntryInstance?.status?.code in [VisitorStatus.CHECKIN]}">			
										<button type="submit" class="btn btn-sm btn-primary" id="validateId" onClick="pleaseWait(this);">
											<i class="glyphicon glyphicon-log-out"></i>
											<span id="idOK">
											</span>	
										</button>
									</g:if>
								</g:form>
							</div>
							<div class="col-md-3">
								<g:link class="btn btn-sm btn-default" controller="${params.controller}" action="show" id="${visitorInstance.id}" params="[entryId: visitorEntryInstance?.id]">
									<i class="glyphicon glyphicon-plus"></i>
								</g:link>
							</div>
						</div>
					</div>
					
				</td>			
			</tr>
		</g:each>
		</tbody>
	</table>
	 
	<div class="container">
		<bs:paginate total="${visitorEntryInstanceCount}" />
	</div>