<%@ page import="grails.util.Environment" %>

<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>
		<g:if test="${Environment.current != Environment.PRODUCTION}">
			<p style="color: red;">
				<strong>TEST MODE</strong>
			</p>
		</g:if>
		<p>
			<strong>***** eVisitor *****</strong></p>
		<p>
			<strong><u>Français</u></strong></p>		
		<p>
			Chèr(e) Team</p>
		<p>
			L'accès à la réception a été <strong>refusée</strong> au visiteur <strong>${visitorInstance?.firstName} ${visitorInstance?.lastName}</strong>.</p>
		<p>
			Motif: ${visitorEntryInstance?.deniedReception}</p>
		<p>	
			Receveur de la Visite: <strong>${visitorInstance?.visit?.receiverVisit?.firstName} ${visitorInstance?.visit?.receiverVisit?.lastName}</strong>.</p>
		<p>	
			Visite Demandé par: <strong>${visitorInstance?.visit?.askBy?.firstName} ${visitorInstance?.visit?.askBy?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailFr"/>
			
		<hr />
		<p>
			<strong><u>English</u></strong></p>
		<p>
			Dear Colleague(s)</p>
		<p>
			Access to the reception was <strong>denied</strong> to the visitor <strong>${visitorInstance?.firstName} ${visitorInstance?.lastName}</strong>.</p>
		<p>
			Reason: ${visitorEntryInstance?.deniedReception}</p>
		<p>	
			Receiver of the Visit: <strong>${visitorInstance?.visit?.receiverVisit?.firstName} ${visitorInstance?.visit?.receiverVisit?.lastName}</strong>.</p>
		<p>	
			Visit Requested by: <strong>${visitorInstance?.visit?.askBy?.firstName} ${visitorInstance?.visit?.askBy?.lastName}</strong>.</p>
			
		<g:render template="/email/footerMailEn"/>
			
		<hr />

	</body>
</html>