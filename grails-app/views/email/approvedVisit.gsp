<%@ page import="grails.util.Environment" %>

<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>
		<g:if test="${Environment.current != Environment.PRODUCTION}">
			<p style="color: red;">
				<strong>TEST MODE</strong>
			</p>
		</g:if>
		<p>
			<strong>***** eVisitor *****</strong></p>
		<p>
			<strong><u>Français</u></strong></p>
		<p>
			Chèr(e) <strong>${receiverVisitStaff?.firstName} ${receiverVisitStaff?.lastName}</strong></p>
		<p>
			Votre demande de visite a été <strong>approuvée</strong>.</p>
		<p>	
			Approuvée par: <strong>${approvedByStaff?.firstName} ${approvedByStaff?.lastName}</strong>.</p>
		<p>	
			Visite demandée par: <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailFr"/>
			
		<hr />
		<p>
			<strong><u>English</u></strong></p>
		<p>
			Dear <strong>${receiverVisitStaff?.firstName} ${receiverVisitStaff?.lastName}</strong></p>
		<p>
			Your visit request has been <strong>approved</strong>.</p>
		<p>	
			Approved by: <strong>${approvedByStaff?.firstName} ${approvedByStaff?.lastName}</strong>.</p>
		<p>	
			Visit requested by: <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong>.</p>
			
		<g:render template="/email/footerMailEn"/>
			
		<hr />

	</body>
</html>