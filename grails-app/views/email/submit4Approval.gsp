<%@ page import="grails.util.Environment" %>
<%@ page import="org.afdb.rs.VisitType" %>

<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>
		<g:if test="${Environment.current != Environment.PRODUCTION}">
			<p style="color: red;">
				<strong>TEST MODE</strong>
			</p>
		</g:if>
		<p>
			<strong>***** eVisitor *****</strong></p>
		
		<table style="width:100%">
			<tr>
				<td>
					<p>
						<strong><u>Français</u></strong></p>
					<p>
						Chèr(e) Collègue(s)</p>
					<p>
						Un formulaire de demande de visite vous a été soumis.</p>
						
					<g:if test="${visiteForm?.visitType == VisitType.findByCode(VisitType.BUSINESS)}">
					<p>
						Nom de la Compagnie: <strong>${visiteForm?.company}</strong></p>
					</g:if>
					<g:if test="${visiteForm?.visitType == VisitType.findByCode(VisitType.EVENT)}">
					<p>
						Nom de l'Evènement: <strong>${visiteForm?.event}</strong></p>
					</g:if>
					<p>	
						Date de la Visite: <strong><g:formatDate date="${visiteForm?.expectedArrivingDateTime}" format="dd MMM yyyy" locale="fr"/></strong></p>
					<p>	
						Heure de la Visite: <strong><g:formatDate date="${visiteForm?.expectedArrivingDateTime}" format="HH:mm"/></strong></p>
					<p>	
						Motif de la Visite: <div class="alert" role="alert">
												<strong>
													${visiteForm?.purposeOfVisite}
												</strong>
											</div>
											</p>
					<p>	
						Receveur de la Visite: <strong>${receiverVisitStaff?.firstName} ${receiverVisitStaff?.lastName}</strong>.</p>
					<p>	
						Visite Demandé par: <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong>.</p>
					
					<g:render template="/email/footerMailFr"/>
				</td>
				<td rowspan="2">
					<p>
						<strong>Visiteur(s) / Visitor(s)</strong>
						<g:each in="${visiteForm.visitors}" >
							<ul>
								<li>${it.lastName} ${it.firstName} - ${it.gender}</li>
							</ul>
						</g:each>
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<p>
						<strong><u>English</u></strong></p>
					<p>
						Dear Colleague(s)</p>
					<p>
						A visit request form has been submitted to you</p>
						
					<g:if test="${visiteForm?.visitType == VisitType.findByCode(VisitType.BUSINESS)}">
					<p>
						Company Name: <strong>${visiteForm?.company}</strong></p>
					</g:if>
					<g:if test="${visiteForm?.visitType == VisitType.findByCode(VisitType.EVENT)}">
					<p>
						Event Name: <strong>${visiteForm?.event}</strong></p>
					</g:if>
					<p>	
						Date of Visit: <strong><g:formatDate date="${visiteForm?.expectedArrivingDateTime}" format="dd MMM yyyy" locale="en"/></strong></p>
					<p>	
						Time of Visit: <strong><g:formatDate date="${visiteForm?.expectedArrivingDateTime}" format="HH:mm"/></strong></p>
					<p>	
						Reason for Visit: <div class="alert" role="alert">
												<strong>
													${visiteForm?.purposeOfVisite}
												</strong>
											</div>
											</p>
					<p>	
						Receiver of the Visit: <strong>${receiverVisitStaff?.firstName} ${receiverVisitStaff?.lastName}</strong>.</p>
					<p>	
						Visit Requested by: <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong>.</p>
					
					<g:render template="/email/footerMailEn"/>
				</td>
			</tr>
		</table>	
		
	</body>
</html>