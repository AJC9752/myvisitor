	<p>
		To access to the <strong>Visitor Management System</strong>, <a href="${appUrl}" target="_blank">Click here</a></p>
	<p>
		For any assistance or additional information, feel free to contact <a href="mailto:psec_abidjan@afdb.org" target="_top">PSEC</a>.</p>
	<p>
		Thank You.</p>