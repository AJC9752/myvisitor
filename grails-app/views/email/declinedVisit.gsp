<%@ page import="grails.util.Environment" %>

<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>
		<g:if test="${Environment.current != Environment.PRODUCTION}">
			<p style="color: red;">
				<strong>TEST MODE</strong>
			</p>
		</g:if>
		<p>
			<strong>***** eVisitor *****</strong></p>
		<p>
			<strong><u>Français</u></strong></p>
		
		<p>
			Chèr(e) <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong></p>
		<p>
			Votre demande de visite a été <strong>refusée</strong>.</p>
		<p>	
			Refusé par: <strong>${declinedByStaff?.firstName} ${declinedByStaff?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailFr"/>
			
		<hr />
		<p>
			<strong><u>English</u></strong></p>
		<p>
			Dear <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong></p>
		<p>
			Your visit request was <strong>denied</strong>.</p>
		<p>	
			Refused by: <strong>${declinedByStaff?.firstName} ${declinedByStaff?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailEn"/>
			
		<hr />

	</body>
</html>