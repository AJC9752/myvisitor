	<p>
		Pour accéder au <strong>Système de Gestion des Visiteurs</strong>, <a href="${appUrl}" target="_blank">Cliquez ici</a></p>
	<p>
		Pour tout besoin d'assistance ou d'information complémentaire, n'hésitez pas à contacter <a href="mailto:psec_abidjan@afdb.org" target="_top">PSEC</a>.</p>
	<p>
		Merci.</p>