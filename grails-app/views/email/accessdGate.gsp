<%@ page import="grails.util.Environment" %>

<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>
		<g:if test="${Environment.current != Environment.PRODUCTION}">
			<p style="color: red;">
				<strong>TEST MODE</strong>
			</p>
		</g:if>
		<p>
			<strong>***** eVisitor *****</strong></p>
		<p>
			<strong><u>Français</u></strong></p>
		<p>
			Chèr(e) <strong>${receiverVisit?.firstName} ${receiverVisit?.lastName}</strong></p>
		<p>
			Votre visiteur, <strong>${visitorInstance?.firstName} ${visitorInstance?.lastName}</strong>, a accédé au batiment ce jour <g:formatDate date="${visitorInstance?.checkinGateDate}" format="dd MMM yyyy" locale="fr"></g:formatDate>  à  <strong><g:formatDate date="${visitorInstance?.checkinGateDate}" format="HH:mm" locale="fr"></g:formatDate></strong>.</p>
		<p>
			Votre êtes priés de bien vouloir vous rendre à l'accueil pour le réceptionner.</p>
		<p>	
			Visite demandé par: <strong>${askBy?.firstName} ${askBy?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailFr"/>
			
		<hr />
		<p>
			<strong><u>English</u></strong></p>
		<p>
			Dear <strong>${receiverVisit?.firstName} ${receiverVisit?.lastName}</strong></p>
		<p>
			Your visitor, <strong>${visitorInstance?.firstName} ${visitorInstance?.lastName}</strong>, accessed to the building this day <g:formatDate date="${visitorInstance?.checkinGateDate}" format="dd MMM yyyy" locale="en"></g:formatDate>  at  <strong><g:formatDate date="${visitorInstance?.checkinGateDate}" format="HH:mm" locale="fr"></g:formatDate></strong>.</p>
		<p>
			You are kindly requested to go to reception to receive it.</p>
		<p>	
			Visit Request by: <strong>${askBy?.firstName} ${askBy?.lastName}</strong>.</p>
		
		<g:render template="/email/footerMailEn"/>
			
		<hr />

	</body>
</html>