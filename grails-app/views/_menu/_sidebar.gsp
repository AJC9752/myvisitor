	<link rel="stylesheet" href="${resource(dir: 'css', file: 'w3.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'font-awesome.min.css')}" type="text/css">
	
	<div class="w3-sidebar w3-bar-block w3-black w3-xxlarge" style="width:70px">
		<a href="#" class="w3-bar-item w3-button"><i class="fa fa-home"></i></a> 
		<a href="#" class="w3-bar-item w3-button"><i class="fa fa-search"></i></a> 
		<a href="#" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i></a> 
		<a href="#" class="w3-bar-item w3-button"><i class="fa fa-globe"></i></a>
		<a href="#" class="w3-bar-item w3-button"><i class="fa fa-trash"></i></a> 
	</div>
	
	<%-- 
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'sidebar.css')}" type="text/css">
	
	<nav class="navbar navbar-default sidebar" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>      
			</div>
			<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Users <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a>
						<ul class="dropdown-menu forAnimate" role="menu">
							<li><a href="{{URL::to('createusuario')}}">Crear</a></li>
							<li><a href="#">Modificar</a></li>
							<li><a href="#">Reportar</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
							<li class="divider"></li>
							<li><a href="#">Informes</a></li>
						</ul>
					</li>          
					<li><a href="#">Libros<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-th-list"></span></a></li>        
					<li><a href="#">Tags<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-tags"></span></a></li>
				</ul>
			</div>
		</div>
	</nav>
	--%>