<g:form controller="${searchController}" action="${searchAction}" method="post" class="navbar-form navbar-left" >
	<div class="well well-sm">
		<div class="form-group fieldcontain text-center">
			<input name="query" type="text" class="form-control search-query" placeholder="${message(code: 'search.placeholder', default: 'Search ...')}" value="${query}">
		</div>
		<g:render template="/_menu/searchButton"/>
	</div>
</g:form>
