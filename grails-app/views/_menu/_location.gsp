
<div class="btn-group" role="group" aria-label="...">
	<g:each in="${org.afdb.rs.Location.list()}" status="i" var="locationButtonInstance">
		<g:link class="btn btn-sm btn-default ${(session.location == locationButtonInstance?.code )? 'active' : '' }" action="${params.action}" controller="${params.controller}" params="[location:locationButtonInstance?.code]">
			<strong>${locationButtonInstance?.code}</strong>
		</g:link>
	</g:each>
</div>