<%@ page import="org.afdb.rs.VisitStatus" %>
<%@ page import="org.afdb.rs.VisitorStatus" %>

	<ul id="Menu" class="nav nav-pills">
		
		<g:if test="${session.user?.role in ["admin", "desk"]}">
			<li class="${ (params.controller == "visiteForm" && params.action == "dashboard") ? 'active' : '' }">
				<g:link controller="visiteForm" action="dashboard">
					<i class="glyphicon glyphicon-tasks"></i>
					<strong><g:message code="menu.dashboard.label" default="Dashboard"/></strong>
				</g:link>
			</li>
		</g:if>
		
		<g:if test="${!(session.user?.role == "guard")}">
			<li class="${ (params.controller == "visiteForm" && params.action == "create") ? 'active' : '' }">
				<g:link controller="visiteForm" action="create">
					<i class="glyphicon glyphicon-plus"></i>
					<strong><g:message code="menu.newVisit.label"/></strong>
				</g:link>
			</li>
		</g:if>
		<%-- 
		<g:if test="${!(session.user?.role == "guard")}">
			<li class="${ (params.controller == "visiteForm" && params.action == "listInProgress") ? 'active' : '' }">
				<g:link controller="visiteForm" action="listInProgress"><i class="icon-th-list"></i> <g:message code="menu.listInProgress.label"/></g:link>
			</li>
		</g:if>
		
		<g:if test="${!(session.user?.role == "guard")}">
			<li class="${ (params.controller == "visiteForm" && params.action == "listProcessed") ? 'active' : '' }">
				<g:link controller="visiteForm" action="listProcessed"><i class="glyphicon glyphicon-th"></i> <g:message code="menu.listProcessed.label" default="List"/></g:link>
			</li>
		</g:if>
		--%>
		<g:if test="${!(session.user?.role == "guard")}">
			<li class="${ (params.controller == "visiteForm" && params.action == "list") ? 'active' : '' }">
				<g:link controller="visiteForm" action="list">
					<i class="glyphicon glyphicon-th"></i>
					<strong><g:message code="menu.listRequest.label" default="List of my Requests"/></strong>
				</g:link>
			</li>
		</g:if>
		
		<g:if test="${!(session.user?.role == "guard")}">
			<g:if test="${visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS, VisitStatus.SUBMITTED]}">	
			<g:if test="${ (params.controller == "visiteForm") && (params.action == 'show' || params.action == 'edit') }">
				<!-- the item is an object (not a list) -->
				<li class="${ params.action == "edit" ? 'active' : '' }">
					<g:link controller="visiteForm" action="edit" id="${params.id}">
						<i class="glyphicon glyphicon-pencil"></i>
						<strong><g:message code="menu.edit.label" default="Edit"/></strong>
					</g:link>
				</li>
			</g:if>
			</g:if>
		</g:if>
		
		<g:if test="${session.user?.role == "desk"}">
			<g:if test="${!(visitorEntryInstance?.status?.code in [VisitorStatus.CHECKOUT])}">	
				<g:if test="${ (params.controller == "visitor") && (params.action == 'show' || params.action == 'edit') }">
					<!-- the item is an object (not a list) -->
					<li class="${ params.action == "edit" ? 'active' : '' }">
						<g:link controller="visitor" action="edit" id="${visitorEntryInstance?.visitor?.id}" params="[entryId: visitorEntryInstance?.id]">
							<i class="glyphicon glyphicon-pencil"></i>
							<strong><g:message code="menu.edit.label" default="Edit"/></strong>
						</g:link>
					</li>
				</g:if>
			</g:if>
		</g:if>
		
		<g:if test="${session.user?.role == "admin"}">
			<li class="${ (params.controller == "visiteForm" && params.action == "listManagement") ? 'active' : '' }">
				<g:link controller="visiteForm" action="listManagement">
					<i class="icon-th-list"></i>
					<strong><g:message code="menu.listManagement.label" default="List Management"/></strong>
				</g:link>
			</li>
		</g:if>
		
		<g:if test="${session.user?.role == "desk"}">
			<li class="${ (params.controller == "visitor" && params.action == "listDesk") ? 'active' : '' }">
				<g:link controller="visitor" action="listDesk">
					<i class="icon-th-list"></i>
					<strong><g:message code="menu.listDesk.label" default="List Desk"/></strong>
				</g:link>
			</li>
		</g:if>
		
		<g:if test="${session.user?.role == "desk"}">
			<li class="${ (params.controller == "visitor" && (params.action == "listCheckout" || params.action == "searchCheckout")) ? 'active' : '' }">
				<g:link controller="visitor" action="listCheckout">
					<i class="glyphicon glyphicon-log-out"></i>
					<strong><g:message code="menu.listCheckout.label" default="Exit List"/></strong>
				</g:link>
			</li>
		</g:if>
		
		<g:if test="${session.user?.role == "guard"}">
			<li class="${ (params.controller == "visitor") && (params.action == "listGate" || params.action == "listGatePersonal" || params.action == "listGateBusiness" || params.action == "listGateEvent") ? 'active' : '' }">
				<g:link controller="visitor" action="listGate">
					<i class="icon-th-list"></i>
					<strong><g:message code="menu.listGate.label" default="List Gate"/></strong>
				</g:link>
			</li>
		</g:if>
		
	</ul>