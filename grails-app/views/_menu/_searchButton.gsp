
	<button type="submit" class="btn btn-default btn-sm">
		<i class="glyphicon glyphicon-search"></i> 
		<strong>${message(code: 'button.search.label', default: 'Search')}</strong>
	</button>