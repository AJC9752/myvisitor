
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">  
			<strong>${message(code:'administrator.label', default:'Administrator')} </strong> <b class="caret"></b>
		</a>
				
		<ul class="dropdown-menu">
			<li class="controller">
				<g:link action="list" controller="staff">
					<i class="glyphicon glyphicon-user"> ${message(code:'menu.admin.staff.label', default:'Staff')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
					
			<li class="controller">
				<g:link action="list" controller="orgUnit">
					<i class="glyphicon glyphicon-indent-right"> ${message(code:'menu.admin.orgUnit.label', default:'Org. Unit')} </i>
				</g:link>
			</li>
			<li class="divider"></li>					
			
			<li class="controller">
				<g:link action="list" controller="visiteForm">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.visiteForm.label', default:'Visit Form')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			<%-- 
			<li class="controller">
				<g:link action="list" controller="visitor">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.visitor.label', default:'Visitor')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			--%>
			<li class="controller">
				<g:link action="list" controller="location">
					<i class="glyphicon glyphicon-retweet"> ${message(code:'menu.admin.location.label', default:'Location')} </i>
				</g:link>
			</li>
			<li class="divider"></li>					

			<li class="controller">
				<g:link action="list" controller="typeBadge">
					<i class="glyphicon glyphicon-usd"> ${message(code:'menu.admin.typeBadge.label', default:'Type Badge')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			
			<li class="controller">
				<g:link action="list" controller="visitType">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.visitType.label', default:'Visit Type')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			
			<li class="controller">
				<g:link action="list" controller="visitStatus">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.visitStatus.label', default:'Visit Status')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
								
			<li class="controller">
				<g:link action="list" controller="visitorStatus">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.visitorStatus.label', default:'Visitor Status')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			
			<li class="controller">
				<g:link action="list" controller="user">
					<i class="glyphicon glyphicon-new-window"> ${message(code:'menu.admin.user.label', default:'User')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
			
			<li class="controller">
				<g:link action="list" controller="parameter">
					<i class="glyphicon glyphicon-indent-right"> ${message(code:'menu.admin.parameter.label', default:'Parameter')} </i>
				</g:link>
			</li>
			<li class="divider"></li>
						
		</ul>
	</li>