<ul class="nav pull-right">
	<li class="dropdown dropdown-btn">

<g:if test="${session?.user}">
	<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
	<!-- TODO: Only show menu items based on permissions (e.g., Guest has no account page) -->
				
		<i class="glyphicon glyphicon-user glyphicon-large glyphicon-white"></i>
			${session.user?.fullname}
		<b class="caret"></b>
	</a>
			<ul class="dropdown-menu" role="menu">
				<!-- TODO: Only show menu items based on permissions -->
				<%--
				<li class=""><a href="${createLink(uri: '/')}">
					<i class="icon-user"></i>
					<g:message code="user.show.label"/>
				</a></li>
				<li class=""><a href="${createLink(uri: '/')}">
					<i class="icon-cogs"></i>
					<g:message code="user.settings.change.label"/>
				</a></li>
				
				<li class="divider"></li>
				 --%>
				<li class="">
				<a href="${createLink(uri: '/user/logout')}">
					<i class="glyphicon glyphicon-off"></i>
					<g:message code="security.signoff.label"/>
				</a></li>
			</ul>
</g:if>
<g:else>
	<a href="${createLink(uri: '/user/login')}">
					<i class="glyphicon glyphicon-user"></i>
					<g:message code="security.signin.label1" default="Log in"/>
	</a>
</g:else>

	</li>
</ul>

<noscript>
<ul class="nav pull-right">
	<li class="">
		<g:link controller="user" action="show"><g:message code="default.user.unknown.label"/></g:link>
	</li>
</ul>
</noscript>
