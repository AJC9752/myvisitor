<li class="dropdown dropdown-btn">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white">
        <i class="icon-info-sign"></i>
        <strong>
		<g:message code="default.user.label" default="${session.user?.fullname}"/> <b class="caret"></b>
		</strong>
    </a>

    <ul class="dropdown-menu">
        <%-- Note: Links to pages without controller are redirected in conf/UrlMappings.groovy --%>
        %{--<li class="">--}%
            %{--<a href="${createLink(uri: '/about')}">--}%
                %{--<i class="icon-info-sign"></i>--}%
                %{--<g:message code="default.about.label"/>--}%
            %{--</a>--}%
        %{--</li>--}%

        %{--<li class="">--}%
            %{--<a href="${createLink(uri: '/contact')}">--}%
                %{--<i class="icon-envelope"></i>--}%
                %{--<g:message code="default.logout.label"/>--}%
            %{--</a>--}%
        %{--</li>--}%

        <li class="">
            <g:link controller="user" action="logout">
				<i class="glyphicon glyphicon-off"></i>
                <g:message code="default.logout.label" default="Logout"/>
            </g:link>
        </li>
    </ul>
</li>
