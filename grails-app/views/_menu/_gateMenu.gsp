
<ul class="nav nav-pills nav-justified">

	<li class="${(params.controller == "visitor" && params.action == "listGatePersonal" )? 'active' : '' }">
		<g:link action="listGatePersonal" controller="visitor"><strong>${message(code:'visitType.individual.label', default:'Personal')}</strong></g:link>
	</li>
	
	<li class="${(params.controller == "visitor" && params.action == "listGateFamily" )? 'active' : '' }">
		<g:link action="listGateFamily" controller="visitor"><strong>${message(code:'visitType.family.label', default:'Family')}</strong></g:link>
	</li>	
	
	<li class="${(params.controller == "visitor" && params.action == "listGateBusiness" )? 'active' : '' }">
		<g:link action="listGateBusiness" controller="visitor"><strong>${message(code:'visitType.business.label', default:'Business')}</strong></g:link>
	</li>
	
	<li class="${(params.controller == "visitor" && params.action == "listGateEvent" )? 'active' : '' }">
		<g:link action="listGateEvent" controller="visitor"><strong>${message(code:'visitType.event.label', default:'Event')}</strong></g:link>
	</li>
	
</ul>
