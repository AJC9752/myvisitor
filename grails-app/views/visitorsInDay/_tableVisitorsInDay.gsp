	<g:each in="${visitorsInDayList}" status="i" var="visitorsInDayInstance">
		<li class="${(i % 2) == 0 ? 'odd' : 'even'} list-group-item">
			<span class="badge">${fieldValue(bean: visitorsInDayInstance, field: "nbVisitor")}</span>
			<strong>${fieldValue(bean: visitorsInDayInstance, field: "nameBuilding")}</strong>
		</li>
	</g:each>
	
	<%-- 
	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<th><g:message code="visitorsInDay.codeBuilding.label" default="Code Building" /></th>
				<th><g:message code="visitorsInDay.nameBuilding.label" default="Name Building" /></th>
				<th width=100><g:message code="visitorsInDay.nbVisitor.label" default="Nb Visiteurs" /></th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${visitorsInDayList}" status="i" var="visitorsInDayInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td>
						<strong>
							${fieldValue(bean: visitorsInDayInstance, field: "codeBuilding")}
						</strong>
					</td>
					<td>
						${fieldValue(bean: visitorsInDayInstance, field: "nameBuilding")}
					</td>
					
					<td>
						<span class="pull-right">
							${fieldValue(bean: visitorsInDayInstance, field: "nbVisitor")}
						</span>
					</td>
				</tr>
			</g:each>
		</tbody>
	</table>
	--%>