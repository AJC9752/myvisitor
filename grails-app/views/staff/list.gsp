
<%@ page import="org.afdb.Staff" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-staff" class="first">

	<g:render template="list"/>
	
</section>

</body>

</html>
