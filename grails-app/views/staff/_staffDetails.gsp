
	<table class="table">
	
       	<g:if test="${staffInstance?.position}">
            <tr class="prop">
				<td valign="top" class="name">
					<strong><g:message code="staff.position.label" default="Position" /></strong>
				</td>
				<td valign="top" class="value">
					${staffInstance?.position}
				</td>
			</tr>
		</g:if>
	
		<g:if test="${staffInstance?.orgUnit}">
			<tr class="prop">
				<td valign="top" class="name">
					<strong><g:message code="staff.orgUnit.label" default="Org Unit" /></strong>
				</td>
				<td valign="top" class="value">
					${staffInstance?.orgUnit}
				</td>
			</tr>
       </g:if>
       
       <g:if test="${staffInstance?.extension}">
			<tr class="prop">
				<td valign="top" class="name">
					<strong><g:message code="staff.extension.label" default="Extension" /></strong>
				</td>
				<td valign="top" class="value">
					${staffInstance?.extension}
				</td>
			</tr>
       </g:if>
       
       <g:if test="${staffInstance?.cell}">
			<tr class="prop">
				<td valign="top" class="name">
					<strong><g:message code="staff.cell.label" default="Cell." /></strong>
				</td>
				<td valign="top" class="value">
					${staffInstance?.cell}
				</td>
			</tr>
       </g:if>

	</table>