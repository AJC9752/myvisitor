

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="perno" title="${message(code: 'staff.perno.label', default: 'Perno')}" />
				<th><g:message code="staff.fullName.label" default="Full Name"/></th>
				<%-- 
				<g:sortableColumn property="firstName" title="${message(code: 'staff.firstName.label', default: 'First Name')}" />
				<g:sortableColumn property="lastName" title="${message(code: 'staff.lastName.label', default: 'Last Name')}" />
				--%>
				<g:sortableColumn property="position" title="${message(code: 'staff.position.label', default: 'Position')}" />
				<th><g:message code="staff.orgUnit.label" default="Org Unit" /></th>
				<th><g:message code="action.label" default="Action"/></th>	
			</tr>
		</thead>
		<tbody>
		<g:each in="${staffInstanceList}" status="i" var="staffInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${staffInstance.id}">${fieldValue(bean: staffInstance, field: "perno")}</g:link></td>
				<td>${staffInstance?.lastName} ${staffInstance?.firstName}</td>
				
				<%-- 
				<td>${fieldValue(bean: staffInstance, field: "firstName")}</td>
				<td>${fieldValue(bean: staffInstance, field: "lastName")}</td>
				--%>
				
				<td>${fieldValue(bean: staffInstance, field: "position")}</td>
				<td>
					<strong>
						<g:link controller="orgUnit" action="show" id="${staffInstance?.orgUnit?.id}">
							${fieldValue(bean: staffInstance, field: "orgUnit")}
						</g:link>
					</strong>
				</td>
				
				<td>
					<%-- 
					<g:if test="${buttonInstance?.status?.code in [ProcessStatus.VALIDATED, ProcessStatus.SUBMITTED]}">
					--%>
						<g:link class="btn btn-sm btn-success" controller="staff" action="show" id="${staffInstance.id}">
							<i class="glyphicon glyphicon-eye-open"></i>
							<strong>${message(code: 'staff.show.label', default: 'Voir Staff')}</strong>
						</g:link>
					<%--
					</g:if>
					--%>
					
					<%-- 
					<g:if test="${buttonInstance?.status?.code in [ProcessStatus.VALIDATED, ProcessStatus.SUBMITTED]}">
					--%>
						<g:link class="btn btn-sm btn-primary" controller="staff" action="visitorsList" params="[perno:staffInstance?.perno]">
							<i class="glyphicon glyphicon-list"></i>
							<strong>${message(code: 'visiteForm.show.label', default: 'Voir Visite')}</strong>
						</g:link>
					<%--
					</g:if>
					--%>
				</td>
						
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${staffInstanceTotal}" />
	</div>

