<%@ page import="org.afdb.Staff" %>
<%@ page import="org.afdb.rs.VisiteForm" %>
<%@ page import="org.afdb.rs.VisitType" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<section id="list-staff" class="first">

	<div class="row">
		
		<div class="col-md-12">
			<h2 class="center">Liste des Visiteurs</h2>
			<br/>
		</div>
		
		<div class="col-md-12">
			<h4 class="center"><strong>${staffInstance?.lastName} ${staffInstance?.firstName}</strong></h4>
			<br/>
		</div>		

		<div class="col-md-12">
			<ul class="nav nav-pills nav-justified">
				<li role="presentation" class="${(codeVisitType == "INDIVIDUAL" )? 'active' : '' }">
					<g:link controller="staff" action="visitorsList" params="[perno:staffInstance?.perno, visitType:VisitType.INDIVIDUAL]"><strong>${message(code:'visitor.list.personal.label', default:'Personal')}</strong></g:link>
				</li>
								
				<li role="presentation" class="${(codeVisitType == "FAMILY" )? 'active' : '' }">
					<g:link controller="staff" action="visitorsList" params="[perno:staffInstance?.perno, visitType:VisitType.FAMILY]"><strong>${message(code:'visitor.list.family.label', default:'Family')}</strong></g:link>
				</li>	
				
				<li role="presentation" class="${(codeVisitType == "BUSINESS" )? 'active' : '' }">
					<g:link controller="staff" action="visitorsList" params="[perno:staffInstance?.perno, visitType:VisitType.BUSINESS]"><strong>${message(code:'visitor.list.business.label', default:'Business')}</strong></g:link>
				</li>
				
				<li role="presentation" class="${(codeVisitType == "EVENT" )? 'active' : '' }">
					<g:link controller="staff" action="visitorsList" params="[perno:staffInstance?.perno, visitType:VisitType.EVENT]"><strong>${message(code:'visitor.list.event.label', default:'Event')}</strong></g:link>
				</li>		
			</ul>
		</div>
	
		<div class="col-md-12">

			<table class="table table-bordered margin-top-medium">
				<thead>
					<tr>
						<th width=30>#</th>					
						<th><g:message code="visitor.gender.label" default="Gender" /></th>
						<th><g:message code="visitor.fullName.label" default="Full Name" /></th>
						
						<th><g:message code="visitor.typeOfCard.label" default="Card Type" /></th>
						<th><g:message code="visitor.numberCard.label" default="Card Number" /></th>
						<th><g:message code="visitor.visit.expectedArrivingDateTime.label" default="Start Date of the Visit" /></th>
						<th><g:message code="visitor.visit.expectedDepartureDateTime.label" default="End Date of the Visit" /></th>
						
						
					</tr>
				</thead>
				<tbody>
					<g:each in="${visitorInstanceList}" status="i" var="visitorInstance">
						<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							<td>
								<strong>${i+1}</strong>
							</td>
							<td>
								<strong>${visitorInstance?.gender}</strong>
							</td>
							<td>
								<strong>${visitorInstance?.lastName} ${visitorInstance?.firstName}</strong>
							</td>
							<td>
								<span class="label label-primary">
									<g:if test="${visitorInstance?.typeOfCard == "PC"}">
										${message(code:'visitor.typeOfCard.PC')}
									</g:if>
									<g:elseif test="${visitorInstance?.typeOfCard == "CNI"}">
										${message(code:'visitor.typeOfCard.CNI')}
									</g:elseif>
									<g:elseif test="${visitorInstance?.typeOfCard == "PASSPORT"}">
										${message(code:'visitor.typeOfCard.PASSPORT')}
									</g:elseif>
									<g:elseif test="${visitorInstance?.typeOfCard == "LAISSEZPASSER"}">
										${message(code:'visitor.typeOfCard.LAISSEZPASSER')}
									</g:elseif>
									<g:elseif test="${visitorInstance?.typeOfCard == "CARTECONSULAIRE"}">
										${message(code:'visitor.typeOfCard.CARTECONSULAIRE')}
									</g:elseif>
									<g:elseif test="${visitorInstance?.typeOfCard == "OTHER"}">
										${message(code:'visitor.typeOfCard.OTHER')}
									</g:elseif>
									<g:else><strong>N/A</strong></g:else>
								</span>	
							</td>
							<td>
								<strong>${visitorInstance?.numberCard}</strong>
							</td>
							<td>
								<strong><g:formatDate date="${visitorInstance?.visit?.expectedArrivingDateTime}" format="dd MMM yyyy HH:mm"/></strong>
							</td>
							<td>
								<strong><g:formatDate date="${visitorInstance?.visit?.expectedDepartureDateTime}" format="dd MMM yyyy HH:mm"/></strong>
							</td>
							<td width=50>
								<g:link class="btn btn-sm btn-default" controller="visitor" action="showManagement" id="${visitorInstance.id}">
									<i class="glyphicon glyphicon-share"></i>
								</g:link>
							</td>						
						</tr>
					</g:each>
				</tbody>
			</table>
			
			<div class="container">
				<bs:paginate total="${visitorInstanceCount}" params="${[perno:staffInstance?.perno, visitType:codeVisitType]}"/>
			</div>
		</div>
	
	</div>
	
</section>

</body>

</html>
