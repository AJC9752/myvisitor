<%@ page import="org.afdb.Staff" %>

<g:set var="lang" value="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE' ?: org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().substring(0,2)}"/>

<br/>

	<div class="row">
		
		<div class="col-md-6">
		
			<div class="panel panel-primary">
			
				<div class="panel-heading">
				   	<h3 class="panel-title center">
				   		<strong>${message(code:'generalInformation.label', default:'General Information')}</strong>
				   	</h3>
				</div>
				
				<div class="row">
	
					<div class="col-md-6">
					
						<div class="${hasErrors(bean: staffInstance, field: 'perno', 'error')} ">
							<label for="perno" class="control-label"><g:message code="staff.perno.label" default="Perno" /></label>
							<div>
								<g:field class="form-control" style="width: 60%;" name="perno" type="number" value="${staffInstance.perno}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'perno', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'firstName', 'error')} ">
							<label for="firstName" class="control-label"><g:message code="staff.firstName.label" default="First Name" /></label>
							<div>
								<g:textField class="form-control" name="firstName" value="${staffInstance?.firstName}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'firstName', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'position', 'error')} ">
							<label for="position" class="control-label"><g:message code="staff.position.label" default="Position" /></label>
							<div>
								<g:textField class="form-control" name="position" value="${staffInstance?.position}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'position', 'error')}</span>
							</div>
						</div>
			
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'orgUnit', 'error')} ">
							<label for="orgUnit" class="control-label"><g:message code="staff.orgUnit.label" default="Org Unit" /></label>
							<div>
								<g:select class="form-control" id="orgUnit" name="orgUnit.id" from="${org.afdb.OrgUnit.list()}" optionKey="id" value="${staffInstance?.orgUnit?.id}" noSelection="['null': '']"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'orgUnit', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'categorie', 'error')} ">
							<label for="categorie" class="control-label"><g:message code="staff.categorie.label" default="Categorie" /></label>
							<div>
								<g:textField class="form-control" name="categorie" value="${staffInstance?.categorie}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'categorie', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'cell', 'error')} ">
							<label for="cell" class="control-label"><g:message code="staff.cell.label" default="Cell" /></label>
							<div>
								<g:textField class="form-control" name="cell" value="${staffInstance?.cell}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'cell', 'error')}</span>
							</div>
						</div>
			
					</div>
					
					<div class="col-md-6">
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'gender', 'error')} ">
							<label for="gender" class="control-label"><g:message code="staff.gender.label" default="Gender" /></label>
							<div>
								<g:textField class="form-control" style="width: 60%;" name="gender" value="${staffInstance?.gender}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'gender', 'error')}</span>
							</div>
						</div>
			
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'lastName', 'error')} ">
							<label for="lastName" class="control-label"><g:message code="staff.lastName.label" default="Last Name" /></label>
							<div>
								<g:textField class="form-control" name="lastName" value="${staffInstance?.lastName}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'lastName', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'email', 'error')} ">
							<label for="email" class="control-label"><g:message code="staff.email.label" default="Email" /></label>
							<div>
								<g:textField class="form-control" name="email" value="${staffInstance?.email}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'email', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'orgUnitCode', 'error')} ">
							<label for="orgUnitCode" class="control-label"><g:message code="staff.orgUnitCode.label" default="Org Unit Code" /></label>
							<div>
								<g:textField class="form-control" name="orgUnitCode" value="${staffInstance?.orgUnitCode}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'orgUnitCode', 'error')}</span>
							</div>
						</div>
			
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'grade', 'error')} ">
							<label for="grade" class="control-label"><g:message code="staff.grade.label" default="Grade" /></label>
							<div>
								<g:textField class="form-control" name="grade" value="${staffInstance?.grade}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'grade', 'error')}</span>
							</div>
						</div>
						
						<div class="control-group fieldcontain ${hasErrors(bean: staffInstance, field: 'extension', 'error')} ">
							<label for="extension" class="control-label"><g:message code="staff.extension.label" default="Extension" /></label>
							<div>
								<g:textField class="form-control" name="extension" value="${staffInstance?.extension}"/>
								<span class="help-inline">${hasErrors(bean: staffInstance, field: 'extension', 'error')}</span>
							</div>
						</div>
						
					</div>
				</div>				
			</div>
		</div>
		
		<div class="col-md-6"></div>
		
	</div>

			

			

			

			

			

			

			

			

