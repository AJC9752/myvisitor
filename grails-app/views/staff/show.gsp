
<%@ page import="org.afdb.Staff" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'staff.label', default: 'Staff')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-staff" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.perno.label" default="Perno" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "perno")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.firstName.label" default="First Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "firstName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.lastName.label" default="Last Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "lastName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.position.label" default="Position" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "position")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.orgUnit.label" default="Org Unit" /></td>
				
				<td valign="top" class="value"><g:link controller="orgUnit" action="show" id="${staffInstance?.orgUnit?.id}">${staffInstance?.orgUnit?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.gender.label" default="Gender" /></td>
				
				<td valign="top" class="value">
					<strong>
						<g:if test="${staffInstance?.gender == '1'}">
							M
						</g:if>
						<g:elseif test="${staffInstance?.gender == '2'}">
							F
						</g:elseif>
					</strong>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.grade.label" default="Grade" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "grade")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.categorie.label" default="Categorie" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "categorie")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.orgUnitCode.label" default="Org Unit Code" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "orgUnitCode")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.cell.label" default="Cell" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "cell")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="staff.extension.label" default="Extension" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: staffInstance, field: "extension")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
