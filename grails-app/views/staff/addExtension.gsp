<%@ page import="org.afdb.Staff" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'addExtension.label', default: 'Add Extension')}" />
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

	<section id="view-profil" class="first">
		
		<div class="alert alert-info">
			<div class="row">
				<div class="col-md-1">
					<span class="label label-primary">Information!</span> 
				</div>
				<div class="col-md-11">
					<strong>${message(code:'information.staff.addInformation.label', default:'Please add your extension')}</strong>
				</div>
			</div>
		</div>
				
		<div class="row">
		
			<div class="col-md-2">	
			</div>
			
			<div class="col-md-8">
				<g:form action="saveAddExtension" controller="staff" class="form-horizontal" >
					<div class="row">
				
						<div class="col-md-12">
								
							<div class="panel panel-primary">
							
								<div class="panel-heading">
							    	<h3 class="panel-title center">
							    		<strong>${message(code:'staff.addExtension.label', default:'Add Extension')}</strong>
							    	</h3>
							 	</div>
							 	
							 	<div class="panel-body">
							 		
							 		<div class="row">
							 			
							 			<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="extension" class="col-sm-4 control-label"><g:message code="staff.extension.label" default="Extension" /><span class="required-indicator">*</span></label>
												<div class="col-sm-8">
													<g:textField class="form-control" name="extension" value="${staffInstance?.extension}" style="width:100%;"/>
													<span class="help-inline">${hasErrors(bean: staffInstance, field: 'extension', 'error')}</span>
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
									</div>
									
								</div>
								
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.save.label', default: 'Save')}" />
										</div>
										<div class="col-md-3">
										</div>
									</div>
								</div>
								
							</div>
							
						</div>
					
					</div>
			
			
				</g:form> 
			</div>
			
			<div class="col-md-2">	
			</div>
			
		</div>
	
	</section>
</body>

</html>