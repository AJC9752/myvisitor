<%@ page import="org.afdb.Staff" %>

<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<title><g:message code="default.searchlist.label" default="Search Results"/></title>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	<g:set var="lang" value="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE' ?: org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().substring(0,2)}"/>
	
	<g:javascript>
		jQuery(function ($) {
	        $("#idSend").tooltip()
	    });
	</g:javascript>
</head>

<body>
	
	<div class="row">
		
		<div class="col-md-3">
		</div>
		
		<div class="col-md-6">
			<h2 class="center">${message(code:'searchList.label', default:'Search Results')}</h2>
			<br/>
		</div>
		
		<div class="col-md-3">
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<g:render template="/_menu/search"/>
		</div>
	</div> 

	<div id="list-phase" class="content scaffold-list" role="main">
	    	
    	<div id="detailsTemplate" style="margin-top: 50px;"> <%-- TEMPLATE DETAILS --%>
			<g:if test="${staffInstanceList}">	    	
    			<g:render template="/staff/list"/>
	   		</g:if>
    		<g:if test="${orgUnitInstanceList}">
    			<g:render template="/orgUnit/list"/>
    		</g:if>
    	</div>
	
	</div>	

</body>

</html>
