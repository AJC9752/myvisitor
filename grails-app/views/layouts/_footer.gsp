<footer class="footer">
	<div class="container">
		<h6>&copy; Developed by <strong>CHIS</strong>
			<%-- 	
			<g:unless env="production">
				<span>under Grails</span>
	    		<font color="red">[ Test mode ]</font>
			</g:unless>
			--%>	
		</h6>
	
		<p>
			This application is a tool for visitor management in collaboration with PSEC and CHIS.  
		</p>
		
	</div>
</footer>

<%-- 
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-2">
				<h4>Product</h4>
				<ul class="unstyled">
					<li>
			    		<i class="icon-home"></i>
						<a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a>
					</li>
					<li>
			    		<i class="icon-eye-open"></i>
						<a href="${createLink(uri: '/')}"><g:message code="default.tour.label"/></a>
					</li>
					<li>
			    		<i class="icon-money"></i>
						<a href="${createLink(uri: '/')}"><g:message code="default.pricing.label"/></a>
					</li>
					<li>
			    		<i class="icon-comments"></i>
						<a href="${createLink(uri: '/')}"><g:message code="default.faq.label"/></a>
					</li>
				</ul>
			</div>
			<div class="col-lg-2">
				<h4>Company</h4>
				<ul class="unstyled">
					<li>
			    		<i class="icon-info-sign"></i>
						<a href="${createLink(uri: '/about')}">
							<g:message code="default.about.label"/>
						</a>
					</li>
					<li>
			    		<i class="icon-envelope"></i>
						<a href="${createLink(uri: '/contact')}">
							<g:message code="default.contact.label"/>
						</a>
					</li>
				</ul>
			</div>
			
		</div>
	
	
		<p class="pull-right"><a href="#">Back to top</a></p>
	</div>
</footer>
--%>