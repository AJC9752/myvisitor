
<%@ page import="org.afdb.OrgUnit" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'orgUnit.label', default: 'OrgUnit')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-orgUnit" class="first">

	<table class="table">
		<tbody>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.code.label" default="Code" /></td>
				<td valign="top" class="value">
					<strong>${fieldValue(bean: orgUnitInstance, field: "code")}</strong>
				</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.name.label" default="Name" /></td>
				<td valign="top" class="value">${fieldValue(bean: orgUnitInstance, field: "name")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.orgUnit.label" default="Org Unit" /></td>
				<td valign="top" class="value">
					<%--
					<g:link controller="orgUnit" action="show" id="${orgUnitInstance?.orgUnit?.id}"> 
						${orgUnitInstance?.orgUnit?.encodeAsHTML()}
					</g:link>
					--%>
				</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.category.label" default="Category" /></td>
				<td valign="top" class="value">
					<strong>${fieldValue(bean: orgUnitInstance, field: "category")}</strong>
				</td>
			</tr>
			
			<%-- 
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.manager.label" default="Manager" /></td>
				<td valign="top" class="value">
					<g:link controller="staff" action="show" id="${orgUnitInstance?.manager?.id}">${orgUnitInstance?.manager?.encodeAsHTML()}</g:link>
				</td>
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.businessPartner.label" default="Business Partner" /></td>
				<td valign="top" class="value">
					<g:link controller="staff" action="show" id="${orgUnitInstance?.businessPartner?.id}">${orgUnitInstance?.businessPartner?.encodeAsHTML()}</g:link>
				</td>
			</tr>
			--%>
			<tr class="prop">
				<td valign="top" class="name"><g:message code="orgUnit.lastUpdated.label" default="Last Updated" /></td>
				<td valign="top" class="value"><g:formatDate date="${orgUnitInstance?.lastUpdated}" format="dd MMM yyyy"/></td>
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
