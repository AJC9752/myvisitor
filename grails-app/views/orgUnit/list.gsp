
<%@ page import="org.afdb.OrgUnit" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'orgUnit.label', default: 'OrgUnit')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-orgUnit" class="first">
	
	<g:render template="list"/>
	
</section>

</body>

</html>
