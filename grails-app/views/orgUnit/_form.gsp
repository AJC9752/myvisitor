<%@ page import="org.afdb.OrgUnit" %>



			<div class="control-group fieldcontain ${hasErrors(bean: orgUnitInstance, field: 'code', 'error')} ">
				<label for="code" class="control-label"><g:message code="orgUnit.code.label" default="Code" /></label>
				<div class="controls">
					<g:textField name="code" value="${orgUnitInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: orgUnitInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: orgUnitInstance, field: 'name', 'error')} ">
				<label for="name" class="control-label"><g:message code="orgUnit.name.label" default="Name" /></label>
				<div class="controls">
					<g:textField name="name" value="${orgUnitInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: orgUnitInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: orgUnitInstance, field: 'orgUnit', 'error')} ">
				<label for="orgUnit" class="control-label"><g:message code="orgUnit.orgUnit.label" default="Org Unit" /></label>
				<div class="controls">
					<g:select id="orgUnit" name="orgUnit.id" from="${org.afdb.OrgUnit.list()}" optionKey="id" value="${orgUnitInstance?.orgUnit?.id}" class="many-to-one" noSelection="['null': '']"/>
					<span class="help-inline">${hasErrors(bean: orgUnitInstance, field: 'orgUnit', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: orgUnitInstance, field: 'category', 'error')} ">
				<label for="category" class="control-label"><g:message code="orgUnit.category.label" default="Category" /></label>
				<div class="controls">
					<g:select name="category" from="${orgUnitInstance.constraints.category.inList}" value="${orgUnitInstance?.category}" valueMessagePrefix="orgUnit.category" noSelection="['': '']"/>
					<span class="help-inline">${hasErrors(bean: orgUnitInstance, field: 'category', 'error')}</span>
				</div>
			</div>

