<%@ page import="org.afdb.OrgUnit" %>

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'orgUnit.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'orgUnit.name.label', default: 'Name')}" />
						
				<g:sortableColumn property="category" title="${message(code: 'orgUnit.category.label', default: 'Category')}" />
			
				<g:sortableColumn property="lastUpdated" title="${message(code: 'orgUnit.lastUpdated.label', default: 'Last Updated')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${orgUnitInstanceList}" status="i" var="orgUnitInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td>
					<g:link action="show" id="${orgUnitInstance.id}">
						<strong>${fieldValue(bean: orgUnitInstance, field: "code")}</strong>
					</g:link>
				</td>
			
				<td>${fieldValue(bean: orgUnitInstance, field: "name")}</td>
				
				<td>${fieldValue(bean: orgUnitInstance, field: "category")}</td>
			
				<td>
					<g:formatDate date="${orgUnitInstance?.lastUpdated}" format="dd MMMM yyyy"/>
				</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${orgUnitInstanceTotal}" />
	</div>
