<%@ page import="org.afdb.rs.Location" %>
<%@ page import="org.afdb.User" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<%-- 
	<g:set var="entityName" value="${message(code: 'selectLocation.label', default: 'Select Location')}" />
	--%>
	<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	<title><g:message code="selectLocation.label" default="Select Location"/></title>
</head>

<body>

	<section id="view-profil" class="first">
		
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<span class="label label-warning">${message(code:'warning.label')}</span>
			<strong>${message(code:'gate.location.isempty.label')}</strong>
		</div>				
		<div class="row">
		
			<div class="col-md-2">	
			</div>
			
			<div class="col-md-8">
				<div class="row">
				
					<div class="col-md-12">
							
						<div class="panel panel-default">
						
							<div class="${hasErrors(bean: staffInstance, field: 'forms', 'error')}">
								<div class="center">
									<legend>${message(code:'location.selectLocation.label', default:'Select Location')}</legend>
								</div>
								
					 			<g:form controller="location" action="addLocationInSession">
									<g:select class="form-control center" 
										name="location"
										from="${org.afdb.rs.Location.list()}"
										onchange="submit()"
										optionKey="code"
										style="width:50%;" noSelection="${[null:'Select One...']}" />
								</g:form>
								
							</div>
						</div>	
					</div>
				</div>
				 
			</div>
			
			<div class="col-md-2">	
			</div>
			
		</div>
	
	</section>
</body>

</html>