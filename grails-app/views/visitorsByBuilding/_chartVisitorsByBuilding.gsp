	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	//alert("Test Jquery");
	$(function () {
		var chart = new Highcharts.Chart({
			chart: {
				plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie',
				renderTo: 'containerPieByBuilding'
			},
	        title: {
	        	text: ''//'<b>${message(code:'visitorsByBuilding.list.label', default:'Nombre de Visiteur par Batiment')}</b>'
	        },
	        credits: {
				// Remove highcharts.com credits link from chart footer.
				enabled: false,
			},
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: 'Brands',
	            colorByPoint: true,
	            data: [<% visitorsByBuildingData.each{
					out<< "{ name: '${it[0]}' ,"
					out<< "y: ${(it[1]*100)/visitorsByBuildingTotal} },"
				} %>]
	        }]
	    });
	});
	</script>
	
	<g:javascript src="highcharts.js"/>
	<%-- 
	<g:javascript src="modules/exporting.js"/>
	--%>
	<div id="containerPieByBuilding"></div>