	<g:each in="${visitorsByBuildingList}" status="i" var="visitorsByBuildingInstance">
		<li class="${(i % 2) == 0 ? 'odd' : 'even'} list-group-item">
			<span class="badge">${fieldValue(bean: visitorsByBuildingInstance, field: "nbVisitor")}</span>
			<strong>${fieldValue(bean: visitorsByBuildingInstance, field: "nameBuilding")}</strong>
		</li>
	</g:each>
	
	<%-- 
	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
				<th><g:message code="visitorsByBuilding.codeBuilding.label" default="Code Building" /></th>
				<th><g:message code="visitorsByBuilding.nameBuilding.label" default="Name Building" /></th>
				<th width=100><g:message code="visitorsByBuilding.nbVisitor.label" default="Nb Visiteurs" /></th>
			</tr>
		</thead>
		<tbody>
			<ul class="list-group">
				<g:each in="${visitorsByBuildingList}" status="i" var="visitorsByBuildingInstance">
					<li class="list-group-item">
						<span class="badge">${fieldValue(bean: visitorsByBuildingInstance, field: "nbVisitor")}</span>
						${fieldValue(bean: visitorsByBuildingInstance, field: "nameBuilding")}
					</li>
				</g:each>
			</ul>
			<g:each in="${visitorsByBuildingList}" status="i" var="visitorsByBuildingInstance">
				<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
					<td>
						<strong>
							${fieldValue(bean: visitorsByBuildingInstance, field: "codeBuilding")}
						</strong>
					</td>
					<td>
						${fieldValue(bean: visitorsByBuildingInstance, field: "nameBuilding")}
					</td>
					
					<td>
						<span class="pull-right">
							${fieldValue(bean: visitorsByBuildingInstance, field: "nbVisitor")}
						</span>
					</td>
				</tr>
			</g:each>
		</tbody>
	</table>
	--%>