<html>
	<head>
		<title>E-VISITOR</title>
	</head>
	<body>

		<p>
			<strong>***** E-VISITOR *****</strong></p>
		<p>
			<strong><u>Français</u></strong></p>
		<p>
			Chèr(e) <strong>${askByStaff?.firstName} ${askByStaff?.lastName}</strong></p>
		<p>
			Votre demande de visite a été <strong>refusée</strong>.</p>
		<p>	
			Refusé par: <strong>${declinedBy?.firstName} ${declinedBy?.lastName}</strong>.</p>
		<p>
			Merci</p>
			
		<hr />
		<p>
			<strong><u>English</u></strong></p>
		<p>
			Dear Colleague(s)</p>
		<p>
			Thank you.</p>
			
		<hr />

	</body>
</html>