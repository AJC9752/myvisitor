
<%@ page import="org.afdb.User" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-user" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="login" title="${message(code: 'user.login.label', default: 'Login')}" />
						
				<g:sortableColumn property="fullname" title="${message(code: 'user.fullname.label', default: 'Fullname')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'user.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="role" title="${message(code: 'user.role.label', default: 'Role')}" />
			
				<g:sortableColumn property="perno" title="${message(code: 'user.perno.label', default: 'Perno')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${userInstanceList}" status="i" var="userInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "login")}</g:link></td>
						
				<td>${fieldValue(bean: userInstance, field: "fullname")}</td>
			
				<td>${fieldValue(bean: userInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: userInstance, field: "role")}</td>
			
				<td>${fieldValue(bean: userInstance, field: "perno")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="container">
		<bs:paginate total="${userInstanceTotal}" />
	</div>
</section>

</body>

</html>
