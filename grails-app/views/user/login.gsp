<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<title><g:message code="springSecurity.login.title" default="Visitor Reception System"/></title>
	
	<g:set var="layout_nomainmenu"		value="${true}" scope="request"/>
	<g:set var="layout_nosecondarymenu" value="${true}" scope="request" />
	
	<style type="text/css">
		.panel-heading {
		    padding: 5px 15px;
		}
		
		.panel-footer {
			padding: 1px 15px;
			color: #A0A0A0;
		}
		
		.profile-img {
			width: 96px;
			height: 96px;
			margin: 0 auto 10px;
			display: block;
			-moz-border-radius: 50%;
			-webkit-border-radius: 50%;
			border-radius: 50%;
		}
	</style>
</head>

<body>
	
	<div class="row">
		<div class="col-md-12">
			<g:if test="${flash.error}">
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<span class="label label-danger">${message(code:'default.error.label', default:'Error')}</span> 
					${flash.error}
				</div>
			</g:if>
		</div>
	</div>
	
    <div class="container" style="margin-top:40px">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong> Log in to continue</strong>
					</div>
					<div class="panel-body">
						<g:form role="form" id='loginForm' class='form-horizontal' action="authenticate" controller="user"  method='POST' autocomplete='off'>
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<%-- 
												<input class="form-control" placeholder="Username" name="username" type="text" autofocus>
												--%>
 
												<input type='text' class='form-control' name='username' id='username' placeholder='ex: NDN4280' autocomplete="off"/>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>
												<%-- 
												<input class="form-control" placeholder="Password" name="password" type="password" value="">
												--%>
												<input type='password' class='form-control' name='password' id='password' placeholder="Password" autocomplete="off"/>
											</div>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block" value="Log in">
											<%-- 
											<button type='submit' id="submit" class="btn btn-success pull-right">
												<i class="glyphicon glyphicon-log-in"></i> 
												${message(code: "springSecurity.login.button")}
											</button>
											--%>
										</div>
									</div>
								</div>
							</fieldset>
						</g:form>
					</div>
					<div class="panel-footer ">
						Use your normal windows credentials
					</div>
                </div>
			</div>
		</div>
	</div>
</body>	
	