
<%@ page import="org.afdb.User" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-user" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.login.label" default="Login" /></td>
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "login")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.fullname.label" default="Fullname" /></td>
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "fullname")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.email.label" default="Email" /></td>
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "email")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.role.label" default="Role" /></td>
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "role")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="user.perno.label" default="Perno" /></td>
				<td valign="top" class="value">${fieldValue(bean: userInstance, field: "perno")}</td>
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
