<%@ page import="org.afdb.User" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-user" class="first">
	
	<div class="row">
		<g:form controller="${params.controller}" action="search" method="post" class="navbar-form navbar-left" >
			<div class="well well-sm">
				<div class="form-group fieldcontain text-center">
					<input name="query" type="text" class="form-control search-query" placeholder="${message(code: 'search.placeholder', default: 'Search ...')}" value="${query}">
				</div>
				<g:render template="/_menu/searchButton"/>
			</div>
		</g:form>
	</div>
	
	<div class="row">
	
		<div class="col-md-6">
			<g:each in="${userInstanceList}" status="i" var="userInstance">
				<g:if test="${(i % 2) == 0}">
					<g:link controller="user" action="show" id="${userInstance.id}">
						<div class="well well-sm">
						    <div class="row">
						        <div class="col-md-3">
									<img src="${'http://intranet3.afdb.org/sites/default/files/styles/square__50x50_/public/pictures/ID' + (userInstance?.perno)?.toInteger()+ '.png?itok=uP2zbkPX'}" alt="" class="img-circle img-responsive">
						        </div>
						        <div class="col-md-9">
						            <h3>${userInstance?.fullname}</h3> <small>${userInstance?.perno}</small>
						            <address>
						                ${userInstance?.email}
						                <br/>
						                <br/>
						                <span class="label label-success">
						                	${userInstance?.role}
						                </span>
						            </address>
						        </div>
						    </div>
						</div>
					</g:link>
				<br/>
				</g:if>
			</g:each>
		</div>
		
		<div class="col-md-6">
			<g:each in="${userInstanceList}" status="i" var="userInstance">
				<g:if test="${(i % 2) != 0}">
					<g:link controller="user" action="show" id="${userInstance.id}">
						<div class="well well-sm">
						    <div class="row">
						        <div class="col-md-3">
									<img src="${'http://intranet3.afdb.org/sites/default/files/styles/square__50x50_/public/pictures/ID' + (userInstance?.perno)?.toInteger()+ '.png?itok=uP2zbkPX'}" alt="" class="img-circle img-responsive">
						        </div>
						        <div class="col-md-9">
						            <h3>${userInstance?.fullname}</h3> <small>${userInstance?.perno}</small>
						            <address>
						                ${userInstance?.email}
						                <br/>
						                <br/>
						                <span class="label label-success">
						                	${userInstance?.role}
						                </span>
						            </address>
						        </div>
						    </div>
						</div>
					</g:link>
				<br/>
				</g:if>
			</g:each>
		</div>
		
	</div>
	
	<div class="container">
		<bs:paginate total="${userInstanceTotal}" />
	</div>
</section>

</body>

</html>
