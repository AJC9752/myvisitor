<%@ page import="org.afdb.User" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
	<script type="text/javascript">
		$(function () {
	
	        $('body').on('click', '.list-group .list-group-item', function () {
	            $(this).toggleClass('active');
	        });
	        $('.list-arrows button').click(function () {
	            var $button = $(this), actives = '';
	            if ($button.hasClass('move-left')) {
	                actives = $('.list-right ul li.active');
	                actives.clone().appendTo('.list-left ul');
	                actives.remove();
	            } else if ($button.hasClass('move-right')) {
	                actives = $('.list-left ul li.active');
	                actives.clone().appendTo('.list-right ul');
	                actives.remove();
	            }
	        });
	        $('.dual-list .selector').click(function () {
	            var $checkBox = $(this);
	            if (!$checkBox.hasClass('selected')) {
	                $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
	                $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
	            } else {
	                $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
	                $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
	            }
	        });
	        $('[name="SearchDualList"]').keyup(function (e) {
	            var code = e.keyCode || e.which;
	            if (code == '9') return;
	            if (code == '27') $(this).val(null);
	            var $rows = $(this).closest('.dual-list').find('.list-group li');
	            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
	            $rows.show().filter(function () {
	                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
	                return !~text.indexOf(val);
	            }).hide();
	        });
	
	    });
	</script>
</head>

<body>

<section id="edit-user" class="first">

	<g:hasErrors bean="${userInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${userInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${userInstance?.id}" />
		<g:hiddenField name="version" value="${userInstance?.version}" />
		<fieldset class="form">
			<g:render template="form"/>
		</fieldset>
		<div class="form-actions">
			<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
			<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		</div>
	</g:form>

</section>
			
</body>

</html>
