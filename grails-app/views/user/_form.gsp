<%@ page import="org.afdb.User" %>

	<div class="row">
		
		<div class="col-md-5">
			
			<div class="${hasErrors(bean: userInstance, field: 'login', 'error')} ">
				<label for="login" class="control-label"><g:message code="user.login.label" default="Login" /><span class="required-indicator">*</span></label>
				<div>
					<g:textField class="form-control" name="login" required="" value="${userInstance?.login}"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'login', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: userInstance, field: 'password', 'error')} ">
				<label for="password" class="control-label"><g:message code="user.password.label" default="Password" /></label>
				<div>
					<g:field class="form-control" type="password" name="password" value="${userInstance?.password}"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'password', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: userInstance, field: 'fullname', 'error')} ">
				<label for="fullname" class="control-label"><g:message code="user.fullname.label" default="Fullname" /></label>
				<div>
					<g:textField class="form-control" name="fullname" value="${userInstance?.fullname}"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'fullname', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: userInstance, field: 'email', 'error')} ">
				<label for="email" class="control-label"><g:message code="user.email.label" default="Email" /></label>
				<div>
					<g:field class="form-control" type="email" name="email" value="${userInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: userInstance, field: 'role', 'error')} ">
				<label for="role" class="control-label"><g:message code="user.role.label" default="Role" /></label>
				<div>
					<g:select class="form-control" name="role" from="${userInstance.constraints.role.inList}" value="${userInstance?.role}" valueMessagePrefix="user.role" noSelection="['': '']"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'role', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: userInstance, field: 'perno', 'error')} ">
				<label for="perno" class="control-label"><g:message code="user.perno.label" default="Perno" /></label>
				<div>
					<g:field class="form-control" name="perno" type="number" value="${userInstance.perno}"/>
					<span class="help-inline">${hasErrors(bean: userInstance, field: 'perno', 'error')}</span>
				</div>
			</div>				

		</div>
			
		<div class="col-md-7">
			<br/>
			<div class="row">
		
		        <div class="dual-list list-left col-md-6">
		            <div class="well text-right">
		                <div class="row">
		                    <div class="col-md-10">
		                        <div class="input-group">
		                            <span class="input-group-addon glyphicon glyphicon-search"></span>
		                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
		                        </div>
		                    </div>
		                    <div class="col-md-2">
		                        <div class="btn-group">
		                            <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>
		                        </div>
		                    </div>
		                </div>
		                <ul class="list-group">
		                    <li class="list-group-item">bootstrap-duallist <a href="https://github.com/bbilginn/bootstrap-duallist" target="_blank">github</a></li>
		                    <li class="list-group-item">Dapibus ac facilisis in</li>
		                    <li class="list-group-item">Morbi leo risus</li>
		                    <li class="list-group-item">Porta ac consectetur ac</li>
		                    <li class="list-group-item">Vestibulum at eros</li>
		                </ul>
		            </div>
		        </div>
		
		        <div class="list-arrows col-md-1 text-center">
		            <button class="btn btn-default btn-sm move-left">
		                <span class="glyphicon glyphicon-chevron-left"></span>
		            </button>
		
		            <button class="btn btn-default btn-sm move-right">
		                <span class="glyphicon glyphicon-chevron-right"></span>
		            </button>
		        </div>
		
		        <div class="dual-list list-right col-md-5">
		            <div class="well">
		                <div class="row">
		                    <div class="col-md-2">
		                        <div class="btn-group">
		                            <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>
		                        </div>
		                    </div>
		                    <div class="col-md-10">
		                    	<div class="pull-right">
		                        <div class="input-group">
		                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
		                            <span class="input-group-addon glyphicon glyphicon-search"></span>
		                        </div>
		                        </div>
		                    </div>
		                </div>
		                <ul class="list-group">
		                    <li class="list-group-item">Cras justo odio</li>
		                    <li class="list-group-item">Dapibus ac facilisis in</li>
		                    <li class="list-group-item">Morbi leo risus</li>
		                    <li class="list-group-item">Porta ac consectetur ac</li>
		                    <li class="list-group-item">Vestibulum at eros</li>
		                </ul>
		            </div>
		        </div>
		
			</div>
		</div>
		
	</div>

