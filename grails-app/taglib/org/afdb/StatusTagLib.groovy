package org.afdb

import org.afdb.rs.*

/**
 * StatusTagLib
 * A taglib library provides a set of reusable tags to help rendering the views.
 */
class StatusTagLib {
	
	def visitStatus ={attr, body->
		def status=attr.status
		def affichLabel, labelSatus
		//println "Status: ${status}"
		
		if (status?.code == VisitStatus.INPROGRESS){
			affichLabel="label-info"
			labelSatus=g.message(code:"visitStatus.inprogress.label")
		}else if (status?.code == VisitStatus.SUBMITTED){
			affichLabel="label-primary"
			labelSatus=g.message(code:"visitStatus.submitted.label")
		}else if (status?.code == VisitStatus.APPROVED){
			affichLabel="label-success"
			labelSatus=g.message(code:"visitStatus.approved.label")
		}else if (status?.code == VisitStatus.DECLINED){
			affichLabel="label-danger"
			labelSatus=g.message(code:"visitStatus.declined.label")
		}else if (status?.code == VisitStatus.CANCELED){
			affichLabel="label-warning"
			labelSatus=g.message(code:"visitStatus.canceled.label")
		}
		
		//println "AffichLabel: ${affichLabel}"
		out << " <span class='label ${affichLabel}'>${labelSatus}</span> "
	}
	
	def visitorStatus ={attr, body->
		def status=attr.status
		def affichLabel, labelSatus
		//println "Status: ${status}"
		
		if (status?.code == VisitorStatus.INITIAL){
			affichLabel="label-info"
			labelSatus=g.message(code:"visitorStatus.initial.label")
		}else if (status?.code == VisitorStatus.AWAITING){
			affichLabel="label-primary"
			labelSatus=g.message(code:"visitorStatus.awaiting.label")
		}else if (status?.code == VisitorStatus.DENIED_GATE){
			affichLabel="label-warning"
			labelSatus=g.message(code:"visitorStatus.deniedGate.label")
		}else if (status?.code == VisitorStatus.ENTRANCE){
			affichLabel="label-success"
			labelSatus=g.message(code:"visitorStatus.entrance.label")
		}else if (status?.code == VisitorStatus.DENIED_DESK){
			affichLabel="label-warning"
			labelSatus=g.message(code:"visitorStatus.deniedReception.label")
		}else if (status?.code == VisitorStatus.CHECKIN){
			affichLabel="label-success"
			labelSatus=g.message(code:"visitorStatus.checkin.label")
		}else if (status?.code == VisitorStatus.CHECKOUT){
			affichLabel="label-danger"
			labelSatus=g.message(code:"visitorStatus.checkout.label")
		}
		
		//println "AffichLabel: ${affichLabel}"
		out << " <span class='label ${affichLabel}'>${labelSatus}</span> "
	}
	
	def visitType ={attr, body->
		def type=attr.type
		def affichLabel, labelSatus
		//println "Status: ${status}"
		
		if (type?.code == VisitType.INDIVIDUAL){
			affichLabel="label-success"
			labelSatus=g.message(code:"visitType.individual.label")
		}else if (type?.code == VisitType.BUSINESS){
			affichLabel="label-primary"
			labelSatus=g.message(code:"visitType.business.label")
		}else if (type?.code == VisitType.EVENT){
			affichLabel="label-inverse"
			labelSatus=g.message(code:"visitType.event.label")
		}else if (type?.code == VisitType.FAMILY){
			affichLabel="label-danger"
			labelSatus=g.message(code:"visitType.family.label")
		}
		
		//println "AffichLabel: ${affichLabel}"
		out << " <span class='label ${affichLabel}'>${labelSatus}</span> "
	}
	
	def typeBadge ={attr, body->
		def badge=attr.badge
		def affichLabel, labelSatus
//		println "Badge: ${badge}"
		
		if (badge?.code == TypeBadge.VISITOR){
			affichLabel="label-success"
			labelSatus=g.message(code:"typeBadge.visitor.label")
		}else if (badge?.code == TypeBadge.FAMILY){
			affichLabel="label-primary"
			labelSatus=g.message(code:"typeBadge.family.label")
		}else if (badge?.code == TypeBadge.MC){
			affichLabel="label-danger"
			labelSatus=g.message(code:"typeBadge.mc.label")
		}else if (badge?.code == TypeBadge.SE){
			affichLabel="label-inverse"
			labelSatus=g.message(code:"typeBadge.se.label")
		}
		
		//println "AffichLabel: ${affichLabel}"
		out << " <span class='label ${affichLabel}'>${labelSatus}</span> "
	}
	
}
