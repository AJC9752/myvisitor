package org.afdb.utils
import org.afdb.Staff
import org.afdb.OrgUnit
/**
 * SearchController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class SearchController extends SecureController{

	
	def doSearch={
		def q=params?.q
		
		def listReq=[] //Liste des request
		def listReqDiv=[]
		
		def resultsStaff = Staff.findAllByFirstNameIlikeOrLastNameIlike("%${params?.q}%","%${params?.q}%")
		try
		{
			Integer.parseInt(params?.q)
			resultsStaff+=Staff.findAllByPerno(Integer.parseInt(params?.q))
		}
		catch(Exception e)
		{}
		
		def resultsDiv = OrgUnit.findAllByCodeIlikeOrLibelleFrIlikeOrLibelleEnIlike("%${params?.q}%","%${params?.q}%","%${params?.q}%")
		
		//System.out.println ">>resultsDiv modif:${resultsDiv?.count}"
		//println ">>resultsStaff:${resultsStaff}"
	
		def totalStaff= resultsStaff?.size()
		def totalDiv= resultsDiv?.size()
		
		flash.message="${totalStaff + totalDiv} results found"
		render (view:'listSearch',model:[staffInstanceList:resultsStaff, divisionInstanceList:resultsDiv])
	}
	
//	def index = { }
}
