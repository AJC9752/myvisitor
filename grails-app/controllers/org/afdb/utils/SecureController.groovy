package org.afdb.utils

abstract class SecureController {
	def beforeInterceptor = [ action: this.&auth,
							  except: [ 'authenticate', 'login', 'logout' ]
	]
	
	private auth() {
		debug()
		if (!session.user) {
			session.controller=params.controller
			session.action=params.action
			session.idReq=params.id
			session.params=params
			
			redirect(action: 'login', controller:'user')
			return false
		}
	}
	
	def debug(){
		response.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
		response.setHeader('Cache-Control',  'post-check=0, pre-check=0, false');
		response.setHeader("Pragma"," no-cache");
		response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
		response.setHeader('Expires', '0')
		
		println ">DEBUG - E-VISITOR: ${actionUri} called."
		def vparams= params.findAll{k,v->
				k!="password"
				}
		println ">E-VISITOR - params: ${vparams}"
	}
	
	def getApplicationName(){
		return grailsApplication.metadata['app.name']
	}
	
}