package org.afdb

import org.afdb.rs.*
import org.afdb.utils.SecureController;
import org.springframework.dao.DataIntegrityViolationException

/**
 * StaffController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class StaffController extends SecureController{

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def staffService
	
	def searchStaff={
		def q=params?.query
				
		def resultsStaff = Staff.findAllByFirstNameIlikeOrLastNameIlike("%${params?.query}%","%${params?.query}%")
//		println ">>resultsStaff:${resultsStaff}"
		try
		{
			Integer.parseInt(params?.query)
			resultsStaff+=Staff.findAllByPerno(Integer.parseInt(params?.query))
		}
		catch(Exception e)
		{}
	
		def resultsOrgUnit = OrgUnit.findAllByCodeIlikeOrNameIlike("%${params?.query}%","%${params?.query}%")
	
		def totalStaff= resultsStaff?.size()
		def totalOrgUnit= resultsOrgUnit?.size()
		
		flash.message="${totalStaff + totalOrgUnit} results found"
		
		render (view:'listSearch', model:[staffInstanceList:resultsStaff, staffInstanceTotal: totalStaff,
			orgUnitInstanceList:resultsOrgUnit, orgUnitInstanceTotal: totalOrgUnit])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }
	
	def addExtension(){
//		println "Staff | Add Extension"
		
		def staffInSession=staffService.currentStaff(session.user)
		def staff=Staff.findByPerno(staffInSession?.perno)
//		println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.staff.notFound.message', default: 'Please contact PSEC')
			redirect(action:"list")
			return
		}
		
		[staffInstance: staff]
	}
	
	def saveAddExtension(){
//		println "Staff | Save AddExtension"
		
		def staffInSession=staffService.currentStaff(session.user)
		def staffInstance=Staff.findByPerno(staffInSession?.perno)
//		println "Staff: ${staffInstance}"
		staffInstance.properties = params
		
		if (!staffInstance.save(flush: true)) {
			render(view: "addExtension", model: [staffInstance: staffInstance])
			return
		}

		flash.message = message(code: 'default.updated.message')		
		redirect(controller:"visiteForm", action:"create")
	}
	
	def visitorsList(Integer max, Integer offset, String sort, String order){
//		println "Staff | Visitors List"
		
		params.max = Math.min(max ?: 10, 100)
		params.offset = offset ?: 0  // require offset for pagination
		params.sort = sort ?: 'id'
		params.order = order ?: 'desc'
		
		def staffInstance = Staff.findByPerno(params.perno)
//		println "Staff: ${staffInstance}"
		if (!staffInstance){
			redirect(controller:"visiteForm", action: "dashboard")
            return
		}
		
		def visitType
		if (params.visitType){
			visitType=VisitType.findByCode(params.visitType)
		}else{
			visitType=VisitType.findByCode(VisitType.INDIVIDUAL)
		}
//		println "VisitType: ${visitType}"
		
		def visiteFormInstanceList=VisiteForm.findAllByAskByAndVisitType(staffInstance, visitType)*.visitors.id.flatten()
//		println "VisiteForm byStaff: ${visiteFormInstanceList}"
		
		def visitorInstanceList=Visitor?.findAllByIdInList(visiteFormInstanceList, params)
//		println "Visitor byStaff: ${visitorInstanceList}"
		
		def visitorInstanceCount= (visitorInstanceList) ? Visitor?.countByIdInList(visiteFormInstanceList) : 0	
		[visitorInstanceList: visitorInstanceList, visitorInstanceCount: visitorInstanceCount, 
			staffInstance:staffInstance, codeVisitType:visitType?.code]
	}
	
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        [staffInstanceList: Staff.list(params), staffInstanceTotal: Staff.count()]
    }

    def create() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        [staffInstance: new Staff(params)]
    }

    def save() {
        def staffInstance = new Staff(params)
        if (!staffInstance.save(flush: true)) {
            render(view: "create", model: [staffInstance: staffInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'staff.label', default: 'Staff'), staffInstance.id])
        redirect(action: "show", id: staffInstance.id)
    }

    def show() {
		
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        def staffInstance = Staff.get(params.id)
        if (!staffInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "list")
            return
        }

        [staffInstance: staffInstance]
    }

    def edit() {
		
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        def staffInstance = Staff.get(params.id)
        if (!staffInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "list")
            return
        }

        [staffInstance: staffInstance]
    }

    def update() {
        def staffInstance = Staff.get(params.id)
        if (!staffInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (staffInstance.version > version) {
                staffInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'staff.label', default: 'Staff')] as Object[],
                          "Another user has updated this Staff while you were editing")
                render(view: "edit", model: [staffInstance: staffInstance])
                return
            }
        }

        staffInstance.properties = params

        if (!staffInstance.save(flush: true)) {
            render(view: "edit", model: [staffInstance: staffInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'staff.label', default: 'Staff'), staffInstance.id])
        redirect(action: "show", id: staffInstance.id)
    }

    def delete() {
		
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        def staffInstance = Staff.get(params.id)
        if (!staffInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "list")
            return
        }

        try {
            staffInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'staff.label', default: 'Staff'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
