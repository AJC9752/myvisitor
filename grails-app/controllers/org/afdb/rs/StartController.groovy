package org.afdb.rs

import org.afdb.utils.SecureController

/**
 * StartController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class StartController  extends SecureController{

//	static scaffold = true
	def index = { 
		println " Start OK"
		if (!session.user.admin){
			redirect(action:"list", controller:"visiteForm")
		}
	}
	
	
}
