package org.afdb.rs

import org.afdb.*
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Formatter.DateTime

import groovy.time.TimeCategory

import org.afdb.utils.SecureController;
import org.springframework.dao.DataIntegrityViolationException

/**
 * VisiteFormController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class VisiteFormController extends SecureController{

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def staffService
	def mailingService
	def dateFormat=new SimpleDateFormat("dd-MM-yyyy HH:mm")
	def dateFormatBs=new SimpleDateFormat("yyyy-MM-dd HH:mm")
	SimpleDateFormat dF=new SimpleDateFormat("yyyy-MM-dd")
	SimpleDateFormat dTF=new SimpleDateFormat("yyyy-MM-dd HH:mm")
	
    def index() {
        //println "VisiteForm | Index"
		
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [visiteFormInstanceList: VisiteForm.list(params), visiteFormInstanceTotal: VisiteForm.count()]
    }
	
	def sideBar(){
		//println "VisiteForm | SideBar"
		
		
	}
	
	def navBar(){
		//println "VisiteForm | NavBar"
		
		
	}
	
	def dashboard() {
		//println "VisiteForm | Dashboard"
		
		////println ">>>Params: ${params}"
		
		def currentYear=new Date().format("yyyy").toInteger()
		
		def codeVisitorNumber, codePeriod
		def visitorsByBuildingList, visitorsByBuildingData, visitorsByBuildingTotal
		def visitorsInDayList, visitorsInDayData, visitorsInDayTotal
		def visitorsPerPeriod, visitorsPerPeriodData, visitorsPerPeriodTotal
		Date day
		if (params.visitorNumber == "BYDAY"){
			/*THISDAY Visitors*/
			codeVisitorNumber="BYDAY"
			
			/*VISITEURS PAR PERIODE*/
			if (params.daySelected){
				day= dF?.parse(params?.daySelected)
				
				////println "Day: ${day}"
				visitorsPerPeriod= VisitorsPerPeriod.createCriteria().list() {
					projections {
						eq('checkinDate', day)
						groupProperty ('codeBuilding')
						groupProperty ('nameBuilding')
						count ('visitor')
					}
				}
			}
//			//println "CodePeriod: ${codePeriod}"
			visitorsPerPeriodData=visitorsPerPeriod
			////println "VisitorsPerPeriod Data: ${visitorsPerPeriodData}"
			visitorsPerPeriodTotal=visitorsPerPeriodData?.sum{it[2]}
			////println "VisitorsPerPeriod Total: ${visitorsPerPeriodTotal}"
			
		}else if (params.visitorNumber == "MONTH"){
			/*PERIOD Visitors*/
			codeVisitorNumber="MONTH"
				
			/*VISITEURS PAR PERIODE*/
			def startDate, endDate
			if (params.monthYearSelected){
				startDate= dF?.parse("${params.monthYearSelected_year}-${params.monthYearSelected_month}-01")
				////println "StartDate: ${startDate}"
				endDate= dF?.parse("${params.monthYearSelected_year}-${params.monthYearSelected_month}-31")
				////println "EndDate: ${endDate}"
				visitorsPerPeriod= VisitorsPerPeriod.createCriteria().list() {
					projections {
						between("checkinDate", startDate, endDate)
						groupProperty ('codeBuilding')
						groupProperty ('nameBuilding')
						count ('visitor')
					}
				}				
			}
//			//println "CodePeriod: ${codePeriod}"
			visitorsPerPeriodData=visitorsPerPeriod
			////println "VisitorsPerPeriod Data: ${visitorsPerPeriodData}"
			visitorsPerPeriodTotal=visitorsPerPeriodData?.sum{it[2]}
			////println "VisitorsPerPeriod Total: ${visitorsPerPeriodTotal}"
			
		}else{
			
			/*THISDAY Visitors*/
			codeVisitorNumber="THISDAY"
			
			/*VISITEURS POUR CE JOUR*/
			visitorsInDayList=VisitorsInDay.list()
			def visitorsInDay= VisitorsInDay.createCriteria().list() {
				projections {
					property ('codeBuilding')
					property ('nbVisitor')
				}
			}
			visitorsInDayData=visitorsInDay
	//		//println "VisitorsInDay Data: ${visitorsInDayData}"
			visitorsInDayTotal=visitorsInDayList.sum { it.nbVisitor }
	//		//println "VisitorsInDay Total: ${visitorsInDayTotal}"
			
			/*VISITEURS PAR BUILDIND*/
			visitorsByBuildingList=VisitorsByBuilding.list()
			def visitorsByBuilding= VisitorsByBuilding.createCriteria().list() {
				projections {
					property ('codeBuilding')
					property ('nbVisitor')
				}
			}
			visitorsByBuildingData=visitorsByBuilding
	//		//println "VisitorsByBuilding Data: ${visitorsByBuildingData}"
			visitorsByBuildingTotal=visitorsByBuildingList.sum { it.nbVisitor }
	//		//println "VisitorsByBuilding Total: ${visitorsByBuildingTotal}"
		}
		////println "CodeVisitor Number: ${codeVisitorNumber}"
		
//		def visitorsPerPeriodList=VisitorsPerPeriod.list()
//		//println "VisitorsPerPeriod: ${visitorsPerPeriodList}"
		
		[visitorsByBuildingList:visitorsByBuildingList, visitorsByBuildingData:visitorsByBuildingData, visitorsByBuildingTotal:visitorsByBuildingTotal,
			visitorsInDayList:visitorsInDayList, visitorsInDayData:visitorsInDayData, visitorsInDayTotal:visitorsInDayTotal,
			currentYear:currentYear, visitorsPerPeriodData:visitorsPerPeriodData, visitorsPerPeriodTotal:visitorsPerPeriodTotal,
			day:day, monthYear:params.monthYearSelected, codeVisitorNumber:codeVisitorNumber, codePeriod:codePeriod]
	}

    def list(Integer max, Integer offset, String sort, String order) {
		//println "VisiteForm | List"
		
		params.max = Math.min(max ?: 10, 100)
		params.offset = offset ?: 0  // require offset for pagination
		params.sort = sort ?: 'id'
		params.order = order ?: 'desc'
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staffPerno=staffService.currentStaff(session.user)?.perno
		////println "Staff Perno: ${staffPerno}"
		def staff=Staff?.findByPerno(staffPerno)
		////println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visitType
		if (params.visitType){
			visitType=VisitType.findByCode(params.visitType)
		}else{
			visitType=VisitType.findByCode(VisitType.INDIVIDUAL)
		}
		////println "VisitType: ${visitType}"
		
		def date30
		use (TimeCategory) {
			date30 = new Date() - 30.day
		}
		
		//def visiteFormInstanceList=VisiteForm?.findAllByVisitTypeAndAskByDate(visitType, staff, params)
		def visiteFormInstanceList = VisiteForm.findAll("from VisiteForm where visitType=:visit and (receiverVisit=:staff or askBy=:staff) and expectedArrivingDateTime>=:date order by expectedArrivingDateTime desc", [visit: visitType, staff: staff, date: date30], params)
		////println "VisiteForm List: ${visiteFormInstanceList}"
		
		//def visiteFormInstanceCount=VisiteForm.countByVisitTypeAndAskBy(visitType, staff)
		def visiteFormInstanceCount=VisiteForm.findAll("from VisiteForm where visitType=:visit and (receiverVisit=:staff or askBy=:staff) and expectedArrivingDateTime>=:date", [visit: visitType, staff: staff, date: date30]).size()
		////println "VisiteForm Count: ${visiteFormInstanceCount}"
		
        [visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount, codeVisitType:visitType?.code]
    }
	
	def listRequestPersonal(){
		////println "VisiteForm | List - Personal"
		
		def noSecuGard= !(session.user?.role == "guard")
		////println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		////println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
		def visiteFormInstanceList=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.INDIVIDUAL)
		}.list(params)
		
		def visiteFormInstanceCount=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.INDIVIDUAL)
		}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listRequestFamily(){
		////println "VisiteForm | List - Family"
		
		def noSecuGard= !(session.user?.role == "guard")
		////println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
//		def visiteFormInstanceList=VisiteForm.where{askBy?.perno == staff?.perno}.list(params)
//		def visiteFormInstanceCount=VisiteForm.where{askBy?.perno == staff?.perno}.count()
		def visiteFormInstanceList=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.FAMILY)
		}.list(params)
		
		def visiteFormInstanceCount=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.FAMILY)
		}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listRequestBusiness(){
		//println "VisiteForm | List - Business"
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
//		def visiteFormInstanceList=VisiteForm.where{askBy?.perno == staff?.perno}.list(params)
//		def visiteFormInstanceCount=VisiteForm.where{askBy?.perno == staff?.perno}.count()
		def visiteFormInstanceList=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.BUSINESS)
		}.list(params)
		
		def visiteFormInstanceCount=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.BUSINESS)
		}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listRequestEvent(){
		//println "VisiteForm | List - Event"
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
//		def visiteFormInstanceList=VisiteForm.where{askBy?.perno == staff?.perno}.list(params)
//		def visiteFormInstanceCount=VisiteForm.where{askBy?.perno == staff?.perno}.count()
		def visiteFormInstanceList=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.EVENT)
		}.list(params)
		
		def visiteFormInstanceCount=VisiteForm.where{
			(askBy?.perno == staff?.perno)&&
			(visitType?.code == VisitType.EVENT)
		}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listInProgress() {
		//println "VisiteForm | List InProgress"
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
		def visiteFormInstanceList=VisiteForm.where{(askBy?.perno == staff?.perno) && (status?.code in [VisitStatus.INPROGRESS, VisitStatus.SUBMITTED])}.list(params)
		def visiteFormInstanceCount=VisiteForm.where{(askBy?.perno == staff?.perno) && (status?.code in [VisitStatus.INPROGRESS, VisitStatus.SUBMITTED])}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listProcessed() {
		//println "VisiteForm | List Processed"
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(controller:"visitor", action:"listGate")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		
		def visiteFormInstanceList=VisiteForm.where{(askBy?.perno == staff?.perno) && (status?.code in [VisitStatus.APPROVED, VisitStatus.DECLINED, VisitStatus.CANCELED])}.list(params)
		def visiteFormInstanceCount=VisiteForm.where{(askBy?.perno == staff?.perno) && (status?.code in [VisitStatus.APPROVED, VisitStatus.DECLINED, VisitStatus.CANCELED])}.count()
		
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount]
	}
	
	def listManagement(Integer max, Integer offset, String sort, String order) {
//		//println "VisiteForm | ListManagement"
		
//		//println "Params: ${params}"
		
		def status
		if (params?.status){
			status=VisitStatus.findByCode(params?.status)
		}else{
			status=VisitStatus.findByCode(VisitStatus.SUBMITTED)
		}
//		//println "VisitStatus: ${status}"
		
		def sOrder
		if (status.code == VisitStatus.SUBMITTED){
			sOrder='asc'
		}else{
			sOrder='desc'
		}
	
		params.max = Math.min(max ?: 10, 100)
		params.offset = offset ?: 0  // require offset for pagination
		params.sort = sort ?: 'id'
		params.order = order ?: sOrder
		
		def adminSecuApprover=session.user?.role == "admin"
		//println "Admin SecuApprover: ${adminSecuApprover}"
		if (!adminSecuApprover){
			redirect(action:"list")
			return
		}
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
//		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def visiteFormInstanceList=VisiteForm.where{
													status.code == status.code &&
													location == location
												}.list(params)
		def visiteFormInstanceCount=VisiteForm.where{
													status.code == status.code &&
													location == location
												}.count()
		[visiteFormInstanceList: visiteFormInstanceList, visiteFormInstanceTotal: visiteFormInstanceCount,
			status:status]
	}
	
	def listPersonal(){//(Integer max, Integer offset, String sort, String order) {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def visiteFormInstanceListPersonal=VisiteForm.where{visitType.code == VisitType.INDIVIDUAL}.list(params)
		def visiteFormInstanceCountPersonal=VisiteForm.where{visitType.code == VisitType.INDIVIDUAL}.count()
		[visiteFormInstanceList: visiteFormInstanceListPersonal, visiteFormInstanceTotal: visiteFormInstanceCountPersonal]
	}
	
	def listBusiness(){//(Integer max, Integer offset, String sort, String order) {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def visiteFormInstanceListBusiness=VisiteForm.where{visitType.code == VisitType.BUSINESS}.list(params)
		def visiteFormInstanceCountBusiness=VisiteForm.where{visitType.code == VisitType.BUSINESS}.count()
		[visiteFormInstanceList: visiteFormInstanceListBusiness, visiteFormInstanceTotal: visiteFormInstanceCountBusiness]
	}
	
	def _list(){//(Integer max, Integer offset, String sort, String order) {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def visiteFormInstanceListEvent=VisiteForm.where{visitType.code == VisitType.EVENT}.list(params)
		def visiteFormInstanceCountEvent=VisiteForm.where{visitType.code == VisitType.EVENT}.count()
		[visiteFormInstanceList:visiteFormInstanceListEvent, visiteFormInstanceTotal: visiteFormInstanceCountEvent]
	}

    def create() {
		//println "VisiteForm | Create"
		
		def noSecuGard= !(session.user?.role == "guard")
		//println "No SecuGard: ${noSecuGard}"
		if (!noSecuGard){
			redirect(action:"list")
			return
		}
		
		def staffInSession=staffService.currentStaff(session.user)
		def staff=Staff.findByPerno(staffInSession?.perno)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.staff.notFound.message', default: 'Please contact PSEC')
			redirect(action:"list")
			return
		}
		
		if (!staff?.extension){
			flash.error = message(code: 'default.staff.extension.notFound.message', default: 'Please enter your extension')
			redirect(controller:"staff", action:"addExtension")
			return
		}
		
		def visiteFormInstance= new VisiteForm(params)
		visiteFormInstance.askBy=staff
//		visiteFormInstance.receiverVisit=staff
		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.INPROGRESS)
		
		def visitorInstance = new Visitor(params)
		
		model:[visiteFormInstance:visiteFormInstance, visitorInstance:visitorInstance]
    }

    def save() {
        //println "VisiteForm | Save"
		
		//println "Params: ${params}"
		
		def staffInSession=staffService.currentStaff(session.user)
		def staff=Staff.findByPerno(staffInSession?.perno)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.staff.notFound.message', default: 'Please contact PSEC')
			redirect(action:"list")
			return
		}
		
		def visiteFormInstance = new VisiteForm()
		bindData(visiteFormInstance, params, [exclude:['expectedArrivingDateTime_date', 'expectedArrivingDateTime_time', 'expectedDepartureDateTime_date', 'expectedDepartureDateTime_time']])
		
		def visitor = new Visitor(params)
		
		def expectedArrivingDateTime_date=dTF?.parse("${params?.expectedArrivingDateTime_date} ${params?.expectedArrivingDateTime_time}")
		//println "ExpectedArrivingDateTime Date: ${expectedArrivingDateTime_date}"
		def expectedArrivingDateTime_time=(dTF?.parse("${params?.expectedArrivingDateTime_date} ${params?.expectedArrivingDateTime_time}"))?.format("HH:mm")
		//println "ExpectedArrivingDateTime Time: ${expectedArrivingDateTime_time}"
		
		def expectedDepartureDateTime_date=dTF?.parse("${params?.expectedDepartureDateTime_date} ${params?.expectedDepartureDateTime_time}")
		//println "ExpectedDepartureDateTime Date: ${expectedDepartureDateTime_date}"
		def expectedDepartureDateTime_time=(dTF?.parse("${params?.expectedDepartureDateTime_date} ${params?.expectedDepartureDateTime_time}"))?.format("HH:mm")
		//println "ExpectedDepartureDateTime Time: ${expectedDepartureDateTime_time}"
		
		def staffWithdraw
		def iDPersonWithdrawX = params?.receiver
		//println "iDPersonWithdrawX: ${iDPersonWithdrawX}"
		if (iDPersonWithdrawX && (iDPersonWithdrawX?.indexOf('-')==-1)){
			//println "NO PERNO"
			flash.error= message(code: 'visiteForm.receiverVisit.info.message', default:"Please enter the name of the receiver of the visit, if different from the applicant, and click on it when it is displayed on the screen")
			render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
			return
		}else if(iDPersonWithdrawX && iDPersonWithdrawX!=""){
			def staffWithdrawPerno = iDPersonWithdrawX.substring(0, iDPersonWithdrawX?.indexOf('-'))
			//println "staffWithdrawPerno: ${staffWithdrawPerno}"
			staffWithdraw = Staff?.findByPerno(staffWithdrawPerno)
			//println "staffWithdraw: ${staffWithdraw}"
		}else{
			staffWithdraw = Staff?.findByPerno(staff?.perno)
		}
		
		//println "VisitType: ${visiteFormInstance?.visitType}"
		if(!visiteFormInstance?.visitType){
			flash.error= message(code: 'visitForm.visitType.errorCase.message', default:"Please, Select a Type of Visit")
			render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
			return
		}
		
		//println "PurposeOfVisite: ${visiteFormInstance?.purposeOfVisite}"
		if(!visiteFormInstance?.purposeOfVisite){
			flash.error= message(code: 'visitForm.purposeOfVisite.errorCase.message', default:"Please, Enter a Purpose of visit")
			render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
			return
		}
		
		//println "Location: ${visiteFormInstance?.location}"
		if(!visiteFormInstance?.location){
			flash.error= message(code: 'visitForm.location.errorCase.message', default:"Please, Select a Location")
			render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
			return
		}
		
		if (visiteFormInstance.visitType == VisitType.findByCode(VisitType.BUSINESS)){
			//println "Company: ${visiteFormInstance?.company}"
			if(!visiteFormInstance?.company){
				flash.error= message(code: 'visitForm.company.errorCase.message', default:"Please, Enter the Company name")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
		}
		
		if (visiteFormInstance.visitType == VisitType.findByCode(VisitType.EVENT)){
			//println "Event: ${visiteFormInstance?.event}"
			if(!visiteFormInstance?.event){
				flash.error= message(code: 'visitForm.event.errorCase.message', default:"Please, Enter the Event name")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
		}
		
		if (visiteFormInstance.visitType.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]){
			//println "Visit Duration: ${visiteFormInstance?.visitDuration}"
			if(!visiteFormInstance?.visitDuration){
				flash.error= message(code: 'visitForm.visitDuration.errorCase.message', default:"Please, Enter a visit duration")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
		}
		
		def expectedArrivingDateTime= dTF?.parse("${params.expectedArrivingDateTime_date} ${params.expectedArrivingDateTime_time}")
		//println "ExpectedArriving DateTime: ${expectedArrivingDateTime}"
		
		def expectedDepartureDateTime
		if (visiteFormInstance.visitType.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]){
			use(TimeCategory) {
				expectedDepartureDateTime= expectedArrivingDateTime + Integer.parseInt(params.visitDuration).hours
			}
			//println "ExpectedDeparture DateTime: ${expectedDepartureDateTime}"
		}else{
			expectedDepartureDateTime= dTF?.parse("${params.expectedDepartureDateTime_date} ${params.expectedDepartureDateTime_time}")
			//println "ExpectedDeparture DateTime: ${expectedDepartureDateTime}"
		}
		
		visiteFormInstance.receiverVisit=staffWithdraw
		visiteFormInstance.expectedArrivingDateTime=expectedArrivingDateTime
		visiteFormInstance.expectedDepartureDateTime=expectedDepartureDateTime
		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.INPROGRESS)
						
		if (visiteFormInstance.visitType in [VisitType.findByCode(VisitType.INDIVIDUAL), VisitType.findByCode(VisitType.FAMILY)]){
						
			//println "Gender: ${visitor?.gender}"
			if(!visitor?.gender){
				flash.error= message(code: 'visitForm.visitor.gender.errorCase.message', default:"Please, Select the Gender of your visitor")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			//println "First Name: ${visitor?.firstName}"
			if(!visitor?.firstName){
				flash.error= message(code: 'visitForm.visitor.firstName.errorCase.message', default:"Please, Enter the First Name of your visitor")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			//println "Last Name: ${visitor?.lastName}"
			if(!visitor?.lastName){
				flash.error= message(code: 'visitForm.visitor.lastName.errorCase.message', default:"Please, Enter the Last Name of your visitor")
				render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			
//			visitor.status=VisitorStatus.findByCode(VisitorStatus.INITIAL)
			visitor.visitType=visiteFormInstance.visitType
			
			bindData(visitor, params)
			
			visitor=visitor.save(flush: true)
			//println "Visitor: ${visitor}"
			if (visitor.hasErrors()){
				//println visitor.errors
			}
			
		}
		
        if (!visiteFormInstance.save(flush: true)) {
            render(view: "create", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
            return
        }
		
		if (visiteFormInstance.visitType in [VisitType.findByCode(VisitType.INDIVIDUAL), VisitType.findByCode(VisitType.FAMILY)]){
			//println "VisiteForm: ${visiteFormInstance}"
			//println "Visitor: ${visitor}"
			
			visiteFormInstance.addToVisitors(visitor)
							  .save()
		}
		
		flash.message = message(code: 'default.created.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), visiteFormInstance.id])
        redirect(action: "show", id: visiteFormInstance.id)
    }
	
	def addVisitor() {
		//println "VisiteForm | Add Visitor"
		
		def visiteFormInstance = VisiteForm.get(params.visit.id)
		//println "VisiteForm: ${visiteFormInstance}"
		
		def visitorInstance = new Visitor(params)
		
		//println "Gender: ${visitorInstance?.gender}"
		if(!visitorInstance?.gender){
			flash.error= message(code: 'visitForm.visitor.gender.errorCase.message', default:"Please, Select the Gender of your visitor")
			 redirect(action: "show", id: visiteFormInstance.id)
			return
		}
		//println "First Name: ${visitorInstance?.firstName}"
		if(!visitorInstance?.firstName){
			flash.error= message(code: 'visitForm.visitor.firstName.errorCase.message', default:"Please, Enter the First Name of your visitor")
			redirect(action: "show", id: visiteFormInstance.id)
			return
		}
		//println "Last Name: ${visitorInstance?.lastName}"
		if(!visitorInstance?.lastName){
			flash.error= message(code: 'visitForm.visitor.lastName.errorCase.message', default:"Please, Enter the Last Name of your visitor")
			 redirect(action: "show", id: visiteFormInstance.id)
			return
		}
		
//		visitorInstance.status=VisitorStatus.findByCode(VisitorStatus.INITIAL)
		visitorInstance.visitType=visiteFormInstance.visitType
		if (!visitorInstance.save(flush: true)) {
			 redirect(action: "show", id: visiteFormInstance.id)
			return
		}
		
		visiteFormInstance?.visitors.add(visitorInstance)
				
		flash.message = message(code: 'default.created.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), visiteFormInstance.id])
        redirect(action: "show", id: visiteFormInstance.id)
	}
	
	def removVisitor(){
		//println "VisiteForm | Remove Visitor"
		
		def visiteFormInstance=VisiteForm.get(params?.visitId)

		def visitor=Visitor.get(params?.visitorId)
		//println "Visitor: ${visitor}"
		
		visiteFormInstance.removeFromVisitors(visitor)
		visitor.delete(flush: true)
		
		redirect(action: "show", id: visiteFormInstance.id)
	}
	
	def submit4Approval (){
		//println "VisiteForm | Submit4Approval"
		
		def visiteFormInstance = VisiteForm.get(params.visit.id)
//		//println "VisiteForm: ${visiteFormInstance}"
		
		def visitorInstanceList
		if (visiteFormInstance.visitType in [VisitType.findByCode(VisitType.BUSINESS), VisitType.findByCode(VisitType.EVENT)]){
			visitorInstanceList = Visitor.findAllByVisit(visiteFormInstance)
			//println "Visitor: ${visitorInstanceList}"			
			if(!visitorInstanceList){
				flash.error= message(code: 'visitForm.submit4Approval.visitor.errorCase.message', default:"Please, Select Gender, Enter the First Name and Last Name of your visitor")
				redirect (controller:"visiteForm", action:"show", id:visiteFormInstance.id)
				return
			}		
		}

		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.SUBMITTED)
				
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.submit4Approval(visiteFormInstance)
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try {
			visiteFormInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.submitted.message', default:'Submitted', args: [message(code: 'visiteForm.title', default: 'Visit Form'), visiteFormInstance.id])
		redirect (controller:"visiteForm", action:"show", id:visiteFormInstance.id)
	}
	
	def canceled (){
		//println "VisiteForm | Canceled"
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visiteFormInstance = VisiteForm.get(params.visit.id)
		//println "VisiteForm: ${visiteFormInstance}"
		
		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.CANCELED)
		visiteFormInstance?.canceledBy=staff
		/*
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.submit4Approval(askBy, receiverVisit)
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
		try {
			visiteFormInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.canceled.message', default:'Canceled', args: [message(code: 'visiteForm.title', default: 'Visit Form'), visiteFormInstance.id])
		redirect (controller:"visiteForm", action:"show", id:visiteFormInstance.id)
	}
	
	def approved (){
		//println "VisiteForm | Approved"
		
		def adminSecuApprover=session.user?.role == "admin"
		//println "Admin SecuApprover: ${adminSecuApprover}"
		if (!adminSecuApprover){
			redirect(action:"listManagement")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"listManagement")
			return
		}
		
		def visiteFormInstance = VisiteForm.get(params.visit.id)
		//println "VisiteForm: ${visiteFormInstance}"
		
		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.APPROVED)
		visiteFormInstance?.approvedBy=staff
		
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.approvedVisit(askBy, receiverVisit, staff)
		}catch(Exception e){
			e.printStackTrace();
		}

		try {
			visiteFormInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		/*
		visiteFormInstance.visitors.each {
			//println "Visitor: ${it.firstName} - ${it.lastName}"
			it.status=VisitorStatus.findByCode(VisitorStatus.AWAITING)
			try {
				it.save(flush:true)
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				e.printStackTrace()
			}
		}
		*/
		flash.message = message(code: 'default.approved.message', default:'Approved', args: [message(code: 'visiteForm.title', default: 'Visit Form'), visiteFormInstance.id])
		redirect(controller:"visiteForm", action: "showManagement", id: visiteFormInstance.id)
	}
	
	def declined (){
		//println "VisiteForm | Declined"
		
		def adminSecuApprover=session.user?.role == "admin"
		//println "Admin SecuApprover: ${adminSecuApprover}"
		if (!adminSecuApprover){
			redirect(action:"listManagement")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"listManagement")
			return
		}
		
		def visiteFormInstance = VisiteForm.get(params.visit.id)
		//println "VisiteForm: ${visiteFormInstance}"
		
		visiteFormInstance.status=VisitStatus.findByCode(VisitStatus.DECLINED)
		visiteFormInstance?.declinedBy=staff

		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.declinedVisit(askBy, receiverVisit, staff)
		}catch(Exception e){
			e.printStackTrace();
		}

		try {
			visiteFormInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.declined.message', default:'Declined', args: [message(code: 'visiteForm.title', default: 'Visit Form'), visiteFormInstance.id])
		redirect(controller:"visiteForm", action: "showManagement", id: visiteFormInstance.id)
	}

    def show() {
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
        def visiteFormInstance = VisiteForm.get(params.id)
        if (!visiteFormInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "list")
            return
        }
		
		def visitorInstance = new Visitor(params)
		
        [visiteFormInstance: visiteFormInstance, visitorInstance: visitorInstance]
    }
	
	def showManagement() {
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visiteFormInstance = VisiteForm.get(params.id)
		if (!visiteFormInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
			redirect(action: "list")
			return
		}

		[visiteFormInstance: visiteFormInstance]
	}

    def edit() {
		//println "VisiteForm | Edit"
		
        def visiteFormInstance = VisiteForm.get(params.id)
        if (!visiteFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "list")
            return
        }
		
		if (!(visiteFormInstance?.status?.code in [VisitStatus.INPROGRESS, VisitStatus.SUBMITTED])){
			flash.message = message(code: 'default.form.submit.message', default:'The form was submitted')
			redirect(action: "show", id: params.id)
			return
		}
		
		def expectedArrivingDateTime_date=visiteFormInstance?.expectedArrivingDateTime//.format("yyyy-MM-dd")
		def expectedArrivingDateTime_time=visiteFormInstance?.expectedArrivingDateTime.format("HH:mm")
		
		def expectedDepartureDateTime_date=visiteFormInstance?.expectedDepartureDateTime//.format("yyyy-MM-dd")
		def expectedDepartureDateTime_time=visiteFormInstance?.expectedDepartureDateTime.format("HH:mm")
		
		if (visiteFormInstance.visitType == VisitType.findByCode(VisitType.INDIVIDUAL)){
	        def visitorId = visiteFormInstance?.visitors?.first()?.id
			def visitorInstance=Visitor.get(visitorId)
			
			model:[visiteFormInstance:visiteFormInstance, visitorInstance:visitorInstance,
				expectedArrivingDateTime_date:expectedArrivingDateTime_date, expectedArrivingDateTime_time:expectedArrivingDateTime_time,
				expectedDepartureDateTime_date:expectedDepartureDateTime_date, expectedDepartureDateTime_time:expectedDepartureDateTime_time]
		}else{
			def visitorInstance = new Visitor(params)
			model:[visiteFormInstance:visiteFormInstance, visitorInstance:visitorInstance,
				expectedArrivingDateTime_date:expectedArrivingDateTime_date, expectedArrivingDateTime_time:expectedArrivingDateTime_time,
				expectedDepartureDateTime_date:expectedDepartureDateTime_date, expectedDepartureDateTime_time:expectedDepartureDateTime_time]
		}
    }

    def update() {
		//println "VisiteForm | Update"
		
        def visiteFormInstance = VisiteForm.get(params.id)
		//println "VisiteFormInstance: ${visiteFormInstance}"
        if (!visiteFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "list")
            return
        }

        def expectedArrivingDateTime_date=visiteFormInstance?.expectedArrivingDateTime
		//println "ExpectedArrivingDateTime Date: ${expectedArrivingDateTime_date}"
		def expectedArrivingDateTime_time=visiteFormInstance?.expectedArrivingDateTime?.format("HH:mm")
		//println "ExpectedArrivingDateTime Time: ${expectedArrivingDateTime_time}"
		
		def expectedDepartureDateTime_date=visiteFormInstance?.expectedDepartureDateTime
		//println "ExpectedDepartureDateTime Date: ${expectedDepartureDateTime_date}"
		def expectedDepartureDateTime_time=visiteFormInstance?.expectedDepartureDateTime?.format("HH:mm")
		//println "ExpectedDepartureDateTime Time: ${expectedDepartureDateTime_time}"

        if (params.version) {
            def version = params.version.toLong()
            if (visiteFormInstance.version > version) {
                visiteFormInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'visiteForm.label', default: 'VisiteForm')] as Object[],
                          "Another user has updated this VisiteForm while you were editing")
                render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
                return
            }
        }
		
		def staffWithdraw
		def iDPersonWithdrawX = params?.receiver
		//println "iDPersonWithdrawX: ${iDPersonWithdrawX}"
		if (iDPersonWithdrawX && (iDPersonWithdrawX?.indexOf('-')==-1)){
			//println "NO PERNO"
			flash.error= message(code: 'visiteForm.receiverVisit.info.message', default:"Please enter the name of the receiver of the visit, if different from the applicant, and click on it when it is displayed on the screen")
			render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
			return
		}else if(iDPersonWithdrawX && iDPersonWithdrawX!=""){
			def staffWithdrawPerno = iDPersonWithdrawX.substring(0, iDPersonWithdrawX?.indexOf('-'))
			//println "staffWithdrawPerno: ${staffWithdrawPerno}"
			staffWithdraw = Staff?.findByPerno(staffWithdrawPerno)
			//println "staffWithdraw: ${staffWithdraw}"
			visiteFormInstance?.receiverVisit=staffWithdraw
		}
		//println "receiverVisit: ${visiteFormInstance?.receiverVisit}"
		
		if (visiteFormInstance.visitType == VisitType.findByCode(VisitType.INDIVIDUAL)){
			def visitorId = visiteFormInstance?.visitors?.first().id
			def visitor=Visitor.get(visitorId)
			
			visiteFormInstance?.expectedDepartureDateTime=visiteFormInstance?.expectedArrivingDateTime
			
			//println "Visit Duration: ${params?.visitDuration}"
			if(!params?.visitDuration){
				flash.error= message(code: 'visitForm.visitDuration.errorCase.message', default:"Please, Enter a visit duration")
				render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			
			//println "Gender: ${params?.gender}"
			if(!params?.gender){
				flash.error= message(code: 'visitForm.visitor.gender.errorCase.message', default:"Please, Select the Gender of your visitor")
				render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			//println "First Name: ${params?.firstName}"
			if(!params?.firstName){
				flash.error= message(code: 'visitForm.visitor.firstName.errorCase.message', default:"Please, Enter the First Name of your visitor")
				render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
			//println "Last Name: ${params?.lastName}"
			if(!params?.lastName){
				flash.error= message(code: 'visitForm.visitor.lastName.errorCase.message', default:"Please, Enter the Last Name of your visitor")
				render(view: "edit", model: [visiteFormInstance: visiteFormInstance, visitorInstance: visitor,
											expectedArrivingDateTime_date:expectedArrivingDateTime_date,
											expectedArrivingDateTime_time:expectedArrivingDateTime_time,
											expectedDepartureDateTime_date:expectedDepartureDateTime_date,
											expectedDepartureDateTime_time:expectedDepartureDateTime_time])
				return
			}
					
			bindData(visitor, params)
						
			visitor=visitor.save(flush: true)
			//println "Visitor: ${visitor}"
			if (visitor.hasErrors()){
				//println visitor.errors
			}
			
			//println "VisiteForm: ${visiteFormInstance}"
			//println "Visitor: ${visitor}"
			
			visiteFormInstance.addToVisitors(visitor)
							  .save()
			
		}
		
		def visitType=visiteFormInstance.visitType
		//println "VisitType: ${visitType}"
		
		def expectedArrivingDateTime= dTF?.parse("${params.expectedArrivingDateTime_date} ${params.expectedArrivingDateTime_time}")
		//println "ExpectedArriving DateTime: ${expectedArrivingDateTime}"
		
		def expectedDepartureDateTime
		if (visitType.code in [VisitType.INDIVIDUAL, VisitType.FAMILY]){
			use(TimeCategory) {
				expectedDepartureDateTime= expectedArrivingDateTime + Integer.parseInt(params.visitDuration).hours
			}
			//println "ExpectedDeparture DateTime: ${expectedDepartureDateTime}"
		}else{
			expectedDepartureDateTime= dTF?.parse("${params.expectedDepartureDateTime_date} ${params.expectedDepartureDateTime_time}")
			//println "ExpectedDeparture DateTime: ${expectedDepartureDateTime}"
		}
		
		bindData(visiteFormInstance, params, [exclude:['expectedArrivingDateTime_date', 'expectedArrivingDateTime_time', 'expectedDepartureDateTime_date', 'expectedDepartureDateTime_time']])
//		visiteFormInstance.properties = params
		visiteFormInstance.expectedArrivingDateTime=expectedArrivingDateTime
		visiteFormInstance.expectedDepartureDateTime=expectedDepartureDateTime
		
        if (!visiteFormInstance.save(flush: true)) {
            render(view: "edit", model: [visiteFormInstance: visiteFormInstance])
            return
        }
		
		flash.message = message(code: 'default.updated.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), visiteFormInstance.id])
        redirect(action: "show", id: visiteFormInstance.id)
    }

    def delete() {
		//println "VisiteForm | Delete"
		
        def visiteFormInstance = VisiteForm.get(params.visit.id)
		//println "VisiteFormInstance: ${visiteFormInstance}"
        
		if (!visiteFormInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "list")
            return
        }
		
		if (visiteFormInstance.visitType == VisitType.findByCode(VisitType.INDIVIDUAL)){
			def visitorId = visiteFormInstance?.visitors?.first().id
			def visitor=Visitor.get(visitorId)
			//println "Visitor: ${visitor}"
			
			try {
				visiteFormInstance.removeFromVisitors(visitor)
				visitor.delete(flush: true)
			}catch (DataIntegrityViolationException e) {
				flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
	            redirect(action: "show", id: params.id)
	        }
		}else{
			visiteFormInstance?.visitors.eachWithIndex {visitor, v ->
				//println "Visitor-${v}: ${visitor}"
				try {
					visiteFormInstance.removeFromVisitors(visitor)
					visitor.delete(flush: true)
				}catch (DataIntegrityViolationException e) {
					flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
					redirect(action: "show", id: params.id)
				}
			}
		}
		
        try {
            visiteFormInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'visiteForm.label', default: 'VisiteForm'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
