package org.afdb.rs

import org.afdb.utils.SecureController;

/**
 * VisitTypeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class VisitTypeController extends SecureController{

	static scaffold = true
//	def index = { }
}
