package org.afdb.rs

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.afdb.utils.SecureController;
import org.springframework.dao.DataIntegrityViolationException

/**
 * VisitorController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class VisitorController extends SecureController{

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def staffService
	def mailingService
	
	SimpleDateFormat dF=new SimpleDateFormat("yyyy-MM-dd")
	def currentDay = new Date()
	
	def searchDesk={
		def q=params?.query="singbo"

		def location=Location.findByCode(session.location)
//		////println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		def currentDayDate=dF.format(currentDay)
		Date expectedCurrentDayDate= dF.parse(currentDayDate)
		//println "CurrentDayDate: ${expectedCurrentDayDate}"
				
		def visitorEntryList=[]
		def visitorEntryInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))",[code: VisitorStatus.ENTRANCE, locationId: location.id])		
		//println "VisitorEntry List: ${visitorEntryInstanceList}"
		def visitorEntryCount=visitorEntryInstanceList.size()
		//println "VisitorEntry Count: ${visitorEntryCount}"
//		//println "CurrentDay Format: ${currentDay.format("yyyy-MM-dd")}"

		visitorEntryInstanceList.eachWithIndex {entry, v ->

			def vEntryDateCreated=dF.format(entry?.dateCreated)
			Date entryDateCreatedDate= dF.parse(vEntryDateCreated)
//			//println "EntryDateCreatedDate: ${entryDateCreatedDate}"
			if (entryDateCreatedDate.compareTo(expectedCurrentDayDate) ==0){
				  visitorEntryList.add(entry)
			}
		}
		//println "VisitorEntry List: ${visitorEntryList}"
		def visitorEntryInstanceCount=visitorEntryList.size()
		//println "VisitorEntry Count: ${visitorEntryInstanceCount}"
		
		def visitorDeskInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where (firstName LIKE :queryf or lastName LIKE :queryl) and visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))))",[code: VisitorStatus.CHECKIN, locationId: location.id, queryf:"%${params?.query}%", queryl:"%${params?.query}%"], params)
		//println "VisitorDesk List: ${visitorDeskInstanceList}"
		
		def resultsStaff = Visitor.findAllByFirstNameIlikeOrLastNameIlike("%${params?.query}%","%${params?.query}%")
		
//		def visitorEntryInstanceList=VisitorEntry.where{(status?.code == VisitorStatus.CHECKIN)}.list()//(params)//(sort: 'name', order: 'asc')
//		def visitorEntryInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where (firstName LIKE :queryf or lastName LIKE :queryl) and visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))))",[code: VisitorStatus.CHECKIN, locationId: location.id, queryf:"%${params?.query}%", queryl:"%${params?.query}%"], params)
//		//println "VisitorEntry List: ${visitorEntryInstanceList}"
//		def visitorEntryInstanceCount=VisitorEntry.where{(status?.code == VisitorStatus.CHECKIN)}.count()
//		def visitorEntryInstanceCount=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where (firstName LIKE :queryf or lastName LIKE :queryl) and visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))))",[code: VisitorStatus.CHECKIN, locationId: location.id, queryf:"%${params?.query}%", queryl:"%${params?.query}%"]).size()
//		//println "VisitorEntry Count: ${visitorEntryInstanceCount}"
			
//		flash.message="${visitorEntryInstanceCount} results found"
		
		render (view:'listCheckout', model:[visitorEntryInstanceList:visitorEntryInstanceList, visitorEntryInstanceCount: visitorEntryInstanceCount])
	}
	
	def searchCheckout={
		def q=params?.query

		def location=Location.findByCode(session.location)
//		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		def visitorEntryInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where (firstName LIKE :queryf or lastName LIKE :queryl) and visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))))",[code: VisitorStatus.CHECKIN, locationId: location.id, queryf:"%${params?.query}%", queryl:"%${params?.query}%"], params)
//		//println "VisitorEntry List: ${visitorEntryInstanceList}"
		def visitorEntryInstanceCount=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where (firstName LIKE :queryf or lastName LIKE :queryl) and visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))))",[code: VisitorStatus.CHECKIN, locationId: location.id, queryf:"%${params?.query}%", queryl:"%${params?.query}%"]).size()
//		//println "VisitorEntry Count: ${visitorEntryInstanceCount}"
			
		flash.message="${visitorEntryInstanceCount} results found"
		
		render (view:'listCheckout', model:[visitorEntryInstanceList:visitorEntryInstanceList, visitorEntryInstanceCount: visitorEntryInstanceCount])
	}
	
    def index() {
        redirect(action: "list", params: params)
    }
	
	def listOptionTypeOfCard(){
		[Visitor.PC, Visitor.CNI, Visitor.PASSPORT, Visitor.LAISSEZPASSER, Visitor.CARTECONSULAIRE, Visitor.OTHER]
	}
	
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [visitorInstanceList: Visitor.list(params), visitorInstanceTotal: Visitor.count()]
    }
	
	def listGate() {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		redirect(action: "listGatePersonal", params: params)
	}
	
	def listGatePersonal(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 5, 100)
		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}

		
		def visitorList=[]
		//println "CurrentDay: ${currentDay}"
		def SimpleDateFormat df=new SimpleDateFormat()
		def visitorInstanceList=Visitor.where{(visitType?.code == VisitType.INDIVIDUAL) && 
											(visit?.status == VisitStatus.findByCode(VisitStatus.APPROVED)) &&
											(visit?.location == location)
											
										}.list()//(params)//(sort: 'name', order: 'asc')
		
		visitorInstanceList.eachWithIndex {visitor, v ->
			if (
					(
						(visitor?.checkinGateDate==null) || 
						(visitor?.checkinGateDate?.format("yyyy-MM-dd") != currentDay.format("yyyy-MM-dd")) 
					)&& 
					(visitor?.visit?.expectedArrivingDateTime?.format("yyyy-MM-dd") == currentDay.format("yyyy-MM-dd"))
				)
			{
				  visitorList.add(visitor)
			}
			
		}
		//println "Visitor List: ${visitorList}"
		
		def visitorInstanceCount=visitorInstanceList.size()
		//println "VisitorInstance Count: ${visitorInstanceCount}"
		
		[visitorInstanceList: visitorList, visitorInstanceTotal: visitorInstanceCount]
	}
	
	def listGateFamily(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 5, 100)
		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		def visitorList=[]
		//println "CurrentDay: ${currentDay}"
		def SimpleDateFormat df=new SimpleDateFormat()
		def visitorInstanceList=Visitor.where{(visitType?.code == VisitType.FAMILY) &&
											(visit?.status == VisitStatus.findByCode(VisitStatus.APPROVED))&&
											(visit?.location == location)
										}.list()//(params)//(sort: 'name', order: 'asc')
		
		visitorInstanceList.eachWithIndex {visitor, v ->
			if (
					(
						(visitor?.checkinGateDate==null) ||
						(visitor?.checkinGateDate?.format("yyyy-MM-dd") != currentDay.format("yyyy-MM-dd"))
					)&&
					(visitor?.visit?.expectedArrivingDateTime?.format("yyyy-MM-dd") == currentDay.format("yyyy-MM-dd"))
				)
			{
				  visitorList.add(visitor)
			}
		}
		//println "Visitor List: ${visitorList}"
		
		def visitorInstanceCount=visitorInstanceList.size()
		//println "VisitorInstance Count: ${visitorInstanceCount}"
		
		[visitorInstanceList: visitorList, visitorInstanceTotal: visitorInstanceCount]
	}
	
	def listGateBusiness(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 5, 100)
		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		def visitorList=[]
		def visitorInstanceList=Visitor.where{(
												(visitType?.code == VisitType.BUSINESS) && 
												(visit?.status == VisitStatus.findByCode(VisitStatus.APPROVED)) &&
												(visit?.location == location)
											)
										}.list()//(params)
		//println "VisitorInstanceList Count: ${visitorInstanceList.size()}"
		
		
		
		visitorInstanceList.eachWithIndex {visitor, v ->
			
			def arrivingDate=dF.format(visitor?.visit?.expectedArrivingDateTime)
			Date expectedArrivingDate= dF.parse(arrivingDate)
//			//println "ArrivingDate: ${expectedArrivingDate}"
			
			def departureDate=dF.format(visitor?.visit?.expectedDepartureDateTime)
			Date expectedDepartureDate= dF.parse(departureDate)
//			//println "DepartureDateTime: ${expectedDepartureDate}"
			
			def currentDayDate=dF.format(currentDay)
			Date expectedCurrentDayDate= dF.parse(currentDayDate)
//			//println "CurrentDayDate: ${expectedCurrentDayDate}"
			
			Date checkinGateDate=null
			
			if (visitor?.checkinGateDate){
				def vCheckinGateDate= dF.format(visitor?.checkinGateDate)
				checkinGateDate= dF.parse(vCheckinGateDate)
			}
	
			if (
					((visitor?.checkinGateDate==null) || (checkinGateDate.compareTo(expectedCurrentDayDate)!=0)) &&
					(
						((expectedArrivingDate.compareTo(expectedCurrentDayDate) < 0) ||(expectedArrivingDate.compareTo(expectedCurrentDayDate) == 0)) &&
						((expectedDepartureDate.compareTo(expectedCurrentDayDate) > 0) || (expectedDepartureDate.compareTo(expectedCurrentDayDate) ==0))
					)					
				)
				{
				visitorList.add(visitor)
			}
			
		}
		//println "Visitor List: ${visitorList}"
		
		def visitorInstanceCount=visitorList.size()
		//println "Visitor Count: ${visitorInstanceCount}"
		
		[visitorInstanceList: visitorList, visitorInstanceTotal: visitorInstanceCount]
	}
	
	def listGateEvent(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 5, 100)
//		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		/**
		 * Recup�ration de la liste des �v�nement
		 */
		def visiteFormEventList=[]
		def visitorList=[]
		def visiteFormEventInstanceList=VisiteForm.where{
											(
												visitType?.code == VisitType.EVENT && 
												status == VisitStatus.findByCode(VisitStatus.APPROVED) &&
												location == location
											)
										}.list()//(params)
		
		//println "VisiteFormEvent List: ${visiteFormEventInstanceList}"
		visiteFormEventInstanceList.eachWithIndex {visit, v ->
			/*				
			if (date1.compareTo(date2) > 0) {
				//println "Date1 is after Date2"
			} else if (date1.compareTo(date2) < 0) {
				//println "Date1 is before Date2"
			} else if (date1.compareTo(date2) == 0) {
				//println "Date1 is equal to Date2"
			} else {
				//println "How to get here?"
			}
			*/
			def arrivingDate=dF.format(visit?.expectedArrivingDateTime)
			Date expectedArrivingDate= dF.parse(arrivingDate)
//			//println "ArrivingDate: ${expectedArrivingDate}"
			
			def departureDate=dF.format(visit?.expectedDepartureDateTime)
			Date expectedDepartureDate= dF.parse(departureDate)
//			//println "DepartureDateTime: ${expectedDepartureDate}"
			
			def currentDayDate=dF.format(currentDay)
			Date expectedCurrentDayDate= dF.parse(currentDayDate)
//			//println "CurrentDayDate: ${expectedCurrentDayDate}"
			
			//On affiche l'Event si la date de debut de l'Event est inferieur ou egale a la date du Jour ET que la date de Fin de l'Event est Superieur ou egale a la date du Jour 
			if (
					((expectedArrivingDate.compareTo(expectedCurrentDayDate) < 0) ||(expectedArrivingDate.compareTo(expectedCurrentDayDate) == 0)) && 
					((expectedDepartureDate.compareTo(expectedCurrentDayDate) > 0) || (expectedDepartureDate.compareTo(expectedCurrentDayDate) ==0))
				) {							

						
				/**
				 * On parcourt la liste des evenement pour verifier s il y a un visiteur attendu.
				 * Si plus aucun visiteur attendu dans l'evenement, on ne r�cup�re pas l'�v�nement.
				 * S il y a un visiteur attendu on r�cup�re l'�v�nement
				 */
				visit.visitors.eachWithIndex {visitor, r ->
					Date checkinGateDate=null
					
					if (visitor?.checkinGateDate){
						def vCheckinGateDate= dF.format(visitor?.checkinGateDate)
						checkinGateDate= dF.parse(vCheckinGateDate)
					}
					
					if ((visitor?.checkinGateDate==null) || (checkinGateDate.compareTo(expectedCurrentDayDate)!=0)){ //(visitor?.checkinGateDate?.format("yyyy-MM-dd") != currentDay.format("yyyy-MM-dd"))){
						visitorList.add(visitor)
					}
				}

				if (visitorList){
					visiteFormEventList.add(visit)
				}
			}
		}
//		//println "VisitFormEvent List: ${visiteFormEventList}"
		
		[visiteFormEventList:visiteFormEventList, currentDay:currentDay]
	}
	
	def listDesk(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuDesk=session.user?.role == "desk"
		//println "Admin SecuDesk: ${adminSecuDesk}"
		if (!adminSecuDesk){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 15, 100)
		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
		def currentDayDate=dF.format(currentDay)
		Date expectedCurrentDayDate= dF.parse(currentDayDate)
		//println "CurrentDayDate: ${expectedCurrentDayDate}"
				
		def visitorEntryList=[]
//		def visitorEntryInstanceList=VisitorEntry.where{
//			def currentVisit=Visitor.get(visitor.id)
//			//println "currentVisitor: ${currentVisit}"
//			//println "VisitorEntry Location: ${visitor?.visit?.location}"
//			(
//				status?.code == VisitorStatus.ENTRANCE &&
////				visitor?.checkinGateDate == new Date() &&
//				visitor?.visit?.location == location
//			)
//		}.list()//(params)//(sort: 'name', order: 'asc')
		def visitorEntryInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))",[code: VisitorStatus.ENTRANCE, locationId: location.id])
		
//		//println "VisitorEntry List: ${visitorEntryInstanceList}"
		def visitorEntryCount=visitorEntryInstanceList.size()
//		//println "VisitorEntry Count: ${visitorEntryCount}"
//		//println "CurrentDay Format: ${currentDay.format("yyyy-MM-dd")}"
		/*
		visitorEntryInstanceList.eachWithIndex {entry, v ->
			def visitor=Visitor.get(entry?.visitor.id)
			//println "Visitor: ${visitor?.firstName}, ${visitor?.lastName}"
			//println "DateCreated: ${entry?.dateCreated.format("yyyy-MM-dd")}"
			//println "Status: ${(entry?.status?.code == VisitorStatus.ENTRANCE)}"
			//println "Entry: ${(entry?.dateCreated.format("yyyy-MM-dd") == currentDay.format("yyyy-MM-dd"))}"
			
		}
		*/
		visitorEntryInstanceList.eachWithIndex {entry, v ->
			
//			//println "VisitorEntry: ${entry?.visitor}"

			def vEntryDateCreated=dF.format(entry?.dateCreated)
			Date entryDateCreatedDate= dF.parse(vEntryDateCreated)
//			//println "EntryDateCreatedDate: ${entryDateCreatedDate}"
			if (entryDateCreatedDate.compareTo(expectedCurrentDayDate) ==0){
				  visitorEntryList.add(entry)
			}
		}
//		//println "VisitorEntry List: ${visitorEntryList}"
		def visitorEntryInstanceCount=visitorEntryList.size()
//		//println "VisitorEntry Count: ${visitorEntryInstanceCount}"
		
		[visitorEntryInstanceList: visitorEntryList, visitorInstanceTotal: visitorEntryInstanceCount]
	}
	
	def listCheckout(Integer max, Integer offset, String sort, String order) {
		
		def adminSecuDesk=session.user?.role == "desk"
		//println "Admin SecuDesk: ${adminSecuDesk}"
		if (!adminSecuDesk){
			redirect(action:"list")
			return
		}
		
		params.max = Math.min(max ?: 15, 100)
		params.offset = offset ?: 0  // require offset for pagination
//		params.sort = sort ?: 'startDate'
//		params.order = order ?: 'desc'
		
		def location=Location.findByCode(session.location)
		//println "Location: ${location}"
		if (!location){
			redirect(controller:"location", action:"selectLocation")
			return
		}
		
//		def visitorEntryInstanceList=VisitorEntry.where{(status?.code == VisitorStatus.CHECKIN)}.list()//(params)//(sort: 'name', order: 'asc')
		def visitorEntryInstanceList=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))",[code: VisitorStatus.CHECKIN, locationId: location.id], params)
		//println "VisitorEntry List: ${visitorEntryInstanceList}"
//		def visitorEntryInstanceCount=VisitorEntry.where{(status?.code == VisitorStatus.CHECKIN)}.count()
		def visitorEntryInstanceCount=VisitorEntry.findAll("from VisitorEntry where status.id in (select id from VisitorStatus where code=:code) and visitor.id in (select id from Visitor where visit.id in (select id from VisiteForm where location.id in (select id from Location where id=:locationId)))",[code: VisitorStatus.CHECKIN, locationId: location.id]).size()
		//println "VisitorEntry Count: ${visitorEntryInstanceCount}"

		[visitorEntryInstanceList: visitorEntryInstanceList, visitorEntryInstanceCount: visitorEntryInstanceCount]
	}
	
	def entrance (){
		//println "Visitor | Entrance"
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		def user=session.user
		//println "User: ${user}"
				
		def visitorInstance = Visitor.get(params.visitor.id)
		//println "Visitor: ${visitorInstance}"
		
		if (visitorInstance?.checkinGateDate.equals(currentDay)){
//		if(visitorInstance.checkinGateDate?.format("yyyy-MM-dd") == currentDay.format("yyyy-MM-dd")){
			flash.error= message(code: 'error.visitor.entrance.message', default:"The visitor was received")
			if (visitorInstance?.visitType?.code == VisitType.INDIVIDUAL){
				redirect(action: "listGatePersonal")
			}else if (visitorInstance?.visitType?.code == VisitType.BUSINESS){
				redirect(action: "listGateBusiness")
			}else if (visitorInstance?.visitType?.code == VisitType.EVENT){
				redirect(action: "listGateEvent")
			}else{
				redirect(action: "listGate")
			}
			return
		}
		
		def entry = new VisitorEntry(params)
		entry.visitor=visitorInstance
		entry.status=VisitorStatus.findByCode(VisitorStatus.ENTRANCE)
		entry.entryPermitBy=user
		entry.save(flush:true)
		
		/*
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.submit4Approval(askBy, receiverVisit)
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
		visitorInstance.checkinGateDate=new Date()
		
		if (visitorInstance?.visitType?.code in [VisitType.INDIVIDUAL, VisitType.BUSINESS, VisitType.FAMILY]){
			try{
				mailingService.entryGate(visitorInstance, entry)
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		try {
			visitorInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.entrance.message', default:'Entrance', args: [message(code: 'visitor.title', default: 'Visitor'), visitorInstance.id])
		if (visitorInstance?.visitType?.code == VisitType.INDIVIDUAL){
			redirect(action: "listGatePersonal")
		}else if (visitorInstance?.visitType?.code == VisitType.BUSINESS){
			redirect(action: "listGateBusiness")
		}else if (visitorInstance?.visitType?.code == VisitType.EVENT){
			redirect(action: "listGateEvent")
		}else{
			redirect(action: "listGate")
		}		
	}
	
	def deniedGate (){
		//println "Visitor | Denied Gate"
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"listGate")
			return
		}
		
		def user=session.user
		//println "User: ${user}"
		
		def visitorInstance = Visitor.get(params.visitor.id)
		//println "Visitor: ${visitorInstance}"
		
		if (visitorInstance?.checkinGateDate.equals(currentDay)){
//		if(visitorInstance.checkinGateDate?.format("yyyy-MM-dd") == currentDay.format("yyyy-MM-dd")){
			flash.error= message(code: 'error.visitor.entrance.message', default:"The visitor was received")
			if (visitorInstance?.visitType?.code == VisitType.INDIVIDUAL){
				redirect(action: "listGatePersonal")
			}else if (visitorInstance?.visitType?.code == VisitType.BUSINESS){
				redirect(action: "listGateBusiness")
			}else if (visitorInstance?.visitType?.code == VisitType.EVENT){
				redirect(action: "listGateEvent")
			}else{
				redirect(action: "listGate")
			}
			return
		}
		
		def entry = new VisitorEntry(params)
		entry.visitor=visitorInstance
		entry.status=VisitorStatus.findByCode(VisitorStatus.DENIED_GATE)
		entry.deniedGateBy=user
		entry.save(flush:true)
		
		visitorInstance.checkinGateDate=new Date()
		
		try{
			mailingService.deniedGate(visitorInstance, entry)
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try {
			visitorInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.repressed.message', default:'Access Denied', args: [message(code: 'visitor.title', default: 'Visitor'), visitorInstance.id])
		if (visitorInstance?.visitType?.code == VisitType.INDIVIDUAL){
			redirect(action: "listGatePersonal")
		}else if (visitorInstance?.visitType?.code == VisitType.BUSINESS){
			redirect(action: "listGateBusiness")
		}else if (visitorInstance?.visitType?.code == VisitType.EVENT){
			redirect(action: "listGateEvent")
		}else{
			redirect(action: "listGate")
		}	
	}
	
	def checkin (){
		//println "Visitor | Checkin"
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visitorEntryInstance = VisitorEntry.get(params.entry.id)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
		def visitor=Visitor.get(visitorEntryInstance?.visitor.id)
		//println "Visitor: ${visitor}"
		
		visitorEntryInstance.status=VisitorStatus.findByCode(VisitorStatus.CHECKIN)
		visitorEntryInstance.checkinBy=staff
		visitorEntryInstance?.checkinDateTime=new Date()
		
		/*
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.submit4Approval(askBy, receiverVisit)
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
		try {
			visitorEntryInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.checkin.message', default:'Checkin', args: [message(code: 'visitor.title', default: 'Visitor'), visitor.id])
//		redirect (controller:"visitor", action:"listReception")
		redirect (action: "edit", id: visitor.id, params:[entryId:visitorEntryInstance.id])
	}
	
	def deniedDesk (){
		//println "Visitor | Denied Desk"
		
		def adminSecuDesk=session.user?.role == "desk"
		//println "Admin SecuDesk: ${adminSecuDesk}"
		if (!adminSecuDesk){
			redirect(action:"list")
			return
		}
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visitorEntryInstance = VisitorEntry.get(params.entry.id)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
		def visitorInstance=Visitor.get(visitorEntryInstance?.visitor.id)
		//println "Visitor: ${visitorInstance}"
		
		visitorEntryInstance.status=VisitorStatus.findByCode(VisitorStatus.DENIED_DESK)
		visitorEntryInstance.deniedReceptionBy=staff
		visitorEntryInstance.properties = params
		visitorEntryInstance.save(flush:true)
				
		try{
			mailingService.deniedReception(visitorInstance, visitorEntryInstance)
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try {
			visitorInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.repressed.message', default:'Access Denied', args: [message(code: 'visitor.title', default: 'Visitor'), visitorInstance.id])
		redirect(action: "listDesk")
	}
	
	def checkout (){
		//println "Visitor | CheckOut"
		
		def staff=staffService.currentStaff(session.user)
		//println "Staff: ${staff}"
		if (!staff){
			flash.error = message(code: 'default.admin.notFound.message', default: 'Admin not found')
			redirect(action:"list")
			return
		}
		
		def visitorEntryInstance = VisitorEntry.get(params.entry.id)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
		def visitorInstance=Visitor.get(visitorEntryInstance?.visitor.id)
		//println "Visitor: ${visitorInstance}"
		
		visitorEntryInstance.status=VisitorStatus.findByCode(VisitorStatus.CHECKOUT)
		visitorEntryInstance.checkoutBy=staff
		visitorEntryInstance?.checkoutDateTime=new Date()

		/*
		def askBy=visiteFormInstance?.askBy
		def receiverVisit=visiteFormInstance?.receiverVisit
		try{
			mailingService.submit4Approval(askBy, receiverVisit)
		}catch(Exception e){
			e.printStackTrace();
		}
		*/
		try {
			visitorEntryInstance.save(flush:true)
		}
		catch (org.springframework.dao.DataIntegrityViolationException e) {
			e.printStackTrace()
		}
		
		flash.message = message(code: 'default.checkout.message', default:'CheckOut', args: [message(code: 'visitor.title', default: 'Visitor'), visitorInstance.id])
		redirect (action: "show", id: visitorInstance.id, params:[entryId:visitorEntryInstance.id])
	}

    def create() {
        [visitorInstance: new Visitor(params), listOptionTypeOfCard:listOptionTypeOfCard()]
    }

    def save() {
        def visitorInstance = new Visitor(params)
        if (!visitorInstance.save(flush: true)) {
            render(view: "create", model: [visitorInstance: visitorInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'visitor.label', default: 'Visitor'), visitorInstance.id])
        redirect(action: "show", id: visitorInstance.id)
    }

    def show() {
        def visitorInstance = Visitor.get(params.id)
		
		def visitorEntryInstance = VisitorEntry.get(params?.entryId)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
        if (!visitorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "list")
            return
        }
		
		if (visitorInstance?.id != visitorEntryInstance?.visitor?.id) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
			redirect(action: "list")
			return
		}

        [visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance]
    }
	
	def showManagement() {
		//println "Visitor | ShowManagement"
		
		def visitorInstance = Visitor.get(params.id)
		
		def visitorEntryInstanceList = VisitorEntry.findAllByVisitor(visitorInstance)
		//println "VisitorEntry List: ${visitorEntryInstanceList}"
		
		if (!visitorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
			redirect(action: "list")
			return
		}

		[visitorInstance: visitorInstance, visitorEntryInstanceList:visitorEntryInstanceList]
	}
	
	def showGate() {
		
		def adminSecuGard=session.user?.role == "guard"
		//println "Admin SecuGard: ${adminSecuGard}"
		if (!adminSecuGard){
			redirect(action:"list")
			return
		}
		
		def user=session.user
		//println "User: ${user}"
		
		def visitorInstance = Visitor.get(params.id)
		if (!visitorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
			redirect(action: "list")
			return
		}
		
		def visitorEntryInstance = new VisitorEntry(params)
		visitorEntryInstance.visitor=visitorInstance
		
		[visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance]
	}
	
	def showDesk() {
		
		def adminSecuDesk=session.user?.role == "desk"
		//println "Admin SecuDesk: ${adminSecuDesk}"
		if (!adminSecuDesk){
			redirect(action:"list")
			return
		}
		
		def visitorEntryInstance = VisitorEntry.get(params.entryId)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
		def visitorInstance=Visitor.get(visitorEntryInstance?.visitor.id)
		//println "Visitor: ${visitorInstance}"
		if (!visitorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
			redirect(action: "list")
			return
		}
		
		[visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance]
	}

    def edit() {
        def visitorInstance = Visitor.get(params.id)
		def visitorEntryInstance = VisitorEntry.get(params?.entryId)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
        if (!visitorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "list")
            return
        }
		
		if (visitorInstance?.id != visitorEntryInstance?.visitor?.id) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
			redirect(action: "list")
			return
		}
		
        [visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance, listOptionTypeOfCard:listOptionTypeOfCard()]
    }

    def update() {
        def visitorInstance = Visitor.get(params.id)
		//println "Visitor: ${visitorInstance}"
		
		def visitorEntryInstance=VisitorEntry.get(params.entry.id)
		//println "VisitorEntry: ${visitorEntryInstance}"
		
		if (!visitorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (visitorInstance.version > version) {
                visitorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'visitor.label', default: 'Visitor')] as Object[],
                          "Another user has updated this Visitor while you were editing")
                render(view: "edit", model: [visitorInstance: visitorInstance])
                return
            }
        }
		
		bindData(visitorEntryInstance, params)
		
		visitorEntryInstance=visitorEntryInstance.save(flush: true)
		//println "VisitorEntry: ${visitorEntryInstance}"
		if (visitorEntryInstance.hasErrors()){
			//println visitorEntryInstance.errors
		}
		
        visitorInstance.properties = params

        if (!visitorInstance.save(flush: true)) {
            render(view: "edit", model: [visitorInstance: visitorInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'visitor.label', default: 'Visitor'), visitorInstance.id])
        redirect(action: "show", id: visitorInstance.id, params:[entryId:visitorEntryInstance.id])
    }

    def delete() {
        def visitorInstance = Visitor.get(params.id)
        if (!visitorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "list")
            return
        }

        try {
            visitorInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'visitor.label', default: 'Visitor'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
