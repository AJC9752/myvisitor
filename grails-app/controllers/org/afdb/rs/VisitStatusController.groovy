package org.afdb.rs

import org.afdb.utils.SecureController;

/**
 * VisitStatusController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class VisitStatusController extends SecureController{

	static scaffold = true
//	def index = { }
}
