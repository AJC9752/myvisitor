package org.afdb.rs

import org.afdb.*
import org.afdb.utils.SecureController;

/**
 * LocationController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class LocationController extends SecureController{
	
	static scaffold = true
	
	def staffService
	
	def selectLocation(){
		println "Location | Select Location"
		
		def user=User.findByPerno(session.user.perno)
		println "User: ${user}"
		if (user.role == "user"){
			flash.error = message(code: 'default.admin.user.notFound.message', default: 'You are not admin, Please contact PSEC')
			redirect (controller:"visiteForm",action:"list")
			return
		}	
//		[user: user]
	}
	
	def addLocationInSession(){
		println "Location | Add Location InSession"
		
//		println "Params: ${params}"
		
		def user=User.findByPerno(session.user.perno)
		if (params?.location == "null"){
			flash.error = message(code: 'gate.location.isempty.label')
			redirect (controller:"location", action:"selectLocation")
			return
		}else{
			session.location=params?.location
		}
		
		if (session.user?.role == "admin"){
			redirect (controller:"visiteForm",action:"listManagement")
			return
		}else if (session.user?.role == "desk"){
			redirect (controller:"visitor",action:"listDesk")
			return
		}else if (session.user?.role == "guard"){
			redirect (controller:"visitor",action:"listGate")
			return
		}else{
			redirect (controller:"visiteForm",action:"list")
			return
		}
		
	}
	
//	def index = { }
}
