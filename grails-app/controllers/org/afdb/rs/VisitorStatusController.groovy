package org.afdb.rs

import org.afdb.utils.SecureController;

/**
 * VisitorStatusController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class VisitorStatusController extends SecureController{

	static scaffold = true
//	def index = { }
}
