package org.afdb.rs

import org.afdb.utils.SecureController;

/**
 * TypeBadgeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class TypeBadgeController extends SecureController{

	static scaffold = true
//	def index = { }
}
