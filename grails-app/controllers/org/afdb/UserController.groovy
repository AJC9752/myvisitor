package org.afdb

import org.afdb.utils.SecureController;
import org.springframework.dao.DataIntegrityViolationException

/**
 * UserController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class UserController extends SecureController{

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def userService
	def mailingService
	
	def search={
		def q=params?.query
				
		def resultsUser = User.findAllByFullnameIlike("%${params?.query}%")
		try
		{
			Integer.parseInt(params?.query)
			resultsUser+=User.findAllByPerno(Integer.parseInt(params?.query))
		}
		catch(Exception e)
		{}
//		println ">>ResultsUser:${resultsUser}"
		
		def totalUser= resultsUser?.size()
		
		def userCount=resultsUser.size()
//		println "UserCount: ${userCount}"
		
		flash.message="${totalUser} results found"
		render (view:'list', model:[userInstanceList:resultsUser, userInstanceTotal: totalUser, userInstanceCount: userCount])
	}
	
	def auth(){
		
		debug()
		
		if (!session.user){
			flash.info = message(code: 'user.login.message')
			redirect(controller:"user",action:"login")
			return false
		}
		
		if(!session.user?.admin){
			flash.info = message(code: 'user.adminTaskOnly')
			//flash.message="Task for admin only"
			redirect(controller:"start")
			return false
		}
	}
	
	def login(){
		println "User | Login"
		
		println ">FLASH: ${session.controller}"
	}
	
	def logout(){
		println "User | Logout"
		
		flash.message="Goodbye ${session?.user?.fullname}"
		log.info("LOGOUT : ${session.user}  at ${new Date()}")
		println ">>>C'est super"
		session.user=null
		session.invalidate()
		redirect (action:"login")
	}
	
	def authenticate(){
		println "User | Authenticate"
		
		println ">UserName: ${params.username}"
		println ">FLASH 2: ${session.controller}"
		
		def login =params.username
		def password= params.password
		
		if (!login || !password){
			flash.error="Incorrect username or password please, please try again"
			redirect(action:"login")
			return
		}
		
		def user =userService.getUserConnected(login,password)
		println "User: ${user}"
		
		if (user){
			
			def userSecu
			if (user?.login in ['SECUHQPREST', 'SECUHQVISIT', 'RECEPTIONBADCCIA']){
				userSecu=User.findByLogin(user?.login)
				session.user = userSecu
			}else{
				session.user = user
			}
			flash.info="${user.fullname} !"
			//log.info("LOGIN user : ${user} log at ${new Date()}")
			//redirect (controller:"start",action:"index")
			
			def staffConnected=Staff.findByPerno(session.user.perno)
			if (!staffConnected){//si le staff n'existe pas on va le creer
				if ((user.fullname).contains(",")){
					def splitted = user.fullname.split(",")
					def firstName=splitted[1]
//					println "FirstName: ${firstName}"
					def lastName=splitted[0]
//					println "LastName: ${lastName}"
					
					new Staff(perno:user.perno, email: user.email, firstName: firstName, lastName: lastName).save(flush:true)
				}			
			}

			if (!session.controller){
				if (session.user?.role == "admin"){
					session.location=null
					redirect (controller:"location",action:"selectLocation")
//					redirect (controller:"visiteForm",action:"listManagement")
					return
				}else if (session.user?.role == "desk"){
					session.location=null
					redirect (controller:"location",action:"selectLocation")
//					redirect (controller:"visitor",action:"listDesk")
					return
				}else if (session.user?.role == "guard"){
					session.location=null
					redirect (controller:"location",action:"selectLocation")
//					redirect (controller:"visitor",action:"listGate")
					return
				}else{
					session.location=null
					redirect (controller:"visiteForm",action:"list")
					return
				}
			}else if(session.controller){
				if (session.user?.role == "desk"){
					session.location=null
					redirect (controller:"location",action:"selectLocation")
//					redirect (controller:"visitor",action:"listDesk")
					return
				}else if (session.user?.role == "guard"){
					session.location=null
					redirect (controller:"location",action:"selectLocation")
//					redirect (controller:"visitor",action:"listGate")
					return
				}
			}
			
			redirect (controller:session.controller,action:session.action, id:session.idReq,params:session.params)
			
		}else{
		
			flash.error="Incorrect username or password please, please try again"
			redirect(action:"login")
		}
	}
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [userInstanceList: User.list(params), userInstanceTotal: User.count()]
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
        def userInstance = new User(params)
        if (!userInstance.save(flush: true)) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def show() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def edit() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def update() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'user.label', default: 'User')] as Object[],
                          "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

        userInstance.properties = params

        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def delete() {
        def userInstance = User.get(params.id)
        if (!userInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
