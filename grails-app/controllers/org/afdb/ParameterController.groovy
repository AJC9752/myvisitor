package org.afdb

import org.afdb.utils.SecureController;

/**
 * ParameterController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ParameterController extends SecureController{

	static scaffold = true
//	def index = { }
}
