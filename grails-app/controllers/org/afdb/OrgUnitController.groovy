package org.afdb

import org.afdb.utils.SecureController;
import org.springframework.dao.DataIntegrityViolationException

/**
 * OrgUnitController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class OrgUnitController extends SecureController{

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
		
        redirect(action: "list", params: params)
    }

    def list() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [orgUnitInstanceList: OrgUnit.list(params), orgUnitInstanceTotal: OrgUnit.count()]
    }

    def create() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
        [orgUnitInstance: new OrgUnit(params)]
    }

    def save() {
        def orgUnitInstance = new OrgUnit(params)
        if (!orgUnitInstance.save(flush: true)) {
            render(view: "create", model: [orgUnitInstance: orgUnitInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), orgUnitInstance.id])
        redirect(action: "show", id: orgUnitInstance.id)
    }

    def show() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
        def orgUnitInstance = OrgUnit.get(params.id)
        if (!orgUnitInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "list")
            return
        }

        [orgUnitInstance: orgUnitInstance]
    }

    def edit() {
		def admin=session.user?.role == "admin"
		println "Admin: ${admin}"
		if (!admin){
			redirect(controller:"visiteForm", action:"list")
			return
		}
        def orgUnitInstance = OrgUnit.get(params.id)
        if (!orgUnitInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "list")
            return
        }

        [orgUnitInstance: orgUnitInstance]
    }

    def update() {
        def orgUnitInstance = OrgUnit.get(params.id)
        if (!orgUnitInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (orgUnitInstance.version > version) {
                orgUnitInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'orgUnit.label', default: 'OrgUnit')] as Object[],
                          "Another user has updated this OrgUnit while you were editing")
                render(view: "edit", model: [orgUnitInstance: orgUnitInstance])
                return
            }
        }

        orgUnitInstance.properties = params

        if (!orgUnitInstance.save(flush: true)) {
            render(view: "edit", model: [orgUnitInstance: orgUnitInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), orgUnitInstance.id])
        redirect(action: "show", id: orgUnitInstance.id)
    }

    def delete() {
        def orgUnitInstance = OrgUnit.get(params.id)
        if (!orgUnitInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "list")
            return
        }

        try {
            orgUnitInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'orgUnit.label', default: 'OrgUnit'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
