package org.afdb

import org.afdb.Staff
import org.codehaus.groovy.grails.web.mapping.LinkGenerator
import grails.util.Environment

class ParameterService {
	LinkGenerator grailsLinkGenerator
	
    static String getLDAP() {
		def vParam=Parameter.findByName(Parameter.LDAP_URL)?.value
		 vParam
    }
   
	String serverUrl() {
		def appUrl=Parameter.findByName(Parameter.URL_APP)?.value
		return appUrl
	 }
   
   
}
