package org.afdb

import java.util.List
import org.afdb.Staff
import org.afdb.rs.*
import grails.util.Environment

/**
 * MailingService
 * A service class encapsulates the core business logic of a Grails application
 */
class MailingService {
	
	def mailService
	def staffService
	def parameterService
	
    static transactional = true
	
	/**
	 * renvoie la liste des emails pour l'envoie en cas de test
	 * @return
	 */
	List getEmailTestList(def email){
		
		List emailList=[]
		if (Environment.current == Environment.DEVELOPMENT){
			emailList= Parameter.findByName(Parameter.EMAIL_RECEIVER_TEST)?.value?.split(";")
		}else{
			emailList=email.split(";")
		}
		return emailList
	}
	
	List getListMailStaff (def emailStaff, def emailPrivate){
		
		def listReceiver=[]
		listReceiver<<emailStaff
		listReceiver<<emailPrivate
		
		List listMailStaff=listReceiver.flatten()
		println ">>>ListMailStaff: ${listMailStaff}"
		
		return listMailStaff
	}
	
	def submit4Approval(def form){
		println ">>>MailingService | Submit4Approval<<<"
		
		def visiteForm=VisiteForm?.get(form?.id)
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def receiverEmail=Parameter.findByName(Parameter.EMAIL_SECU_ABIDJAN)?.value
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_SUBMIT4VALIDATION)?.value
		
		def askByStaff=Staff?.findByPerno(visiteForm?.askBy?.perno)
		println ">>>AskBy: ${askByStaff}"
		def receiverVisitStaff=Staff?.findByPerno(visiteForm?.receiverVisit?.perno)
		println ">>>ReceiverVisit: ${receiverVisitStaff}"
		
		List emailListCC=[]
		emailListCC<<this.getEmailTestList(receiverVisitStaff?.email)
		emailListCC<<this.getEmailTestList(askByStaff?.email)
		emailListCC=emailListCC.flatten()
		println ">>>emailListCC: ${emailListCC}"
		
		mailService.sendMail{
			from senderEmail
			to receiverEmail
			cc	emailListCC
			bcc ccTest
			subject "eVisitor: ${visiteForm?.visitType?.name} - ${receiverVisitStaff} | " + subjectMail
			html view: "/email/submit4Approval", model: [visiteForm:visiteForm, receiverEmail:receiverEmail, emailListCC:emailListCC, 
											askByStaff:askByStaff, receiverVisitStaff: receiverVisitStaff, appUrl:parameterService.serverUrl()]
		}
	}
	
	def approvedVisit(def askBy, def receiverVisit, def approvedBy){
		println ">>>MailingService | Approved Visit<<<"
		
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def receiverEmail=Parameter.findByName(Parameter.EMAIL_SECU_ABIDJAN)?.value
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_APPROVEDVISIT)?.value
		
		def askByStaff=Staff?.findByPerno(askBy?.perno)
		println ">>>AskBy: ${askByStaff}"
		def receiverVisitStaff=Staff?.findByPerno(receiverVisit?.perno)
		println ">>>ReceiverVisit: ${receiverVisitStaff}"
		def approvedByStaff=Staff?.findByPerno(approvedBy?.perno)
		println ">>>Approved By: ${approvedByStaff}"
		
		List emailList=[]
		emailList<<this.getEmailTestList(receiverVisitStaff?.email)
		emailList<<this.getEmailTestList(askByStaff?.email)
		emailList=emailList.flatten()
		println ">>>emailList: ${emailList}"
		
		List emailListCC=[]
		emailListCC<<this.getEmailTestList(receiverEmail)
		emailListCC<<this.getEmailTestList(approvedByStaff?.email)
		emailListCC=emailListCC.flatten()
		println ">>>emailListCC: ${emailListCC}"
		
		mailService.sendMail{
			from senderEmail
			to emailList
			cc	emailListCC
			bcc ccTest
			subject "eVisitor: ${receiverVisitStaff} | " + subjectMail
			html view: "/email/approvedVisit", model: [emailList:emailList, emailListCC:emailListCC, askByStaff:askByStaff, receiverVisitStaff: receiverVisitStaff, approvedByStaff:approvedByStaff, appUrl:parameterService.serverUrl()]
		}
	}
	
	def declinedVisit(def askBy, def receiverVisit, def declinedBy){
		println ">>>MailingService | Declined Visit<<<"
		
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def receiverEmail=Parameter.findByName(Parameter.EMAIL_SECU_ABIDJAN)?.value
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_DECLINEDVISIT)?.value
		
		def askByStaff=Staff?.findByPerno(askBy?.perno)
		println ">>>AskBy: ${askByStaff}"
		def receiverVisitStaff=Staff?.findByPerno(receiverVisit?.perno)
		println ">>>ReceiverVisit: ${receiverVisitStaff}"
		def declinedByStaff=Staff?.findByPerno(declinedBy?.perno)
		println ">>>Declined By: ${declinedByStaff}"
		
		List emailList=[]
		emailList<<this.getEmailTestList(askByStaff?.email)
		emailList<<this.getEmailTestList(receiverVisitStaff?.email)
		emailList=emailList.flatten()
		println ">>>emailList: ${emailList}"
		
		List emailListCC=[]
		emailListCC<<this.getEmailTestList(receiverEmail)
		emailListCC<<this.getEmailTestList(declinedByStaff?.email)
		emailListCC=emailListCC.flatten()
		println ">>>emailListCC: ${emailListCC}"
		
		mailService.sendMail{
			from senderEmail
			to emailList
			cc	emailListCC
			bcc ccTest
			subject "eVisitor: ${receiverVisitStaff} | " + subjectMail
			html view: "/email/declinedVisit", model: [emailList:emailList, emailListCC:emailListCC, askByStaff:askByStaff, receiverVisitStaff: receiverVisitStaff, declinedByStaff:declinedByStaff, appUrl:parameterService.serverUrl()]
		}
	}
	
	def entryGate(def visitor, def entry){
		println ">>>MailingService | Access Accepted Gate<<<"
		
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def visitorInstance=Visitor.get(visitor.id)
		
		def visitorEntryInstance=VisitorEntry.get(entry.id)
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def askBy=visitorInstance?.visit?.askBy
		def askByEmail=visitorInstance?.visit?.askBy?.email
		def receiverVisit=visitorInstance?.visit?.receiverVisit
		def receiverEmail=visitorInstance?.visit?.receiverVisit?.email
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_ACCESSGATE)?.value
				
		mailService.sendMail{
			from senderEmail
			to receiverEmail
			cc	askByEmail
			bcc ccTest
			subject "eVisitor: ${receiverVisit} | " + subjectMail
			html view: "/email/accessdGate", model: [receiverVisit: receiverVisit, askBy:askBy, visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance, appUrl:parameterService.serverUrl()]
		}
	}
	
	def deniedGate(def visitor, def entry){
		println ">>>MailingService | Access Denied Gate<<<"
		
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def visitorInstance=Visitor.get(visitor.id)
		
		def visitorEntryInstance=VisitorEntry.get(entry.id)
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def receiverEmail=Parameter.findByName(Parameter.EMAIL_SECU_ABIDJAN)?.value
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_DENIEDGATE)?.value
				
		mailService.sendMail{
			from senderEmail
			to receiverEmail
			bcc ccTest
			subject "eVisitor: ${visitorInstance?.visit?.receiverVisit} | " + subjectMail
			html view: "/email/accessDeniedGate", model: [receiverEmail: receiverEmail, visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance, appUrl:parameterService.serverUrl()]
		}
	}
	
	def deniedReception(def visitor, def entry){
		println ">>>MailingService | Access Denied Desk<<<"
		
		def ccTest=Parameter.findByName(Parameter.BCC_EMAIL)?.value.split(";")
		
		def visitorInstance=Visitor.get(visitor.id)
		
		def visitorEntryInstance=VisitorEntry.get(entry.id)
		
		def senderEmail=Parameter.findByName(Parameter.EMAIL_SENDER)?.value
		def receiverEmail=Parameter.findByName(Parameter.EMAIL_SECU_ABIDJAN)?.value
		def subjectMail=Parameter.findByName(Parameter.SUBJECT_DENIEDDESK)?.value
				
		mailService.sendMail{
			from senderEmail
			to receiverEmail
			bcc ccTest
			subject "eVisitor: ${visitorInstance?.visit?.receiverVisit} | " + subjectMail
			html view: "/email/accessDeniedDesk", model: [receiverEmail: receiverEmail, visitorInstance: visitorInstance, visitorEntryInstance:visitorEntryInstance, appUrl:parameterService.serverUrl()]
		}
	}
	
	
    def serviceMethod() {

    }
}
