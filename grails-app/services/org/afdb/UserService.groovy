package org.afdb
import grails.util.GrailsUtil;

class UserService {

    User getUserConnected(String login,String passwd){
		User user
//		println "00: getUserConnected ${login}"
		
		switch(GrailsUtil.environment){
			case "development":
//			case ["development","test"]:
			println "01-development"
			
				user= User.findByLoginAndPassword(login,passwd)
				//println "02"
				//println "user: ${user?.login}"
				//println "021"
				
				break
				
			case "test_before" :
				println "02-test Environment"
				
					if (login=="admin"){
						user= User.findByLoginAndPassword(login,passwd)
						break
					}
				
				AuthentificationAD aut=new AuthentificationAD()
				UserBean userBean= aut.authentificate_AD(login, passwd, "afdb", ParameterService.getLDAP())
				
				if (userBean){					
					
					user=User.findByEmail(userBean.email.toLowerCase())
					if (!user){
					println " Not found"
					
					def role="user"  //mettre tout le monde � admin pour voir toutes les possiblit�s
						user=new User(login:login,role:role,email:userBean.email, fullname:userBean.fullname)
					}else{
						println " User found"
					}					
					
					
				}
				break
				
				
//			case "production":
			case ["production","test"]:
			println "03-production"
				
				println "Login: ${login}"
				if (login in ['CCIAGATE', 'HQGATE', 'TESTSECUAGENT1', 'TRESORSGATE']){
					println "Login GATE"
					user= User.findByLoginAndPassword(login,passwd)
					break
				}else{
					println "Login USER"
					if (login=="admin"){
						user= User.findByLoginAndPassword(login,passwd)
						break
					}
				
				AuthentificationAD aut=new AuthentificationAD()
				UserBean userBean= aut.authentificate_AD(login, passwd, "afdb", ParameterService.getLDAP())
				
				if (userBean){					
					
					user=User.findByEmail(userBean?.email?.toLowerCase())
					if (!user){
					println " Not found"

						user=new User(perno:userBean.perno,login:login,role:"user",email:userBean.email, fullname:userBean.fullname )
					}else{
						println " User found"
					}					
					
					
				}
				break
			}
		 }
		println "0F : ${user?.login}  - perno: ${user?.perno} - email : ${user?.email} on ${new Date()} role :  ${user?.role} "
		return user
	}
	
	def currentUser(def user){
		User.findByEmail(user?.email?.toLowerCase())
	}
	
}
