package org.afdb

/**
 * StaffService
 * A service class encapsulates the core business logic of a Grails application
 */

class StaffService {
	
	def getStaffinSession(def user){
		Staff.findByPerno(user?.perno)
	}
	
	def currentStaff(def user){
		Staff.findByEmail(user?.email?.toLowerCase())
	}
	
    def serviceMethod() {

    }
	
}
