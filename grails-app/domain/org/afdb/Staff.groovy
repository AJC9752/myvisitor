package org.afdb

/**
 * Staff
 * A domain class describes the data object and it's mapping to the database
 */
class Staff {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	Integer perno
	String lastName
	String firstName
	
	OrgUnit orgUnit
	String position
	String grade
	
	String extension
	String cell
	
	String email
	String categorie
	
	
	String orgUnitCode

//	String language

	String gender
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static mapping = {
		sort perno:"asc"
	}
    
	static constraints = {
		perno nullable: false,unique:true
		firstName nullable: true
		lastName nullable: true
		position nullable: true
		
		// Ajout
		orgUnit nullable: true
		position nullable: true
		
		extension	nullable: true
		cell	nullable: true

		 gender nullable: true
		//Fin ajout

		email nullable: true
		grade nullable: true
		categorie nullable: true
		orgUnitCode nullable:true

	}
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		
		return "${perno} - ${lastName} ${firstName}";
	}
	
	String getEmail(){ email?.toLowerCase()}
	
}
