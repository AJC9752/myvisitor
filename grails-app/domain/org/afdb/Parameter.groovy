package org.afdb

/**
 * Parameter
 * A domain class describes the data object and it's mapping to the database
 */
class Parameter {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	String name
	String value
	String description
	//NON
	
	static final String LDAP_URL="LDAP_URL"
	def static URL_APP="URL_APP"
	
	def static EMAIL_SENDER="EMAIL_SENDER"
	def static BCC_EMAIL="BCC_EMAIL"
	def static EMAIL_RECEIVER_TEST="EMAIL_RECEIVER_TEST"
	def static EMAIL_SECU_ABIDJAN="EMAIL_SECU_ABIDJAN"
	
	def static SUBJECT_SUBMIT4VALIDATION="SUBJECT_SUBMIT4VALIDATION"
	def static SUBJECT_APPROVEDVISIT="SUBJECT_APPROVEDVISIT"
	def static SUBJECT_DECLINEDVISIT="SUBJECT_DECLINEDVISIT"
	def static SUBJECT_DENIEDGATE="SUBJECT_DENIEDGATE"
	def static SUBJECT_DENIEDDESK="SUBJECT_DENIEDDESK"
	def static SUBJECT_ACCESSGATE="SUBJECT_ACCESSGATE"
	
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
		name ()
		value ()
		description nullable:true
    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
