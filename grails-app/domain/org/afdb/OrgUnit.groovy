package org.afdb

import java.util.Date;

/**
 * OrgUnit
 * A domain class describes the data object and it's mapping to the database
 */
class OrgUnit {
	
	static searchable = true
	
	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	String code
	String name
	
	OrgUnit orgUnit

	String category
	static String T_DIVISION="Division"
	static String T_DEPARTMENT="Department"
	static String T_UNIT="Unit"
	static String T_COMPLEX="Complex"
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
		sort code:"asc"
    }
    
	static	constraints = {
		code ()
		name(nullable:true)
		orgUnit(nullable:true)
		category (inList:[T_DIVISION,T_UNIT,T_DEPARTMENT,T_COMPLEX], nullable:true)
		lastUpdated(nullable: true)
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${code}";
	}
}
