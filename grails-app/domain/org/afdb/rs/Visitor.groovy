package org.afdb.rs

import org.afdb.*

/**
 * Visitor
 * A domain class describes the data object and it's mapping to the database
 */
class Visitor {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	String	firstName
	String	lastName
	String	gender
	static String MALE="M"
	static String FEMALE="F"
	
	String	address
	String	contact
	
	String	typeOfCard
	static final String PC="PC"
	static final String CNI="CNI"
	static final String PASSPORT="PASSPORT"
	static final String LAISSEZPASSER="LAISSEZPASSER"
	static final String CARTECONSULAIRE="CARTECONSULAIRE"
	static final String OTHER="OTHER"
	
	String	otherCard
	String	numberCard
	Date	validityDate
//	Integer	badgeNumber
	
//	VisitorStatus	status
	VisiteForm	visit
	VisitType	visitType
	
	Date	checkinGateDate
//	Date	checkoutDateTime
	
//	User	deniedGateBy
//	User	entryPermitBy
	
//	Staff	deniedReceptionBy
//	Staff	checkinBy
	
//	Staff	checkoutBy
	
//	String	deniedGate
//	String 	deniedReception
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [entrys:VisitorEntry]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
		firstName()
		lastName()
		gender(inList:[MALE,FEMALE])
		
		address	nullable:true
		contact nullable:true
		
		typeOfCard	nullable:true
		otherCard	nullable:true
		numberCard	nullable:true
		validityDate	nullable:true
		
		visit	nullable:true
		visitType	nullable:true
		
		checkinGateDate	nullable:true
		/*
		status()
		
		badgeNumber nullable:true
		
		checkinDateTime		nullable:true
		checkoutDateTime	nullable:true
		
		deniedGateBy		nullable:true
		entryPermitBy		nullable:true
		deniedReceptionBy	nullable:true
		checkinBy			nullable:true
		checkoutBy			nullable:true
		
		deniedGate			nullable:true, maxSize:1024
		deniedReception		nullable:true, maxSize:1024
		*/
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${firstName} ${lastName}, ${gender}";
	}
}
