package org.afdb.rs

import java.util.Date;

import org.afdb.Staff;
import org.afdb.User;

/**
 * VisitorEntry
 * A domain class describes the data object and it's mapping to the database
 */
class VisitorEntry {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	Visitor	visitor
	VisitorStatus	status
	
	Date	checkinDateTime
	Date	checkoutDateTime
	
	User	deniedGateBy
	User	entryPermitBy
	
	Staff	deniedReceptionBy
	Staff	checkinBy
	
	Staff	checkoutBy
	
	String	deniedGate
	String 	deniedReception
	
	TypeBadge	badge
	Integer		badgeNumber
	
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
		visitor()
		status()
		
		badge		nullable:true
		badgeNumber	nullable:true
		
		checkinDateTime		nullable:true
		checkoutDateTime	nullable:true
		
		deniedGateBy		nullable:true
		entryPermitBy		nullable:true
		deniedReceptionBy	nullable:true
		checkinBy			nullable:true
		checkoutBy			nullable:true
		
		deniedGate			nullable:true, maxSize:1024
		deniedReception		nullable:true, maxSize:1024
    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
