package org.afdb.rs

/**
 * VisitorStatus
 * A domain class describes the data object and it's mapping to the database
 */
class VisitorStatus {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	String code
	String name
	String nameFr
	
	static String INITIAL="INITIAL"
	static String AWAITING="AWAITING"
	static String DENIED_GATE="DENIED_GATE"
	static String ENTRANCE="ENTRANCE"
	static String DENIED_DESK="DENIED_DESK"
	static String CHECKIN="CHECKIN"
	static String CHECKOUT="CHECKOUT"
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
		code()
		name nullable: true, maxSize: 50
		nameFr nullable: true, maxSize: 50
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${name}";
	}
	public getName(def lg){
		if (lg=='fr')
			return nameFr
		else 
			return name
	}
}
