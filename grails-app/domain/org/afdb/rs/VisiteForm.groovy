package org.afdb.rs

import java.util.Date;
import java.util.Formatter.DateTime

import org.afdb.Staff;

class VisiteForm {
	
	Staff 	receiverVisit
	Staff	askBy
	
	Staff	approvedBy
	Staff	declinedBy
	Staff	canceledBy
	
	String	company
	String	event
	String	purposeOfVisite
	
	Date	expectedArrivingDateTime
	Date 	expectedDepartureDateTime
	
	Integer	visitDuration
	
	VisitStatus	status
	VisitType	visitType
	Location	location
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [visitors:Visitor]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping
		
	static	mapping = {
	}
	
    static constraints = {
		receiverVisit	nullable:true
		askBy()
		
		approvedBy	nullable:true
		declinedBy	nullable:true
		canceledBy	nullable:true
		
		status()
		visitType()
		location()
		
		company	nullable:true
		event	nullable:true
		purposeOfVisite		nullable:true, maxSize:3076
		
		expectedArrivingDateTime(nullable:true)
		expectedDepartureDateTime(nullable:true)
		visitDuration(nullable:true)
		
		dateCreated	nullable:true
		lastUpdated	nullable:true
    }
}
