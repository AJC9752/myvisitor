package org.afdb.rs

import java.io.Serializable;

/**
 * VisitorsInDay
 * A domain class describes the data object and it's mapping to the database
 */
class VisitorsInDay implements Serializable{

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	String	codeBuilding
	String	nameBuilding
	Integer	nbVisitor
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
		table 'visitors_in_day'
		id name:'codeBuilding'
		codeBuilding	column:'code_building'
		nameBuilding column:'name_building'
		nbVisitor column:'nb_visitor'
		version false
    }
    
	static	constraints = {
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${codeBuilding} - ${nbVisitor}";
	}
}
