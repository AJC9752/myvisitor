package org.afdb

/**
 * User
 * A domain class describes the data object and it's mapping to the database
 */
class User {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	Integer perno
	String login
	String password
	String email
	String fullname

	String role ="user"
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
	
    static	mapping = {
    }
    
	static	constraints = {
		login(blank:false, unique:true)
		password(nullable:true, password:true)
		fullname(nullable:true)
		email(email:true)
		role(inList:["admin", "user", "desk", "guard"])
		perno(nullable :true)
    }
	
	static transients=['admin']
	
	boolean isAdmin(){
		role == "admin" ||  isSysadmin()
	}
	
	boolean isSysadmin(){
		role == "sysadmin"
	}
	
	String toString(){
		login
	}
	
	def beforeInsert(){
		//password=password.encodeAsSHA()
	}
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
