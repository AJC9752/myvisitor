import org.afdb.Parameter
import org.afdb.Staff;

class BootStrap {

    def init = { servletContext ->
		
		/* PARAMETRE GLOBAL
		*/
	   def parameterLdap=new Parameter(name:Parameter.LDAP_URL,value:"ldap://192.168.140.41:389")
	   
	   if (!Parameter.findByName(Parameter.LDAP_URL)){
		   parameterLdap.save()
		   println ">>save it : LDAP_URL!!!"
	   }
	   
	   def parameterEmailSender=new Parameter(name:Parameter.EMAIL_SENDER,value:"noreply@afdb.org", description:"The person sending email to the Moving companies")
	   if (!Parameter.findByName(Parameter.EMAIL_SENDER)){
		   parameterEmailSender.save()
		   println ">>save it : EMAIL_SENDER!!!"
	   }
	   
	   //STAFF
	   /*
	   def staffNaon=new Staff(perno:4280, firstName:"Adama", lastName:"NAON", email:"a.naon@afdb.org")
	   def staffJoel=new Staff(perno:9752, firstName:"Joel", lastName:"ADALBERT", email:"j.adalbert@afdb.org")
	   def listStaff=[staffNaon,staffJoel]
	   listStaff.each {  
		   it.save()
		   if (it.hasErrors()){
			   println it.errors
		   }
	   }
	   */
    }
    def destroy = {
    }
}
