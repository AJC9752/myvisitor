dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
  development {
        dataSource {
			dbCreate = "update"
			//dbCreate = "create-drop"
			driverClassName = 'com.mysql.jdbc.Driver'
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect   
			
			username = "root"
			password = ""
			url="jdbc:mysql://localhost:3306/evisitor_250118?autoreconnect=true"		
			/*
			username = "ajc9752"
			password = "Tcat+Ajc"
			url="jdbc:mysql://192.168.227.141:3306/evisitor?autoreconnect=true"
			*/
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }				
    }
    test {
		dataSource {
			dbCreate = "update"
			//dbCreate = "create-drop"
			driverClassName = 'com.mysql.jdbc.Driver'
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect

		  
			username = "ajc9752"
			password = "Tcat+Ajc"
			url="jdbc:mysql://192.168.227.141:3306/evisitor?autoreconnect=true"
			
			pooled = true
			properties {
			   maxActive = -1
			   minEvictableIdleTimeMillis=1800000
			   timeBetweenEvictionRunsMillis=1800000
			   numTestsPerEvictionRun=3
			   testOnBorrow=true
			   testWhileIdle=true
			   testOnReturn=true
			   validationQuery="SELECT 1"
			}
		}
	}
	
    production {
       dataSource {
			dbCreate = "update"
			driverClassName = 'com.mysql.jdbc.Driver'
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect

			 username = "evisitor"
			 password = 'Iv1$iT0r1720'
			 url="jdbc:mysql://localhost:3306/evisitor?autoreconnect=true"
			
			pooled = true
			properties {
			   maxActive = -1
			   minEvictableIdleTimeMillis=1800000
			   timeBetweenEvictionRunsMillis=1800000
			   numTestsPerEvictionRun=3
			   testOnBorrow=true
			   testWhileIdle=true
			   testOnReturn=true
			   validationQuery="SELECT 1"
			}
		}
    }
}
