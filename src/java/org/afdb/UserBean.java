package org.afdb;


public class UserBean {
	   // private int EMS_EVENT_EDITIONS;
	    private Integer perno;
	    private String username;
	    private String fullname;
	    private String email;
	    private String unit;
	    private String dept;
	    private String complex;
	    
	    private boolean administrator;
	    
	    
	    public UserBean() {
	        
	    }

	    public void setPerno(Integer perno) {
	        this.perno = perno;
	    }

	    public Integer getPerno() {
	        return perno;
	    }

	    public void setUsername(String username) {
	        this.username = username;
	    }

	    public String getUsername() {
	        return username;
	    }

	    public void setFullname(String fullname) {
	        this.fullname = fullname;
	    }

	    public String getFullname() {
	        return fullname;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setUnit(String unit) {
	        this.unit = unit;
	    }

	    public String getUnit() {
	        return unit;
	    }

	    public void setDept(String dept) {
	        this.dept = dept;
	    }

	    public String getDept() {
	        return dept;
	    }

	    public void setComplex(String complex) {
	        this.complex = complex;
	    }

	    public String getComplex() {
	        return complex;
	    }

	   /*  public void setEMS_EVENT_EDITIONS(int EMS_EVENT_EDITIONS) {
	        this.EMS_EVENT_EDITIONS = EMS_EVENT_EDITIONS;
	    }

	    public int getEMS_EVENT_EDITIONS() {
	        return EMS_EVENT_EDITIONS;
	    } */

	    public void setAdministrator(boolean administrator) {
	        this.administrator = administrator;
	    }

	    public boolean isAdministrator() {
	        return administrator;
	    }

	  
	}