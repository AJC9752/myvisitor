package org.afdb;




import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.afdb.ParameterService;

public class AuthentificationAD {

	/**
	 * @param args
	 */
	
	
	public String doConnexion(String user, String password){
		
		return "OK for "+user +" and"+ password;
	}
	
	
    public static UserBean authentificate_AD(String username, String password, String domain, String ldapServeurUrl){
        UserBean aUserBean= null;
       
        // realiser la connexion simple
        try{
        LdapContext ctx = getLdapContext(username,password,domain, ldapServeurUrl);
       
        if (ctx!=null){
            aUserBean= new UserBean();
            // a partir de la connexion, chercher les informations d'un user donnee
            String sAMAccountName = username ;
            String searchFilter = "(&(objectClass=user)(sAMAccountName=" + sAMAccountName + "))";
            String returnedAtts[] = { "sn", "givenName", "mail", "telephoneNumber", "department", "name" };

            //Create the search controls  
            SearchControls searchCtls = new SearchControls();
            searchCtls.setReturningAttributes(returnedAtts);

            //Specify the search scope  
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String searchBase = "dc=afdb,dc=local";


            NamingEnumeration answer = null;
            try {
                 answer = ctx.search(searchBase, searchFilter, searchCtls);

            } catch (NamingException e) {
                  e.printStackTrace();
            }

            while (answer.hasMoreElements()) {
                  SearchResult sr = null;
                  try {
                        sr = (SearchResult) answer.next();

                  } catch (NamingException e) {
                        e.printStackTrace();
                  }
                 Attributes attrs = sr.getAttributes();
                 Enumeration listAttrsID = attrs.getIDs();

                while (listAttrsID.hasMoreElements()) {
                    String attrID = (String) listAttrsID.nextElement();
                    Attribute attribute = attrs.get(attrID);
                    String attrValue = "" ;

                    try {
                        attrValue = (String) attribute.get();
                        if (attrID.equals("name")){
                            aUserBean.setFullname(attrValue);
                        }
                        //if (attrID.equals("name")){
                        String str = attrValue;
                        StringTokenizer st = new StringTokenizer(str, ",");
                        
                           
                        aUserBean.setUsername( st.nextToken());
                       
                        
                        if (attrID.equals("department")){
                            aUserBean.setUnit(attrValue);
                        }
                        
                        if (attrID.equals("mail")){
                            aUserBean.setEmail(attrValue);
                        }
                        try{
                        String sPerno= username.substring(3);
                        
                        	aUserBean.setPerno(Integer.valueOf(sPerno));
                        }catch( Exception e){
                        }
                    } catch (NamingException e) {
                        e.printStackTrace();
                    }
                    //System.out.println(attrID + " : " + attrValue ) ;
                }

            }
        }
        }catch (Exception e){
        	e.printStackTrace();
        }
        return aUserBean;
    }
    
   
      
       public static LdapContext getLdapContext(String username, String password, String domain, String ldapServeurUrl){
                LdapContext ctx = null;
                try{
                      Hashtable env = new Hashtable();
                      env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
                      env.put(Context.SECURITY_AUTHENTICATION, "Simple");
                      
                      String strConnexion= domain+"\\"+username;
                      env.put(Context.SECURITY_PRINCIPAL, strConnexion);
                      env.put(Context.SECURITY_CREDENTIALS, password);
                
                      env.put(Context.PROVIDER_URL, ldapServeurUrl);

                      ctx = new InitialLdapContext(env, null);

                      System.out.println("Connection Successful.");

                }catch(NamingException nex){
                      System.out.println("LDAP Connection: FAILED");
                      nex.printStackTrace();
                } 

            return ctx;
          }
         
      public static void main(String...args){
       String username="NDN4280";
       System.out.println(username.substring(3));
       
       UserBean aUserBean=  authentificate_AD("NDN4280", "", "afdb", ParameterService.getLDAP());
              if (aUserBean!=null){
                  System.out.print("user connected :"+ aUserBean.getFullname()+"-"+aUserBean.getUnit());
              }else{
                 System.out.print("login error");
              }
       
      }

}
