package org.afdb.rs



import org.junit.*
import grails.test.mixin.*

/**
 * VisitorControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(VisitorController)
@Mock(Visitor)
class VisitorControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/visitor/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.visitorInstanceList.size() == 0
        assert model.visitorInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.visitorInstance != null
    }

    void testSave() {
        controller.save()

        assert model.visitorInstance != null
        assert view == '/visitor/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/visitor/show/1'
        assert controller.flash.message != null
        assert Visitor.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/visitor/list'


        populateValidParams(params)
        def visitor = new Visitor(params)

        assert visitor.save() != null

        params.id = visitor.id

        def model = controller.show()

        assert model.visitorInstance == visitor
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/visitor/list'


        populateValidParams(params)
        def visitor = new Visitor(params)

        assert visitor.save() != null

        params.id = visitor.id

        def model = controller.edit()

        assert model.visitorInstance == visitor
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/visitor/list'

        response.reset()


        populateValidParams(params)
        def visitor = new Visitor(params)

        assert visitor.save() != null

        // test invalid parameters in update
        params.id = visitor.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/visitor/edit"
        assert model.visitorInstance != null

        visitor.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/visitor/show/$visitor.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        visitor.clearErrors()

        populateValidParams(params)
        params.id = visitor.id
        params.version = -1
        controller.update()

        assert view == "/visitor/edit"
        assert model.visitorInstance != null
        assert model.visitorInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/visitor/list'

        response.reset()

        populateValidParams(params)
        def visitor = new Visitor(params)

        assert visitor.save() != null
        assert Visitor.count() == 1

        params.id = visitor.id

        controller.delete()

        assert Visitor.count() == 0
        assert Visitor.get(visitor.id) == null
        assert response.redirectedUrl == '/visitor/list'
    }
}
