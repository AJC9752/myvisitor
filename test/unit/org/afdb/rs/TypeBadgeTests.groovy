package org.afdb.rs



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(TypeBadge)
class TypeBadgeTests {

    void testSomething() {
       fail "Implement me"
    }
}
