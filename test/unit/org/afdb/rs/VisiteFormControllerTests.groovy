package org.afdb.rs



import org.junit.*
import grails.test.mixin.*

/**
 * VisiteFormControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(VisiteFormController)
@Mock(VisiteForm)
class VisiteFormControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/visiteForm/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.visiteFormInstanceList.size() == 0
        assert model.visiteFormInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.visiteFormInstance != null
    }

    void testSave() {
        controller.save()

        assert model.visiteFormInstance != null
        assert view == '/visiteForm/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/visiteForm/show/1'
        assert controller.flash.message != null
        assert VisiteForm.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/visiteForm/list'


        populateValidParams(params)
        def visiteForm = new VisiteForm(params)

        assert visiteForm.save() != null

        params.id = visiteForm.id

        def model = controller.show()

        assert model.visiteFormInstance == visiteForm
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/visiteForm/list'


        populateValidParams(params)
        def visiteForm = new VisiteForm(params)

        assert visiteForm.save() != null

        params.id = visiteForm.id

        def model = controller.edit()

        assert model.visiteFormInstance == visiteForm
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/visiteForm/list'

        response.reset()


        populateValidParams(params)
        def visiteForm = new VisiteForm(params)

        assert visiteForm.save() != null

        // test invalid parameters in update
        params.id = visiteForm.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/visiteForm/edit"
        assert model.visiteFormInstance != null

        visiteForm.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/visiteForm/show/$visiteForm.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        visiteForm.clearErrors()

        populateValidParams(params)
        params.id = visiteForm.id
        params.version = -1
        controller.update()

        assert view == "/visiteForm/edit"
        assert model.visiteFormInstance != null
        assert model.visiteFormInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/visiteForm/list'

        response.reset()

        populateValidParams(params)
        def visiteForm = new VisiteForm(params)

        assert visiteForm.save() != null
        assert VisiteForm.count() == 1

        params.id = visiteForm.id

        controller.delete()

        assert VisiteForm.count() == 0
        assert VisiteForm.get(visiteForm.id) == null
        assert response.redirectedUrl == '/visiteForm/list'
    }
}
