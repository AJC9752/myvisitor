package org.afdb.rs



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TypeBadgeController)
class TypeBadgeControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
