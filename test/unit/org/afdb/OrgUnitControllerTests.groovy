package org.afdb



import org.junit.*
import grails.test.mixin.*

/**
 * OrgUnitControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(OrgUnitController)
@Mock(OrgUnit)
class OrgUnitControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/orgUnit/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.orgUnitInstanceList.size() == 0
        assert model.orgUnitInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.orgUnitInstance != null
    }

    void testSave() {
        controller.save()

        assert model.orgUnitInstance != null
        assert view == '/orgUnit/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/orgUnit/show/1'
        assert controller.flash.message != null
        assert OrgUnit.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/orgUnit/list'


        populateValidParams(params)
        def orgUnit = new OrgUnit(params)

        assert orgUnit.save() != null

        params.id = orgUnit.id

        def model = controller.show()

        assert model.orgUnitInstance == orgUnit
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/orgUnit/list'


        populateValidParams(params)
        def orgUnit = new OrgUnit(params)

        assert orgUnit.save() != null

        params.id = orgUnit.id

        def model = controller.edit()

        assert model.orgUnitInstance == orgUnit
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/orgUnit/list'

        response.reset()


        populateValidParams(params)
        def orgUnit = new OrgUnit(params)

        assert orgUnit.save() != null

        // test invalid parameters in update
        params.id = orgUnit.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/orgUnit/edit"
        assert model.orgUnitInstance != null

        orgUnit.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/orgUnit/show/$orgUnit.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        orgUnit.clearErrors()

        populateValidParams(params)
        params.id = orgUnit.id
        params.version = -1
        controller.update()

        assert view == "/orgUnit/edit"
        assert model.orgUnitInstance != null
        assert model.orgUnitInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/orgUnit/list'

        response.reset()

        populateValidParams(params)
        def orgUnit = new OrgUnit(params)

        assert orgUnit.save() != null
        assert OrgUnit.count() == 1

        params.id = orgUnit.id

        controller.delete()

        assert OrgUnit.count() == 0
        assert OrgUnit.get(orgUnit.id) == null
        assert response.redirectedUrl == '/orgUnit/list'
    }
}
