package org.afdb



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(MailingService)
class MailingServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
